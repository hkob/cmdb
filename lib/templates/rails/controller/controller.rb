<% if namespaced? -%>
require_dependency "<%= namespaced_path %>/application_controller"
<% end -%>
<% path_pname = class_name.underscore -%>
<% model_pname = File.basename(path_pname) -%>
<% model_sname = model_pname.singularize -%>
<% model_class = model_sname.camelize -%>
<% module_namespacing do -%>
class <%= class_name %>Controller < ApplicationController
  before_action :privilege_check
#  before_action :get_one, only: %i( show edit update delete destroy )
#  before_action :ajax_access, except: %i( index show )
  Privileges = %i( index show new create edit update delete destroy ).map { |k|
#    [ k, %w( is_kyoumu? ) ] }.to_h

#  def index
#   get_array
#   set_last_path
#  end

#  def show
#  end

#  def new
#    @<%= model_sname %> = <%= model_class %>.new(
#      abc: :def,
#      ghi: :jkl,
#      mno: :pqr
#    )
#  end

#  def create
#    @<%= model_sname %> = <%= model_class %>.create(<%= model_sname %>_params)
#    get_array
#  end

#  def edit
#  end

#  def update
#    @<%= model_sname %>.update(<%= model_sname %>_params)
#    get_array
#  end

#  def delete
#  end

#  def destroy
#    @<%= model_sname %>.destroy
#    get_array
#  end

#  private
#  def get_one
#    @<%= model_sname %> = <%= model_class %>.find(params[:id])
#  end

#  def get_array
#    @<%= model_pname %> = <%= model_class %>.order_start_year_desc.order_cur_type_desc
#  end

#  def <%= model_sname %>_params
#    params.require(:<%= model_sname %>).permit(:abc, :def, :ghi)
#  end
end
<% end -%>

##### append to config/locales/views/titles/ja.yml
    <%= model_pname %>:
      index:
        title: XXX一覧
        link_title: 一覧YYY
      show:
        title: XXX詳細
        link_title: 詳細YYY
      new:
        title: 新規XXX
        link_title: XXXを新規追加
      edit:
        title: XXX編集
        link_title: 編集YYY
      delete:
        title: 以下のXXXを削除します。よろしいですか?
        link_title: 削除YYY
