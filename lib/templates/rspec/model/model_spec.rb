require 'rails_helper'

RSpec.describe <%= class_name %>, :type => :model do
  context "common validation check" do
    before do
      @target = <%= singular_table_name %>_factory :<%= singular_table_name %>
    end

#    check_presence_validates %i( )
#    check_unique_validates %i( ) do
#      <%= singular_table_name %>_factory :other
#    end
#    check_plural_unique_validates %i( ) do
#      <%= singular_table_name %>_factory :other
#    end
#    check_destroy_validates
#    check_reject_destroy_validates
#    check_belongs_to :<%= singular_table_name %>, %i( has_many ), %i( has_one )
#    check_dependent_destroy :<%= singular_table_name %>, %i( has_many has_one )
#    check_reject_destroy_for_relations :<%= singular_table_name %>, %i( has_many has_one )
#    check_destroy_nullify_for_relations :<%= singular_table_name %>, %i( has_many has_one )
  end

  context 'after all <%= plural_table_name %> are registrered' do
    before do
      @<%= plural_table_name %> = <%= singular_table_name %>_all_factories
    end

    context 'for <%= class_name %> class' do
      subject { <%= class_name %> }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for <%= class_name %> instances' do
      subject { @<%= plural_table_name %> }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

##### write to spec/support/create_factories/<%= singular_table_name %>_factory.rb
<%= class_name %>FactoryHash = {
  one: %w( ),
  two: %w( ),
  three: %w( )
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [<%= class_name %>] <%= class_name %> FactoryGirl オブジェクト
def <%= singular_table_name %>_factory(key)
  n, c = <%= class_name %>FactoryHash[key.to_sym]
  FG.find_or_create(
    :<%= singular_table_name %>,
    name: n,
    code: c,
    sort_order: <%= class_name %>.sort_orders[key.to_sym]) if n
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<<%= class_name %>>] <%= class_name %> FactoryGirl オブジェクトの配列
def <%= singular_table_name %>_factories(keys)
  keys.map { |k| <%= singular_table_name %>_factory(k) }
end
