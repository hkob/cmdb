require 'rails_helper'

<% path_pname = class_name.underscore -%>
<% model_pname = File.basename(path_pname) -%>
<% model_sname = model_pname.singularize -%>
<% model_class = model_sname.camelize -%>
RSpec.describe <%= class_name %>Controller, type: :controller do
#  before { login_user_as :skyoumu, controller }
#  after { sign_out_one }
#  let!(:<%= model_sname %>) { <%= model_sname %>_factory :h24_shinkari}

#  describe 'GET index' do
#    before do
#      @<%= model_pname %> = <%= model_class %>FactoryHash.keys.map { |k| <%= model_sname %>_factory k }
#      get :index
#    end

#    it { expect(response).to have_http_status(:success) }
#    it('should have <%= model_pname %>') do
#      expect(assigns[:<%= model_pname %>]).to eq @<%= model_pname %>
#    end
#  end

#  describe 'xhr get new' do
#    subject { assigns[:<%= model_sname %>] }
#    before do
#      expect(controller).to receive(:now_year) { double Year, year: 2014 }
#      xhr :get, :new
#    end

#    it { expect(response).to have_http_status(:success) }
#    it('should have <%= model_sname %> for correct start_year') do
#      expect(subject.start_year).to eq 2015
#    end
#    it('should have <%= model_sname %> for correct cur_type') do
#      expect(subject.cur_type).to eq <%= model_sname %>.cur_type + 1
#    end
#  end

#  describe 'xhr post create' do
#    before do
#      @ph = <%= model_sname %>.attributes
#    end
#    subject { -> { xhr :patch, :create, <%= model_sname %>: @ph } }
#
#    context 'with valid params' do
#      before { @ph.merge!(name: :abc) }
#
#      it { subject.call; expect(response).to have_http_status(:success) }
#      it { expect { subject.call }.to change(<%= model_class %>, :count).by 1 }
#    end
#
#    context 'with invalid params' do
#      before { @ph.merge!(name: nil) }
#
#      it { subject.call; expect(response).to have_http_status(:success) }
#      it { expect { subject.call }.to change(<%= model_class %>, :count).by 0 }
#    end
#  end

#  describe 'for member action' do
#    describe 'get show' do
#      before do
#        get :show, id: <%= model_sname %>
#      end
#      it { expect(response).to have_http_status(:success) }
#      it('should have a <%= model_sname %>') do
#        expect(assigns[:<%= model_sname %>]).to eq <%= model_sname %>
#      end
#    end

#    describe 'xhr get edit' do
#      before do
#        xhr :get, :edit, id: <%= model_sname %>
#      end
#      it { expect(response).to have_http_status(:success) }
#      it('should have a <%= model_sname %>') do
#        expect(assigns[:<%= model_sname %>]).to eq <%= model_sname %>
#      end
#    end

#    describe 'xhr patch udpate' do
#      before do
#        @ph = <%= model_sname %>.attributes
#      end
#      subject { -> { xhr :patch, :update, id: <%= model_sname %>, <%= model_sname %>: @ph } }
#
#      context 'with valid params' do
#        before { @ph.merge!(name: :abc) }
#
#        it { subject.call; expect(response).to have_http_status(:success) }
#        it "should update start_year" do
#          expect { subject.call; <%= model_sname %>.reload }.to(
#            change(<%= model_sname %>, :name)
#          )
#        end
#      end
#
#      context 'with invalid params' do
#        before { @ph.merge!(name: nil) }
#
#        it { subject.call; expect(response).to have_http_status(:success) }
#        it "should not update dupulicated cur_type" do
#          expect { subject.call; <%= model_sname %>.reload }.to_not(
#            change(<%= model_sname %>, :name)
#          )
#        end
#      end
#    end

#    describe 'xhr get delete' do
#      subject { -> { xhr :get, :delete, id: <%= model_sname %> } }
#      it { subject.call; expect(response).to have_http_status(:success) }
#    end

#    describe 'xhr delete destroy' do
#      subject { -> { xhr :delete, :destroy, id: <%= model_sname %> } }

#      context 'with valid params' do
#        it { subject.call; expect(response).to have_http_status(:success) }
#        it { expect { subject.call }.to change(<%= model_class %>, :count).by -1 }
#      end
#    end

end

