# @!attribute param1
#   @return [Type] 説明
# @!attribute param2
#   @return [Type] 説明
# @!attribute param3
#   @return [Type] 説明
# @!attribute param4
#   @return [Boolean] 説明
# @!attribute param5
#   @return [Type] 説明
class <%= class_name %> < ActiveRecord::Base
  LoadKey = %i( id param1 param2 param3 param4 param5 param6 )
#  validates :param1, :param2, :param3, presence: true
#  validates :param4, inclusion: { in: [ true, false ] }
#  validates :param5, uniqueness: true
#  validates :param6, uniqueness: { scope: %i( kikan_id ) }

#  enum param3: { val1: 1, val2: 2, val3: 3 }

#  before_destroy :on_destroy_validation_method
#  # @return [Boolean] false (XXXは削除できない)
#  def on_destroy_validation_method
#    errors.add :base, 'XXXは削除できません'
#    false
#  end
#  private :on_destroy_validation_method

#  before_save :on_save_validation_method
#  # @return [boolean] 説明
#  def on_save_validation_method
#    ans = true
#    # do something
#    ans
#  end
#  private :on_save_validation_method

#  # @return [<AAA>] 対応するXXX
#  belongs_to :aaa

#  # @return [<BBB>] 対応するYYY
#  has_one :bbb

#  # @return [Array<CCC>] 対応するZZZ一覧
#  has_many :cccs

#  has_many :ddds, class_name: 'CCC', foreign_key: :ddd_id

#  # @return [Array<<%= class_name %>>] sort_order 順に並んだ <%= class_name %> 一覧を得る
#  scope :order_sort_order, -> { order arel_table[:sort_order] }
#  # @return [Array<<%= class_name %>>] qqq が v な <%= class_name %> 一覧を得る
#  scope :qqq_value_is, -> v { where arel_table[:qqq].eq v }

#  # @param [String] arg1 説明
#  # @return [<%= class_name %>] XXX な <%= class_name %>
#  def self.rrr(arg1)
#  end

#  # @param [String] arg1 説明
#  # @return [<%= class_name %>] 説明
#  def ttt(arg1)
#  end
end
