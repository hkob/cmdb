# spec/turnip_helper.rb
require 'rails_helper'
Dir.glob("spec/acceptance/steps/*steps.rb") { |f| load f, true }

require 'capybara'
require 'capybara/rspec'
require 'capybara/poltergeist'
require 'turnip'
require 'turnip/capybara'
Capybara.javascript_driver = :poltergeist

# web driver
Capybara.register_driver :poltergeist_debug do |app|
  Capybara::Poltergeist::Driver.new(app, :inspector => true)
end
