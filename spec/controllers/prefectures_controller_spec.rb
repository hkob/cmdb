require 'rails_helper'

RSpec.describe PrefecturesController, type: :controller do
  let!(:prefecture) { prefecture_factory :Tokyo }
  let!(:hall) { hall_factory :shibuya_live_in }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @prefectures = [ prefecture ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have prefectures') do
        expect(assigns[:prefectures]).to eq @prefectures
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: prefecture"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a prefecture') do
        expect(assigns[:prefecture]).to eq prefecture
      end
    end
  end
end
