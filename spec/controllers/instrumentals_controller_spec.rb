require 'rails_helper'

RSpec.describe InstrumentalsController, type: :controller do
  let!(:instrumental) { instrumental_factory :VOCAL }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @instrumentals = [ instrumental ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have instrumentals') do
        expect(assigns[:instrumentals]).to eq @instrumentals
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: instrumental"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a instrumental') do
        expect(assigns[:instrumental]).to eq instrumental
      end
    end
  end
end
