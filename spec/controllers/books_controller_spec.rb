require 'rails_helper'

RSpec.describe BooksController, type: :controller do
  let!(:book) { book_test_factory }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @books = [ book ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have books') do
        expect(assigns[:books]).to eq @books
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: book"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a book') do
        expect(assigns[:book]).to eq book
      end
    end
  end
end
