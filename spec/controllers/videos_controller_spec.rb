require 'rails_helper'

RSpec.describe VideosController, type: :controller do
  let!(:video) { video_test_factory }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @videos = [ video ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have videos') do
        expect(assigns[:videos]).to eq @videos
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: video"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a video') do
        expect(assigns[:video]).to eq video
      end
    end
  end
end
