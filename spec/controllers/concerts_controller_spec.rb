require 'rails_helper'

RSpec.describe ConcertsController, type: :controller do
  let!(:concert) { concert_test_factory }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index, concert_type: :live"
    describe action do
      before do
        @concerts = [ concert ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have concerts') do
        expect(assigns[:concerts]).to eq @concerts
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: concert"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a concert') do
        expect(assigns[:concert]).to eq concert
      end
    end
  end
end
