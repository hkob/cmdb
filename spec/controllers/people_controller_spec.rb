require 'rails_helper'

RSpec.describe PeopleController, type: :controller do
  let!(:person) { person_factory :iChisatoMoritaka }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index, head: '6'"
    describe action do
      before do
        @people = [ person ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have people') do
        expect(assigns[:people]).to eq @people
      end
    end
  end
end
