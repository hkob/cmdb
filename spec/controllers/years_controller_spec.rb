require 'rails_helper'

RSpec.describe YearsController, type: :controller do
  let!(:year) { year_factory 1987 }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @years = [ year ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have years') do
        expect(assigns[:years]).to eq @years
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: year"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a year') do
        expect(assigns[:year]).to eq year
      end
    end
  end
end
