require 'rails_helper'

RSpec.describe SinglesController, type: :controller do
  let!(:single) { single_test_factory }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @singles = [ single ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have singles') do
        expect(assigns[:singles]).to eq @singles
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: single"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a single') do
        expect(assigns[:single]).to eq single
      end
    end
  end
end
