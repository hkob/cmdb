require 'rails_helper'

RSpec.describe HallsController, type: :controller do
  let!(:hall) { hall_factory :shibuya_live_in }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :show, id: hall"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a hall') do
        expect(assigns[:hall]).to eq hall
      end
    end
  end
end
