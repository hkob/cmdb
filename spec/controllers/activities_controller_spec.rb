require 'rails_helper'

RSpec.describe ActivitiesController, type: :controller do
  let!(:activity) { activity_test2_factory }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @activities = [ activity ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have activities') do
        expect(assigns[:activities]).to eq @activities
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: activity"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a activity') do
        expect(assigns[:activity]).to eq activity
      end
    end
  end
end
