require 'rails_helper'

RSpec.describe BandsController, type: :controller do
  let!(:band) { band_factory :animals_1 }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @bands = [ band ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have bands') do
        expect(assigns[:bands]).to eq @bands
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: band"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a band') do
        expect(assigns[:band]).to eq band
      end
    end
  end
end
