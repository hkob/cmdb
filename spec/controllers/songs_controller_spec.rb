require 'rails_helper'

RSpec.describe SongsController, type: :controller do
  let!(:song) { song_factory :new_season }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index, head: '4'"
    describe action do
      before do
        @songs = [ song ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have songs') do
        expect(assigns[:songs]).to eq @songs
      end
    end
  end

  actions.each do |key|
    action = "#{key} :list_by_date, year_id: 1"
    describe action do
      before do
        @songs = [ song ]
        allow(Year).to receive(:find).with('1').and_return song.event_date.year
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have songs') do
        expect(assigns[:songs]).to eq @songs
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: song"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a song') do
        expect(assigns[:song]).to eq song
      end
    end
  end
end
