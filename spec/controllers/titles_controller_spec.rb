require 'rails_helper'

RSpec.describe TitlesController, type: :controller do
  let!(:title) { title_factory :new_season}

  describe 'GET index' do
    before do
      @titles = [ title ]
      get :index, head: '41'
    end

    it { expect(response).to have_http_status(:success) }
    it('should have titles') do
      expect(assigns[:titles]).to eq @titles
    end
  end
end
