require 'rails_helper'

RSpec.describe AlbumsController, type: :controller do
  let!(:album) { album_test_factory }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @albums = [ album ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have albums') do
        expect(assigns[:albums]).to eq @albums
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: album"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a album') do
        expect(assigns[:album]).to eq album
      end
    end
  end
end
