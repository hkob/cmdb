require 'rails_helper'

RSpec.describe CompaniesController, type: :controller do
  let!(:company) { company_factory :warner_pioneer }

  actions = [ 'get', 'xhr :get,' ]

  actions.each do |key|
    action = "#{key} :index"
    describe action do
      before do
        @companies = [ company ]
        eval action
      end

      it { expect(response).to have_http_status(:success) }
      it('should have companies') do
        expect(assigns[:companies]).to eq @companies
      end
    end
  end

  actions.each do |key|
    action = "#{key} :show, id: company"
    describe action do
      before do
        eval action
      end
      it { expect(response).to have_http_status(:success) }
      it('should have a company') do
        expect(assigns[:company]).to eq company
      end
    end
  end
end
