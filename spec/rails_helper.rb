# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require 'spec_helper'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
# Add additional requires below this line. Rails is not loaded until this point!

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  config.before :suite do
    DatabaseRewinder.clean_all
    # or
    # DatabaseRewinder.clean_with :any_arg_that_would_be_actually_ignored_anyway
  end

  config.after :each do
    DatabaseRewinder.clean
  end
end

# @param [Symbol] fg_key user の FactoryGirl のキー
# @param [ApplicatonController] コントローラのサブクラス
# @return [User] ログインしたユーザ
# ログインを回避し、@note now_year と now_campus の stub も用意する
def login_user_as(fg_key, controller)
#  allow(controller).to receive(:now_year) { double(Year, year: 2014) }
  allow(controller).to receive(:now_campus) { campus_factory :shinagawa }
  half_kikan_all_factories
  user = user_factory(fg_key)
  sign_in :user, user
  user.set_privileges(controller.user_session)
  user
end

# @note sign_out する
def sign_out_one
  sign_out controller.current_user
end

# @param [Symbol] fg_key user の FactoryGirl のキー
# @return [Admin] ログインした管理者ユーザ
# @note ログインを回避する
def login_admin_as(fg_key)
  admin = FactoryGirl.create(fg_key)
  allow(controller).to receive(:now_campus) { double(Campus, name: '品川', id: 1) }
  sign_in :admin, admin
  admin
end

# @praam [Array<Symbol>] 検査する presence 属性
# @param [Proc, nil] ターゲットを作成する Proc オブジェクト(必要な場合)
# @note Proc がない場合には @target を検査
def check_presence_validates(keys = [])
  it { expect(block_given? ? yield : @target).to be_valid }

  keys.each do |key|
    context "when #{key} is nil" do
      it do
        object = block_given? ? yield : @target
        object[key] = nil
        expect(object).to_not be_valid
      end
    end
  end
end

# @praam [Array<Symbol>] 検査する unique 属性
# @param [Proc] もう一つのオブジェクトを作成する Proc オブジェクト
def check_unique_validates(keys)
  keys.each do |key|
    context "when another object has same #{key}" do
      it do
        object = yield
        object[key] = @target[key]
        expect(object).to_not be_valid
      end
    end
  end
end

# @praam [Array<Symbol>] 検査する unique 属性
# @param [Proc] もう一つのオブジェクトを作成する Proc オブジェクト
def check_plural_unique_validates(keys)
  context "when another object has same #{keys.join ', '}" do
    it do
      object = yield
      keys.each do |key|
        object[key] = @target[key]
      end
      expect(object).to_not be_valid
    end
  end
  context "when another object has at least one different keys(#{keys.join ', '})" do
    it do
      keys.each do |except_key|
        object = yield
        compare = true
        keys.each do |key|
          if key == except_key
            compare = false if object[key] == @target[key]
          else
            object[key] = @target[key]
          end
        end
        expect(object).to be_valid if compare
        object.destroy
      end
    end
  end
end

# @param [Proc, nil] ターゲットを作成する Proc オブジェクト(必要な場合)
# @note Proc がない場合には @target を検査
def check_destroy_validates
  context "should destroy" do
    it do
      object = block_given? ? yield : @target
      klass = object.class
      expect { object.destroy }.to change(klass, :count).by -1
      end
  end
end

# @param [Proc, nil] ターゲットを作成する Proc オブジェクト(必要な場合)
# @note Proc がない場合には @target を検査
def check_reject_destroy_validates
  context "should not destroy" do
    it do
      object = block_given? ? yield : @target
      klass = object.class
      expect { object.destroy }.to change(klass, :count).by 0
      end
  end
end

# @param [Array<Symbol>] has_many_relations has_many の相手先関連名
# @param [Array<Symbol>] has_one_relations has_one の相手先関連名
# @param [Proc, nil] ターゲットを作成する Proc オブジェクト(必要な場合)
# @note Proc がない場合には @target を検査
# @note FactoryGirl を削除したときに、関連からデータ参照が消えるかを確認
def check_belongs_to(model, has_many_relations, has_one_relations = [], models = nil)
  context "when destroying #{model}" do
    has_many_relations.each do |relation|
      models ||= model.to_s.pluralize
      it "#{relation}.#{models}.count should decrease" do
        object = block_given? ? yield : @target
        parent = object.send(relation)
        expect { object.destroy }.to change(parent.send(models), :count).by(-1)
      end
    end
    has_one_relations.each do |relation|
      it "#{relation}.#{model} should be nil" do
        object = block_given? ? yield : @target
        parent = object.send(relation)
        object.destroy
        expect(parent.send(model, :true)).to be_nil
      end
    end
  end
end

# @param [Class] 確認するモデルクラス
# @param [Array<Symbol>] relations has_many, has_one の相手先関連名
# @param [Proc, nil] ターゲットを作成する Proc オブジェクト(必要な場合)
# @note Proc がない場合には @target を検査
# @note 関連を削除したときに、FactoryGirl が消えるかを確認
def check_dependent_destroy(model, relations)
  relations.each do |relation|
    context "when destroying #{relation}" do
      it "should be destroyed by dependency" do
        object = block_given? ? yield : @target
        parent = object.send(relation)
        expect { parent.destroy }.to change(model.to_s.classify.constantize, :count).by(-1)
      end
    end
  end
end

# @param [Array<Symbol>] relations has_many, has_one の相手先関連名
# @param [Proc, nil] ターゲットを作成する Proc オブジェクト(必要な場合)
# @note Proc がない場合には @target を検査
# @note FactoryGirl が存在するときに、関連が nil になることを確認
def check_destroy_nullify_for_relations(model, relations)
  relations.each do |relation|
    context "when destroying #{model}.#{relation}" do
      it "#{model} should set null to #{relation}" do
        object = block_given? ? yield : @target
        parent = object.send(relation)
        parent.destroy
        expect(object.send(relation, true)).to be_nil
      end
    end
  end
end

# @param [Array<Symbol>] relations has_many, has_one の相手先関連名
# @param [Proc, nil] ターゲットを作成する Proc オブジェクト(必要な場合)
# @note Proc がない場合には @target を検査
# @note FactoryGirl が存在するときに、関連を削除できないことを確認
def check_reject_destroy_for_relations(model, relations)
  relations.each do |relation|
    context "when destroying #{model}.#{relation}" do
      it "#{model} should reject destroying #{relation}" do
        object = block_given? ? yield : @target
        parent = object.send(relation)
        parent.destroy
        expect(parent.errors[:base].size).to eq(1)
      end
    end
  end
end

# @param [Object] target テストするオブジェクト
# @param [Array<Array<Object>>] answers method, answers, args
def target_method_send_test(target, *array)
  array.each_slice(3) do |method, args, answers|
    if args != nil
      expect(target.send(method, *args)).to eq answers
    else
      expect(target.send(method)).to eq answers
    end
  end
end

# @param [Object] target テストするオブジェクト
# @param [Array<Array<Object>>] answers method, answers, args
def target_method_send_test_with_chain(target, *array)
  array.each_slice(4) do |method, args, chain, answers|
    if args != nil
      expect(target.send(method, *args).send(:try, chain)).to eq answers
    else
      expect(target.send(method).send(:try, chain)).to eq answers
    end
  end
end

# @param [Array<Object>] target_array テストするオブジェクト配列
# @param [Array] array 引数配列 (メソッド名, 引数, その応答)の繰り返し
def target_array_method_send_test(target_array, *array)
  array.each_slice(3) do |method, args, answers|
    if args != nil
      expect(target_array.map { |o| o.send(method, *args) }).to eq answers
    else
      expect(target_array.map(&method)).to eq answers
    end
  end
end

# @param [Array<Object>] target_array テストするオブジェクト配列
# @param [Array] array 引数配列 (メソッド名, 引数, その応答)の繰り返し
def target_array_method_send_test_with_chain(target_array, *array)
  array.each_slice(4) do |method, args, chain, answers|
    if args != nil
      expect(target_array.map { |o| o.send(method, *args) }.map{ |o| o.try(chain) }).to eq answers
    else
      expect(target_array.map(&method).map{ |o| o.try(chain) }).to eq answers
    end
  end
end

# @param [Controller] controller_class コントローラクラス
# @param [Array] test_array 検証するデータセット
def privilege_test(controller_class, *array)
  it "#{controller_class.name} should have correct privileges" do
    array.each_slice(3) do |(action, permit_privileges, reject_privileges)|
      permit_privileges.each do |p|
        expect(controller_class.has_privileges?(action, { p => true })).to be_truthy
      end
      reject_privileges.each do |p|
         expect(controller_class.has_privileges?(action, { p => true })).to be_falsey
       end
    end
  end
end
