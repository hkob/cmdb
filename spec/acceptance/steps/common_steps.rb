# 共通 steps
# 参考: [RSpec][Turnip] 一般的に使えるTurnipステップ集
#       http://techracho.bpsinc.jp/hachi8833/2014_06_26/18068

## テスト用ステップ

# 表示テキスト存在確認
step %(:textと表示されていること) do |text|
  expect(page).to have_content(text)
end

# 指定行の表示テキスト存在確認
step %(:targetの行に:textと表示されていること) do |target, text|
  within :xpath, "//tr[td='#{target}']" do
    expect(page).to have_content(text)
  end
end

# 指定行の表示テキスト存在確認
step %(:targetがヘッダにある行に:textと表示されていること) do |target, text|
  within :xpath, "//tr[th='#{target}']" do
    expect(page).to have_content(text)
  end
end

#表示テキスト不在確認
step %(:textと表示されていないこと) do |text|
  expect(page).not_to have_content(text)
end

# 指定行の表示テキスト不在確認
step %(:targetの行に:textと表示されていないこと) do |target, text|
  within :xpath, "//tr[td='#{target}']" do
    expect(page).not_to have_content(text)
  end
end

## 操作用ステップ

#ページ移動
step %(:pageにアクセスする) do |page|
  visit case page
  when 'トップページ'
    root_path
  else
    page
  end
end

# 指定行のリンクをクリック (textはリンク文字列かid)
step %(:targetの行の:textリンクをクリックする) do |target, text|
  within :xpath, "//tr[td='#{target}']" do
    click_link text
    step '少し待つ'
  end
end

# 指定行のリンクをクリック (textはリンク文字列かid)
step %(:targetで始まる行の:textリンクをクリックする) do |target, text|
  within :xpath, "//tr[th='#{target}']" do
    click_link text
    step '少し待つ'
  end
end

#リンクをクリック (textはリンク文字列かid)
#同じ名前のリンクが複数ある場合には文字列は指定できない
step %(:textリンクをクリックする) do |text|
  click_link text
  step '少し待つ'
end

#同じ名前のリンクが複数ある場合に順序も指定
step %(:n番目の:textリンクをクリックする) do |n, text|
  n = n.to_i - 1
  all(:link_or_button, text)[n].click
  step '少し待つ'
end

#ボタンをクリック (textはボタン文字列かid)
#同じ名前のボタンが複数ある場合には文字列は指定できない
step %(:textボタンをクリックする) do |text|
  click_button text
  step '少し待つ'
end

# フィールドに文字列を入力する |fieldはモデル[属性]の形式
step %(:fieldに:valueを設定する) do |field, value|
  fill_in field, with: value
  step '少し待つ'
end

# フィールドにタブ区切り文字列を入力する |fieldはモデル[属性]の形式(＆をタブに変える)
step %(:fieldにタブ区切りの:valueを設定する) do |field, value|
  fill_in field, with: value.gsub('＆', "\t")
end

# フィールドから文字列を消す |fieldはモデル[属性]の形式
step %(:fieldのテキストを消す) do |field|
  fill_in field, with: nil
end

# ドロップダウンボックスを選択 (textはボタン文字列かid)
#同じ名前の項目が複数ある場合には文字列は指定できない
step %(:targetで始まる行のラジオボタンの:choiceを選択する) do |target, choice|
  within :xpath, "//tr[th='#{target}']" do
    choose choice
    step '少し待つ'
  end
end

# ドロップダウンボックスを選択 (textはボタン文字列かid)
#同じ名前の項目が複数ある場合には文字列は指定できない
step %(ラジオボタン:buttonの:choiceを選択する) do |button, choice|
  choose [ button, choice ].join('_')
  step '少し待つ'
end

# ドロップダウンボックスを選択 (textはボタン文字列かid)
#同じ名前の項目が複数ある場合には文字列は指定できない
step %(ラジオボタン:buttonの:choiceをクリックする) do |button, choice|
  find(:xpath, "//input[@id='#{button}_#{choice}']").click
  step '少し待つ'
end

# セレクトボックスを選択 (textはボタン文字列かid)
#同じ名前の項目が複数ある場合には文字列は指定できない
step %(:targetセレクタの:choiceを選択する) do |target, choice|
  within :xpath, "//select[@id='#{target}']" do
    select choice
    step '少し待つ'
  end
end

# チェックボックスを選択  (choiceは文字列かid)
# 同じ名前の項目が複数ある場合には文字列は指定できない
step %(:choiceをチェックする) do |choice|
  check choice
  step '少し待つ'
end

# チェックボックスを選択解除  (choiceは文字列かid)
# 同じ名前の項目が複数ある場合には文字列は指定できない
step %(:choiceのチェックを外す) do |choice|
  uncheck choice
  step '少し待つ'
end

## デバッグ用

# pryを呼ぶ (pry, pry-rails, pry-doc がインストールされていること)
step %(pryを呼び出す) do
  binding.pry
  puts ''
end

# その時点の静的なwebページを表示する
step %(表示する) do
  save_and_open_page
end

# その時点のwebページの画面キャプチャを撮る
step %(SC) do
  page.save_screenshot('screen.png', full: true)
end

# その時点のwebページの画面キャプチャを保存する
step %(画面を保存) do
  hash = Rails.application.routes.recognize_path(current_path)
  dir = "#{Rails.root}/doc/screenshot"
  FileUtils.mkdir_p dir
  path = "#{dir}/#{hash[:controller].gsub('/', '.')}##{hash[:action]}.png"
  page.save_screenshot(path, full: true) unless FileTest.exist?(path)
end

# ペンディング用
step %(:reasonという理由でペンディング) do |reason|
  pending reason
end

step %(少し待つ) do
  sleep 0.3
end
