require 'rails_helper'

RSpec.describe BandMember, :type => :model do
  context "common validation check" do
    before do
      @target = band_member_test_factory
    end

    check_presence_validates %i( band_id person_id instrumental_id )
    check_plural_unique_validates %i( band_id person_id instrumental_id ) do
      band_member_test2_factory
    end
    check_destroy_validates
    check_belongs_to :band_member, %i( band person instrumental )
    check_dependent_destroy :band_member, %i( band person instrumental )
  end

  context 'after all band_members are registrered' do
    before do
      @band_members = band_member_all_factories
    end

    context 'for BandMember class' do
      subject { BandMember }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for BandMember instances' do
      subject { @band_members }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
