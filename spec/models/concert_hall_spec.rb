require 'rails_helper'

RSpec.describe ConcertHall, :type => :model do
  context "common validation check" do
    before do
      @target = concert_hall_test_factory
    end

    check_presence_validates %i( concert_id sort_order hall_id event_date_id )
#    check_plural_unique_validates %i( concert_id sort_order ) do
#      concert_hall_test2_factory
#    end
#    check_destroy_validates
#    check_reject_destroy_validates
    check_belongs_to :concert_hall, %i( concert hall device song_list event_date )
    check_dependent_destroy :concert_hall, %i( concert hall device song_list event_date )
#    check_reject_destroy_for_relations :concert_hall, %i( has_many has_one )
#    check_destroy_nullify_for_relations :concert_hall, %i( has_many has_one )
  end

  context 'after all concert_halls are registrered' do
    before do
      @concert_halls = concert_hall_all_factories
    end

    context 'for ConcertHall class' do
      subject { ConcertHall }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for ConcertHall instances' do
      subject { @concert_halls }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
