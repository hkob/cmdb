require 'rails_helper'

RSpec.describe Song, :type => :model do
  context "common validation check" do
    before do
      @target = song_factory :new_season
    end

    check_presence_validates %i( title_id event_date_id )
    check_unique_validates %i( title_id ) do
      song_factory :yumeno_owari
    end
    check_destroy_validates
    check_belongs_to :song, %i( event_date ), %i( title )
    check_dependent_destroy :song, %i( title event_date )
  end

  context 'after all songs are registrered' do
    before do
      @songs = song_factories %i( new_season )
    end

    context 'for Song class' do
      subject { Song }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Song instances' do
      subject { @songs }

      it 'should receive METHOD3, METHOD4' do
        target_array_method_send_test subject,
          :name, true, [ 'NEW SEASON' ],
          :name, false, [ 'NEW SEASON' ]
      end
    end
  end
end
