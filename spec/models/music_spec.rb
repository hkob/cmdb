require 'rails_helper'

RSpec.describe Music, :type => :model do
  let(:song) { song_factory :new_season }
  context "common validation check" do
    before do
      @target = music_factory song: song, person_key: :iHideoSaito, sort_order: 0
    end

    check_presence_validates %i( song_id person_id sort_order )
    check_plural_unique_validates %i( song_id sort_order ) do
      music_factory song: song_factory(:new_season), person_key: :iChisatoMoritaka, sort_order: 1
    end
    check_destroy_validates
    check_belongs_to :music, %i( song person )
    check_dependent_destroy :music, %i( song person )
  end

  context 'after all musics are registrered' do
    before do
      @musics = music_all_factories
    end

    context 'for Music class' do
      subject { Music }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Music instances' do
      subject { @musics }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
