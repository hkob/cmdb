require 'rails_helper'

RSpec.describe Single, :type => :model do
  context "common validation check" do
    before do
      @target = single_test_factory
    end

    check_presence_validates %i( type device_type title_id sort_order year_id )
    check_destroy_validates
    check_belongs_to :single, %i( title year singer )
    check_dependent_destroy :single, %i( title year singer )
  end

  context 'after all singles are registrered' do
    before do
      @singles = [ single_test_factory ]
    end

    context 'for Single class' do
      subject { Single }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Single instances' do
      subject { @singles }

      it 'should receive name' do
        target_array_method_send_test subject,
          :name, true, [ 'NEW SEASON' ],
          :name, false, [ 'NEW SEASON' ],
          :show_path, true, @singles.map { |s| Rails.application.routes.url_helpers.single_path(s, locale: :ja) },
          :preview_path, false, @singles.map { |s| Rails.application.routes.url_helpers.preview_single_path(s, locale: :en) }
      end
    end
  end
end
