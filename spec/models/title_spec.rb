require 'rails_helper'

RSpec.describe Title, :type => :model do
  context "common validation check" do
    before do
      @target = title_factory :new_season
    end

    check_presence_validates %i( japanese english yomi )
    check_destroy_validates
  end

  context 'after all titles are registrered' do
    before do
      @titles = title_factories %i( new_season yumeno_owari )
    end

    context 'for Title class' do
      subject { Title }

      it 'should receive head_value_is' do
        target_method_send_test_with_chain subject,
          :head_value_is, '41', :count, 1,
          :head_value_is, '72', :count, 1,
          :head_value_is, '11', :count, 0
      end
    end

    context 'for Title instances' do
      subject { @titles }

      it 'should receive yomi_suuji, name' do
        target_array_method_send_test subject,
          :yomi_suuji, nil, %w( 4172212295.00010 726344049081.000000 ),
          :name, true, [ "NEW SEASON", "夢の終わり" ],
          :name, false, [ "NEW SEASON", "{YUME-NO OWARI}" ]
      end
    end
  end
end
