require 'rails_helper'

RSpec.describe Year, :type => :model do
  context "common validation check" do
    before do
      @target = year_factory 1987
    end

    check_presence_validates %i( year )
    check_unique_validates %i( year ) do
      year_factory 1988
    end
    check_destroy_validates
  end

  context 'after all years are registrered' do
    before do
      @years = year_factories %w( 1987 1988 )
    end

    context 'for Year class' do
      subject { Year }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Year instances' do
      subject { @years }

      it 'should receive name' do
        target_array_method_send_test subject,
          :name, true, %w( 1987年 1988年 ),
          :name, false, [ 'In 1987', 'In 1988' ]
      end
    end
  end
end
