require 'rails_helper'

RSpec.describe ConcertVideo, :type => :model do
  context "common validation check" do
    before do
      @target = concert_video_test_factory
    end

    check_presence_validates %i( concert_song_list_song_id video_song_list_song_id concert_hall_id )
#    check_unique_validates %i( ) do
#      concert_video_factory :other
#    end
#    check_plural_unique_validates %i( ) do
#      concert_video_factory :other
#    end
    check_destroy_validates
#    check_reject_destroy_validates
    check_belongs_to :concert_video, %i( concert_song_list_song ), [], :concert_to_concert_videos
    check_belongs_to :concert_video, %i( video_song_list_song ), [], :video_to_concert_videos
    check_belongs_to :concert_video, %i( concert_hall )
    check_dependent_destroy :concert_video, %i( concert_song_list_song video_song_list_song concert_hall )
#    check_reject_destroy_for_relations :concert_video, %i( has_many has_one )
#    check_destroy_nullify_for_relations :concert_video, %i( has_many has_one )
  end

  context 'after all concert_videos are registrered' do
    before do
      @concert_videos = concert_video_all_factories
    end

    context 'for ConcertVideo class' do
      subject { ConcertVideo }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for ConcertVideo instances' do
      subject { @concert_videos }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

