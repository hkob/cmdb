require 'rails_helper'

RSpec.describe EventDate, :type => :model do
  context "common validation check" do
    before do
      @target = event_date_test_factory
    end

    check_presence_validates %i( date year_id )
    check_destroy_validates
    check_belongs_to :event_date, %i( year )
    check_dependent_destroy :event_date, %i( year )
  end

  context 'after all event_dates are registrered' do
    before do
      @event_dates = [
        event_date_factory(date: '1987/5/25'),
        event_date_factory(date: '1988/7/25', no_day: true),
        event_date_factory(date: '1989/8/25', no_day: true, no_month: true)
      ]
    end

    context 'for EventDate class' do
      subject { EventDate }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for EventDate instances' do
      subject { @event_dates }

      it 'should receive year, name ' do
        target_array_method_send_test subject,
          :year, nil, [ 1987, 1988, 1989 ].map { |y| year_factory y },
          :name, nil, %w[ 1987年05月25日(月) 1988年07月 1989年日付不明 ]
      end
    end
  end
end

