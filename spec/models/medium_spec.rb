require 'rails_helper'

RSpec.describe Medium, :type => :model do
  context "common validation check" do
    before do
      @target = medium_test_factory
    end

    check_presence_validates %i( medium_device sort_order device_id )
#    check_unique_validates %i( ) do
#      medium_factory :other
#    end
#    check_plural_unique_validates %i( ) do
#      medium_factory :other
#    end
    check_destroy_validates
#    check_reject_destroy_validates
    check_belongs_to :medium, %i( device company ), %i( )
#    check_dependent_destroy :medium, %i( has_many has_one )
#    check_reject_destroy_for_relations :medium, %i( has_many has_one )
#    check_destroy_nullify_for_relations :medium, %i( has_many has_one )
  end

  context 'after all media are registrered' do
    before do
      @media = medium_all_factories
    end

    context 'for Medium class' do
      subject { Medium }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Medium instances' do
      subject { @media }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
