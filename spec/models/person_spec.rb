require 'rails_helper'

RSpec.describe Person, :type => :model do
  context "common validation check" do
    before do
      @target = person_factory :iChisatoMoritaka
    end

    check_presence_validates %i( title_id )
    check_unique_validates %i( title_id ) do
      person_factory :iHideoSaito
    end
    check_destroy_validates
    check_belongs_to :person, %i( ), %i( title )
    check_dependent_destroy :person, %i( title )
  end

  context 'after all people are registrered' do
    before do
      @people = person_factories %i( iChisatoMoritaka iHideoSaito iHiro )
    end

    context 'for Person class' do
      subject { Person }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Person instances' do
      subject { @people }

      it 'should receive name, names' do
        target_array_method_send_test subject,
          :name, true, %w( 森高千里 斉藤英夫 HIRO ),
          :name, false, [ 'Chisato Moritaka', 'Hideo Saito', 'HIRO' ],
          :names, true, [ '森高千里', '斉藤英夫', '伊秩弘将, HIRO' ],
          :names, false, [ 'Chisato Moritaka', 'Hideo Saito', 'Hiromasa Ijichi, HIRO' ]
      end
    end
  end
end
