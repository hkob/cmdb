require 'rails_helper'

RSpec.describe Band, :type => :model do
  context "common validation check" do
    before do
      @target = band_factory :animals_1
    end

    check_presence_validates %i( title_id )
    check_unique_validates %i( title_id ) do
      band_factory :animals_2
    end
#    check_plural_unique_validates %i( ) do
#      band_factory :other
#    end
    check_destroy_validates
#    check_reject_destroy_validates
    check_belongs_to :band, %i( ), %i( title )
    check_dependent_destroy :band, %i( title )
#    check_reject_destroy_for_relations :band, %i( has_many has_one )
#    check_destroy_nullify_for_relations :band, %i( has_many has_one )
  end

  context 'after all bands are registrered' do
    before do
      @bands = band_factories %i( animals_1 )
    end

    context 'for Band class' do
      subject { Band }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Band instances' do
      subject { @bands }

      it 'should receive name' do
        target_array_method_send_test subject,
          :name, true, [ 'ANIMALS (1)' ],
          :name, false, [ 'ANIMALS (1)' ]
      end
    end
  end
end

