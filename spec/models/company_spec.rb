require 'rails_helper'

RSpec.describe Company, :type => :model do
  context "common validation check" do
    before do
      @target = company_factory :warner_pioneer
    end

    check_presence_validates %i( title_id)
#    check_unique_validates %i( ) do
#      company_factory :other
#    end
#    check_plural_unique_validates %i( ) do
#      company_factory :other
#    end
    check_destroy_validates
#    check_reject_destroy_validates
#    check_belongs_to :company, %i( has_many ), %i( has_one )
#    check_dependent_destroy :company, %i( has_many has_one )
#    check_reject_destroy_for_relations :company, %i( has_many has_one )
#    check_destroy_nullify_for_relations :company, %i( has_many has_one )
  end

  context 'after all companies are registrered' do
    before do
      @companies = company_factories [ :warner_pioneer ]
    end

    context 'for Company class' do
      subject { Company }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Company instances' do
      subject { @companies }

      it 'should receive name' do
        target_array_method_send_test subject,
          :name, true, [ 'Warner Pioneer' ],
          :name, false, [ 'Warner Pioneer' ]
      end
    end
  end
end
