require 'rails_helper'

RSpec.describe SongListSong, :type => :model do
  context "common validation check" do
    before do
      @target = song_list_song_test_factory
    end

    check_presence_validates %i( song_list_id sort_order )
#    check_unique_validates %i( ) do
#      song_list_song_factory :other
#    end
    check_plural_unique_validates %i( song_list_id sort_order ) do
      song_list_song_test2_factory
    end
    check_destroy_validates
#    check_reject_destroy_validates
    check_belongs_to :song_list_song, %i( song_list song )
#    check_dependent_destroy :song_list_song, %i( song_list song )
#    check_reject_destroy_for_relations :song_list_song, %i( has_many has_one )
#    check_destroy_nullify_for_relations :song_list_song, %i( has_many has_one )
  end

  context 'after all song_list_songs are registrered' do
    before do
      @song_list_songs = song_list_song_all_factories
    end

    context 'for SongListSong class' do
      subject { SongListSong }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for SongListSong instances' do
      subject { @song_list_songs }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

