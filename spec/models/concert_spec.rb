require 'rails_helper'

RSpec.describe Concert, :type => :model do
  context "common validation check" do
    before do
      @target = concert_test_factory
    end

    check_presence_validates %i( title_id concert_type from_id has_song_list has_product num_of_performances num_of_halls sort_order )
#    check_unique_validates %i( ) do
#      concert_factory :concert_test2_factory
#    end
    check_plural_unique_validates %i( concert_type sort_order) do
      concert_test2_factory
    end
    check_destroy_validates
#    check_reject_destroy_validates
    check_belongs_to :concert, %i( band ), %i( title )
    check_belongs_to :concert, %i( from ), [], :concert_froms
    check_belongs_to :concert, %i( to ), [], :concert_tos
    check_dependent_destroy :concert, %i( band title from to )
#    check_reject_destroy_for_relations :concert, %i( has_many has_one )
#    check_destroy_nullify_for_relations :concert, %i( has_many has_one )
  end

  context 'after all concerts are registrered' do
    before do
      @concerts = [ concert_test_factory, concert_test2_factory ]
    end

    context 'for Concert class' do
      subject { Concert }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Concert instances' do
      subject { @concerts }

      it 'should receive name' do
        target_array_method_send_test subject,
          :name, true, [
            '「OVERHEAT. NIGHT」, 「GET SMILE」',
            '渋谷ライブイン ファーストライブ'
          ],
          :name, false, [
            '"OVERHEAT. NIGHT",  "GET SMILE"',
            'First Live at Shibuya Live-in'
          ],
          :num_of_concerts, nil, %w( 3(2) 1(1) )
      end
    end
  end
end

