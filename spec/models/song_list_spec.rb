require 'rails_helper'

RSpec.describe SongList, :type => :model do
  context "common validation check" do
    before do
      @target = song_list_test_factory
    end

    check_presence_validates %i( keyword )
    check_destroy_validates
    check_belongs_to :song_list, %i( single )
    check_dependent_destroy :song_list, %i( single )
  end

  context 'after all song_lists are registrered' do
    before do
      @song_lists = song_list_all_factories
    end

    context 'for SongList class' do
      subject { SongList }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for SongList instances' do
      subject { @song_lists }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

