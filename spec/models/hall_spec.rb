require 'rails_helper'

RSpec.describe Hall, :type => :model do
  context "common validation check" do
    before do
      @target = hall_factory :shibuya_live_in
    end

    check_presence_validates %i( title_id prefecture_id sort_order )
    check_unique_validates %i( title_id ) do
      hall_factory :osaka_kousei_nenkin_kaikan
    end
    check_plural_unique_validates %i( prefecture_id sort_order ) do
      hall_factory :osaka_kousei_nenkin_kaikan
    end
    check_destroy_validates
#    check_reject_destroy_validates
    check_belongs_to :hall, %i( prefecture ), %i( title )
    check_dependent_destroy :hall, %i( title prefecture )
#    check_reject_destroy_for_relations :hall, %i( has_many has_one )
#    check_destroy_nullify_for_relations :hall, %i( has_many has_one )
  end

  context 'after all halls are registrered' do
    before do
      @halls = hall_factories %i( shibuya_live_in )
    end

    context 'for Hall class' do
      subject { Hall }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Hall instances' do
      subject { @halls }

      it 'should receive name' do
        target_array_method_send_test subject,
          :name, true, %w( 渋谷ライブイン),
          :name, false, [ 'Shibuya Live-in' ]
      end
    end
  end
end

