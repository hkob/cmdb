require 'rails_helper'

RSpec.describe SongPerson, :type => :model do
  context "common validation check" do
    before do
      @target = song_person_test_factory
    end

    check_presence_validates %i( song_list_song_id person_id instrumental_id )
    check_destroy_validates
    check_belongs_to :song_person, %i( song_list_song person instrumental )
    check_dependent_destroy :song_person, %i( song_list_song person instrumental )
  end

  context 'after all song_people are registrered' do
    before do
      @song_people = song_person_all_factories
    end

    context 'for SongPerson class' do
      subject { SongPerson }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for SongPerson instances' do
      subject { @song_people }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
