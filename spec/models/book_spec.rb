require 'rails_helper'

RSpec.describe Book, :type => :model do
  context "common validation check" do
    before do
      @target = book_test_factory
    end

    check_presence_validates %i( book_type title_id sort_order )
#    check_unique_validates %i( ) do
#      book_factory :other
#    end
    check_plural_unique_validates %i( book_type sort_order ) do
      book_test2_factory
    end
    check_destroy_validates
    check_belongs_to :book, %i( event_date )
    check_belongs_to :book, %i( photographer ), [], :photographed_books
    check_belongs_to :book, %i( author ), [], :written_books
    check_belongs_to :book, %i( publisher ), [], :published_books
    check_belongs_to :book, %i( seller ), [], :selled_books
    check_dependent_destroy :book, %i( publisher seller photographer author )
#    check_reject_destroy_for_relations :book, %i( has_many has_one )
#    check_destroy_nullify_for_relations :book, %i( has_many has_one )
  end

  context 'after all books are registrered' do
    before do
      @books = [ book_test_factory ]
    end

    context 'for Book class' do
      subject { Book }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Book instances' do
      subject { @books }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

