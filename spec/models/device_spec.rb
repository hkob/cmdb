require 'rails_helper'

RSpec.describe Device, :type => :model do
  context "common validation check" do
    before do
      @target = device_factory :device
    end

#    check_presence_validates %i( )
#    check_unique_validates %i( ) do
#      device_factory :other
#    end
#    check_plural_unique_validates %i( ) do
#      device_factory :other
#    end
#    check_destroy_validates
#    check_reject_destroy_validates
#    check_belongs_to :device, %i( has_many ), %i( has_one )
#    check_dependent_destroy :device, %i( has_many has_one )
#    check_reject_destroy_for_relations :device, %i( has_many has_one )
#    check_destroy_nullify_for_relations :device, %i( has_many has_one )
  end

  context 'after all devices are registrered' do
    before do
      @devices = device_all_factories
    end

    context 'for Device class' do
      subject { Device }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Device instances' do
      subject { @devices }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
