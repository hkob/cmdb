require 'rails_helper'

RSpec.describe Instrumental, :type => :model do
  context "common validation check" do
    before do
      @target = instrumental_factory :VOCAL
    end

    check_presence_validates %i( sort_order title_id )
    check_unique_validates %i( sort_order title_id ) do
      instrumental_factory :GUITAR
    end
    check_destroy_validates
    check_belongs_to :instrumental, %i( ), %i( title )
    check_dependent_destroy :instrumental, %i( title )
  end

  context 'after all instrumentals are registrered' do
    before do
      @instrumentals = instrumental_factories %i( VOCAL ARRANGE )
    end

    context 'for Instrumental class' do
      subject { Instrumental }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Instrumental instances' do
      subject { @instrumentals }

      it 'should receive name' do
        target_array_method_send_test subject,
          :name, true, %w( Vocal 編曲 ),
          :name, false, %w( Vocal Arrangement )
      end
    end
  end
end
