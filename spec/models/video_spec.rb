require 'rails_helper'

RSpec.describe Video, :type => :model do
  context "common validation check" do
    before do
      @target = video_test_factory
    end

    check_presence_validates %i( type device_type title_id sort_order year_id )
    check_destroy_validates
    check_belongs_to :video, %i( title year singer )
    check_dependent_destroy :video, %i( title year singer )
  end

  context 'after all videos are registrered' do
    before do
      @videos = video_all_factories
    end

    context 'for Video class' do
      subject { Video }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Video instances' do
      subject { @videos }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
