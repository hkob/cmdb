require 'rails_helper'

RSpec.describe Activity, :type => :model do
  context "common validation check" do
    before do
      @target = activity_test_factory
    end

    check_presence_validates %i( activity_type title_id sort_order)
#    check_unique_validates %i( ) do
#      activity_factory :other
#    end
    check_plural_unique_validates %i( activity_type sort_order ) do
      activity_test2_factory
    end
    check_destroy_validates
#    check_reject_destroy_validates
    check_belongs_to :activity, %i( title company song )
    check_belongs_to :activity, %i( from ), [], :activity_froms
    check_belongs_to :activity, %i( to ), [], :activity_tos
    check_dependent_destroy :activity, %i( title company song from to )
#    check_reject_destroy_for_relations :activity, %i( has_many has_one )
#    check_destroy_nullify_for_relations :activity, %i( has_many has_one )
  end

  context 'after all activities are registrered' do
    before do
      @activities = activity_all_factories
    end

    context 'for Activity class' do
      subject { Activity }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Activity instances' do
      subject { @activities }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

