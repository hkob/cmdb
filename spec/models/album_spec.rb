require 'rails_helper'

RSpec.describe Album, :type => :model do
  context "common validation check" do
    before do
      @target = album_test_factory
    end

    check_presence_validates %i( type device_type title_id sort_order event_date_id )
    check_destroy_validates
    check_belongs_to :album, %i( title event_date singer )
    check_dependent_destroy :album, %i( title event_date singer )
  end

  context 'after all albums are registrered' do
    before do
      @albums = album_all_factories
    end

    context 'for Album class' do
      subject { Album }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Album instances' do
      subject { @albums }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end
