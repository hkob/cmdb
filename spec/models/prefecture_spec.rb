require 'rails_helper'

RSpec.describe Prefecture, :type => :model do
  context "common validation check" do
    before do
      @target = prefecture_factory :Tokyo
    end

    check_presence_validates %i( title_id j_capital e_capital region sort_order )
    check_unique_validates %i( sort_order title_id ) do
      prefecture_factory :Chiba
    end
#    check_plural_unique_validates %i( ) do
#      prefecture_factory :other
#    end
    check_destroy_validates
#    check_reject_destroy_validates
    check_belongs_to :prefecture, %i( ), %i( title )
#    check_dependent_destroy :prefecture, %i( has_many has_one )
#    check_reject_destroy_for_relations :prefecture, %i( has_many has_one )
#    check_destroy_nullify_for_relations :prefecture, %i( has_many has_one )
  end

  context 'after all prefectures are registrered' do
    before do
      @prefectures = prefecture_all_factories
    end

    context 'for Prefecture class' do
      subject { Prefecture }

#      it 'should receive METHOD1, METHOD2' do
#        target_method_send_test subject,
#          :METHOD1, :ARG1, :ANS1,
#          :METHOD2, :ARG2, :ANS2
#      end
    end

    context 'for Prefecture instances' do
      subject { @prefectures }

#      it 'should receive METHOD3, METHOD4' do
#        target_array_method_send_test subject,
#          :METHOD3?, :ARG3, :ANS3,
#          :METHOD4?, :ARG4, :ANS4
#      end
    end
  end
end

