FG = FactoryGirl

# @see https://gist.github.com/thewatts/dcc91ef40b144aff42ae
module FactoryGirl::Syntax::Methods
  def find_or_create(name, attributes = {}, &block)
    factory = FG.factory_by_name(name)
    klass   = factory.build_class

    factory_attributes = FG.attributes_for(name)
    attributes = factory_attributes.merge(attributes)

    result = klass.find_by(attributes, &block)

    result || FG.create(name, attributes, &block)
  end
end

