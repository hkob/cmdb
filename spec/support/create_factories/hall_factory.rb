##### write to spec/support/create_factories/hall_factory.rb
HallFactoryHash = {
  #### 北海道
  sapporo_messe_hall: [ :Hokkaidou, 1, '札幌メッセホール', 'Sapporo Messe Hall', 'さっぽろめっせほーる' ],
  sapporo_doshin_hall: [ :Hokkaidou, 2, '札幌道新ホール', 'Sapporo DOSHIN Hall', 'さっぽろどうしんほーる' ],
  #### 宮城
  sendai_moning_moon: [ :Miyagi, 1, '仙台モーニングムーン', 'Sendai Morning Moon', 'せんだいもーにんぐむーん' ],
  sendai_shimin_kaikan: [ :Miyagi, 2, '仙台市民会館', 'Sendai SHIMIN KAIKAN', 'せんだいしみんかいかん' ],
  #### 埼玉
  omiya_sonic_city: [ :Saitama, 1, '大宮ソニックシティ', 'Omiya Sonic City', 'おおみやそにっくしてぃ' ],
  toyo_university: [ :Saitama, 2, '東洋大学', 'Toyo university', 'とうようだいがく' ],
  #### 神奈川
  enoshima_seaside: [ :Kanagawa, 1, '江ノ島シーサイド', 'Enoshima sea side', 'えのしましーさいど' ],
  mukogaoka_yuen: [ :Kanagawa, 2, '向ヶ丘遊園', 'Mukogaoka-Yuen', 'むこうがおかゆうえん' ],
  club_citta_kawasaki: [ :Kanagawa, 3, 'クラブチッタ川崎', "CLUB CITTA' Kawasaki", 'くらぶちったかわさき' ],
  miura_kaigan: [ :Kanagawa, 4, '三浦海岸', 'Miura Kaigan', 'みうらかいがん' ],
  #### 千葉
  teikyo_technical_college: [ :Chiba, 1, '帝京技術大学', 'Teikyo technical college', 'ていきょうぎじゅつだいがく' ],
  #### 東京
  shibuya_live_in: [ :Tokyo, 1, '渋谷ライブイン', 'Shibuya Live-in', 'しぶやらいぶいん' ],
  nihon_seinenkan: [ :Tokyo, 2, '日本青年館', 'Nippon-seinenkan', 'にほんせいねんかん' ],
  instick_shibaura_factory: [ :Tokyo, 3, 'INKSTICK 芝浦 FACTORY', 'INKSTICK Shibaura FACTORY', 'いんすてぃっく　しばうら　ふぁくとりー' ],
  hibiya_yagai_ongakudo: [ :Tokyo, 4, '日比谷野外音楽堂', 'Hibiya YAGAI ONGAKU-DO(bandstand)', 'ひびややがいおんがくどう' ],
  ariake_mza_convention: [ :Tokyo, 5, '有明MZAコンベンション', 'Ariake MZA Convention', 'ありあけえむざこんべんしょん' ],
  shibuya_koukado: [ :Tokyo, 6, '渋谷公会堂', 'Shibuya KOKAIDO', 'しぶやこうかいどう' ],
  StMargaretsSchool: [ :Tokyo, 7, '立教女学院', "St Margaret's School", 'りっきょうじょがくえん' ],
  tokyo_metropolitan_institute_of_technology: [ :Tokyo, 8, '東京都立科学技術大学', 'Tokyo Metropolitan Institute of Technology', 'とうきょうとりつかがくぎじゅつだいがく' ],
  shiodome_pit_ii: [ :Tokyo, 9, '汐留 PIT II', 'Shiodome PIT II', 'しおどめぴっとつう' ],
  nakano_sunplaza: [ :Tokyo, 10, '中野サンプラザ', 'Nakano Sunplaza', 'なかのさんぷらざ' ],
  #### 長野
  nagano_live_house: [ :Nagano, 1, '長野市内ライブハウス', 'Live House in the Nagano city', 'ながのしないらいぶはうす' ],
  shinshu_university: [ :Nagano, 2, '信州大学', 'Shinsyu university', 'しんしゅうだいがく' ],
  nagano_kenmin_bunka_kaikan: [ :Nagano, 3, '長野県民文化会館', 'Nagano KENMIN BUNKA KAIKAN', 'ながのけんみんぶんかかいかん' ],
  #### 新潟
  niigata_kenmin_kaikan: [ :Niigata, 1, '新潟県民会館', 'Niigata Prefectural Civic Center', 'にいがたけんみんかいかん' ],
  niigata_university_of_phermacy: [ :Niigata, 2, '新潟薬科大学', 'Nigata University of Phermacy and Applied Life Sciences', 'にいがたやっかだいがく' ],
  niigata_sangyo_university: [ :Niigata, 3, '新潟産業大学', 'Niigata Sangyo University', 'にいがたさんぎょうだいがく' ],
  #### 富山
  takaoka_national_college: [ :Toyama, 1, '高岡短期大学', 'Takaoka National College', 'たかおかたんきだいがく' ],
  #### 石川
  kanazawa_university: [ :Ishikawa, 1, '金沢大学', 'Kanazawa university', 'かなざわだいがく' ],
  #### 愛知
  nagoya_heart_land: [ :Aichi, 1, '名古屋ハートランド', 'Nagaya Heart Land', 'なごやはーとらんど' ],
  toyohashi_university_of_technology: [ :Aichi, 2, '豊橋科学技術大学', 'Toyohashi University of Technology', 'とよはしかがくぎじゅつだいがく' ],
  nagoya_kinrou_kaikan: [ :Aichi, 3, '名古屋勤労会館', 'Nagoya KINRO KAIKAN', 'なごやきんろうかいかん' ],
  nagoya_university: [ :Aichi, 4, '名古屋大学', 'Nagoya University', 'なごやだいがく' ],
  aichi_kinrou_kaikan: [ :Aichi, 5, '愛知勤労会館', 'Aichi KINRO KAIKAN', 'あいちきんろうかいかん' ],
  #### 滋賀
  shiga_university: [ :Shiga, 1, '滋賀大学', 'Shiga university', 'しがだいがく' ],
  #### 大阪
  osaka_kousei_nenkin_kaikan: [ :Osaka, 1, '大阪厚生年金会館', 'Osaka KOSEI NENKIN KAIKAN', 'おおさかこうせいねんきんかいかん' ],
  #### 広島
  hiroshima_kamagari: [ :Hiroshima, 1, '広島蒲苅', 'Hiroshima Kamagari', 'ひろしまかまがり' ],
  #### 愛媛
  ehime_bunka_koudou: [ :Ehime, 1, '愛媛文化講堂', 'Ehime BUNKA KODO', 'えひめぶんかこうどう' ],
  #### 徳島
  tokushima_vega_hall: [ :Tokushima, 1,  '徳島ベガホール', 'Tokushima Vega hall', 'とくしまべがほーる' ],
  #### 福岡
  fukuoka_vivre: [ :Fukuoka, 1, '福岡ビブレ', 'Fukuoka VIVRE', 'ふくおかびぶれ' ],
  kyushu_institute_technology: [ :Fukuoka, 2, '九州工業大学', 'Kyusyu institute of technology', 'きゅうしゅうこうぎょうだいがく' ],
  fukuoka_tsukushi_kaikan: [ :Fukuoka, 3, '福岡都久志会館', 'Fukuoka Tsukushi KAIKAN', 'ふくおかつくしかいかん' ],
  #### 長崎
  nagasaki_nbc_hall: [ :Nagasaki, 1, '長崎NBCホール', 'Nagasaki NBC Hall', 'ながさきえぬびーしーほーる' ],
  nagasaki_heiwa_kaikan: [ :Nagasaki, 2, '長崎平和会館', 'Nagasaki Peace Hall', 'ながさきへいわかいかん' ],
  #### 熊本
  kumamoto_yubinchokin_hall: [ :Kumamoto, 1, '熊本郵便貯金ホール', 'Kumamoto YUBINCHOKIN Hall', 'くまもとゆうびんちょきんほーる' ],
  yatsushiro_kousei_nenkin_kaikan: [ :Kumamoto, 2, '八代厚生年金会館', 'Yatsushiro {KOSEI NENKIN KAIKAN}', 'やつしろこうせんねんきんかいかん' ],
  kumamoto_techno_research_park: [ :Kumamoto, 3, '熊本テクノリサーチパーク', 'Kumamoto techno research park', 'くまもとてくのりさーちぱーく' ],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Hall] Hall FactoryGirl オブジェクト
def hall_factory(key)
  pr, so, jt, et, yo = HallFactoryHash[key.to_sym]
  title = title_factory_with_values jt, et, yo
  prefecture = prefecture_factory pr
  FG.find_or_create(
    :hall,
    title_id: title.id,
    prefecture_id: prefecture.id,
    sort_order: so.to_i) if jt
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Hall>] Hall FactoryGirl オブジェクトの配列
def hall_factories(keys)
  keys.map { |k| hall_factory(k) }
end

