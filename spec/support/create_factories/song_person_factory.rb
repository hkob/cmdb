# @param [Hash] hash
# @option hash [SongListSong] song_list_song ソングリスト曲
# @option hash [Fixnum] person_key 関係者を一意に確定する key
# @option hash [Person] person 関係者
# @option hash [Fixnum] instrumental_key 楽器を一意に確定する key
# @option hash [Instrumental] instrumental 楽器
# @return [SongPerson] SongPerson FactoryGirl オブジェクト
def song_person_factory(song_list_song: nil, person_key: nil, person: nil, instrumental_key: nil, instrumental: nil)
  person ||= person_factory(person_key)
  instrumental ||= instrumental_factory(instrumental_key)
  p person_key unless person
  p instrumental_key unless instrumental
  FG.find_or_create(
    :song_person,
    song_list_song_id: song_list_song.id,
    person_id: person.id,
    instrumental_id: instrumental.id)
end

# @return [SongPerson] SongPerson FactoryGirl オブジェクト
def song_person_test_factory
  song_person_factory song_list_song: song_list_song_test_factory, person_key: :iChisatoMoritaka, instrumental_key: :VOCAL
end

