# @param [Hash] hash
# @option hash [String] medium_device メディアデバイス
# @option hash [String] code コード
# @option hash [String] release リリース
# @option hash [Boolean] now_sale 現在発売中なら true
# @option hash [String] company_key 会社を一意に確定する key
# @option hash [Company] company 会社
# @option hash [String] price 価格
# @option hash [Fixnum] 並び順
# @option hash [Device] デバイス
# @return [Medium] Medium FactoryGirl オブジェクト
def medium_factory(medium_device: nil, code: nil, release: nil, now_sale: nil, company_key: nil, company: nil, price: nil, sort_order: nil, device: nil)
  ans = Medium.get_object(device, sort_order)
  unless ans
    company ||= company_factory company_key
    ans = FG.find_or_create(
      :medium,
      medium_device: medium_device,
      code: code,
      release: release,
      now_sale: now_sale,
      company_id: company.id,
      price: price,
      sort_order: sort_order,
      device_id: device.id)
  end
  ans
end

# @return [Medium] Medium FactoryGirl オブジェクト
def medium_test_factory
  medium_factory medium_device: :ep, code: 'K-1564', release: '1st', now_sale: false, company_key: :warner_pioneer, price: '700', sort_order: 1, device: single_test_factory
end
