# @param [Hash] hash
# @option hash [String] device_type デバイスタイプ
# @option hash [Symbol] title_key タイトルの key
# @option hash [String] date 発売日
# @option hash [String] minutes 分
# @option Hash [String] seconds 秒
# @option Hash [Fixnum] sort_order 並び順
# @option Hash [String] number 通番
# @option Hash [Singer] singer_key 歌手の key
# @option Hash [Person] singer 歌手
# @option Hash [String] j_comment 日本語コメント
# @option Hash [String] e_comment 英語コメント
# @option Hash [String] link 外部リンク
# @return [Single] Single FactoryGirl オブジェクト
def single_factory(title_key: nil, device_type: nil, date: nil, no_month: nil, no_day: nil, minutes: nil, seconds: nil, sort_order: nil, number: nil, singer_key: nil, singer: nil, j_comment: nil, e_comment: nil, link: nil, add_device_type: [])
  dtv = Single::SingleStr2Num[device_type]
  add_device_type.each do |dt|
    dtv += Single::SingleStr2Num[dt]
  end
  ans = Single.get_object(dtv, sort_order)
  unless ans
    event_date = event_date_factory date: date, no_month: no_month, no_day: no_day
    title = title_factory title_key
    singer ||= singer_key && person_factory(singer_key)
    ans = FG.find_or_create(
      :single,
      title_id: title.id,
      device_type: dtv,
      event_date_id: event_date.id,
      minutes: minutes,
      seconds: seconds,
      sort_order: sort_order.to_i,
      number: number,
      singer_id: singer.id,
      j_comment: j_comment,
      e_comment: e_comment,
      link: link)
  end
  ans
end

# @return [Single] Single FactoryGirl オブジェクト
def single_test_factory
  single_factory title_key: :new_season, device_type: :ep_single, date: '1987/5/25', sort_order: 1, number: '1st', singer_key: :iChisatoMoritaka, j_comment: 'デビューシングル', e_comment: 'Debut Single'
end
