CompanyFactoryHash = {
  warner_music_japan: [ 'Warner Music Japan', 'Warner Music Japan', 'わーなーみゅーじっくじゃぱん' ],
  warner_pioneer: [ 'Warner Pioneer', 'Warner Pioneer', 'わーなーぱいおにあ' ],
  kindai_eigasha: [ '近代映画社', 'Kindaieigasha', 'きんだいえいがしゃ' ],
  kodansha: [ '講談社', 'Kodansha', 'こうだんしゃ' ],
  fuji_tv: [ 'フジテレビ', 'Fuji Television', 'ふじてれび' ],
  otsuka_seiyaku: [ '大塚製薬', 'Otsuka Pharmaceutical Company', 'おおつかせいやく' ],
  nhk: [ '日本放送協会', 'NHK', 'にほんほうそうきょうかい' ],
  pioneer: [ 'パイオニア', 'PIONEER', 'ぱいおにあ' ],
  kadokawa_shoten: [ '角川書店', 'Kadokawa Shoten', 'かどかわしょてん' ],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Company] Company FactoryGirl オブジェクト
def company_factory(key)
  jt, et, yo = CompanyFactoryHash[key.to_sym]
  title = title_factory_with_values jt, et, yo
  FG.find_or_create(
    :company,
    title_id: title.id)
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Company>] Company FactoryGirl オブジェクトの配列
def company_factories(keys)
  keys.map { |k| company_factory(k) }
end

