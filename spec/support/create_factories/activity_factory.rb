# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Activity] Activity FactoryGirl オブジェクト
def activity_factory(activity_type: nil, j_title: nil, e_title: nil, yomi: nil, company_key: nil, song_key: nil, from: nil, from_no_month: false, from_no_day: false, to: nil, to_no_month: false, to_no_day: false, j_comment: nil, e_comment: nil, sort_order: nil)
  ans = Activity.activity_type_str_is(activity_type).sort_order_value_is(sort_order).take
  unless ans
    title = j_title && title_factory_with_values(j_title, e_title, yomi)
    from_obj = from && event_date_factory(date: from, no_month: from_no_month, no_day: from_no_day)
    to_obj = to && event_date_factory(date: to, no_month: to_no_month, no_day: to_no_day)
    ans = FG.find_or_create(
      :activity,
      activity_type: Activity.activity_types[activity_type],
      title_id: title.try(:id),
      company_id: company_key && company_factory(company_key).id,
      song_id: song_key && song_factory(song_key).id,
      from_id: from_obj.try(:id),
      to_id: to_obj.try(:id),
      j_comment: j_comment,
      e_comment: e_comment,
      sort_order: sort_order
    )
  end
  ans
end

def activity_test_factory
  activity_factory activity_type: :regular_tv, j_title: 'TVハッカー', e_title: 'TV Hacker', yomi: 'てれびはっかー', company_key: :fuji_tv, from: '1987/4/19', to: '1987/9/27', j_comment: '毎週日曜 20:00 - 20:54', e_comment: 'Every Sunday, 20:00 - 20:54', sort_order: 1, song_key: :new_season
end

def activity_test2_factory
  activity_factory activity_type: :cm, j_title: '大塚製薬 「ポカリスエット」', e_title: "Otsuka Pharmaceutical Company `Pocarisweat'", yomi: 'おおつかせいやく　ぽかりすえっと', company_key: :otsuka_seiyaku, from: '1986/10/1', to: '1988/9/30', j_comment: '和服編|寝室編|電車編|自動販売機編|骨董店編|レストラン編|温泉編', e_comment: 'Japanese custume version|Bed room version|Train version|{JIDOHANBAIKI} version|{KOTTOHIN-TEN} version|Restaurant version|{ONSEN} version', sort_order: 2
end
