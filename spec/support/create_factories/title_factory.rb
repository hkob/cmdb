TitleFactoryHash = {
  ##### Book
  shuka: [ '朱夏 NEW SEASON', '{SHUKA} NEW SEASON', 'しゅか　にゅーしーずん' ],
  wakariyasui_koi: [ 'わかりやすい恋', '{WAKARIYASUI KOI}', 'わかりやすいこい' ],
  rikutsuja_nai: [ 'りくつじゃない', '{RIKUTSU-JA NAI}', 'りくつじゃない' ],
  ##### Song
  new_season: [ 'NEW SEASON', 'NEW SEASON', 'にゅーしーずん' ],
  period: %w( ピリオド Period ぴりおど ),
  namida_good_bye: [ '涙 Good-bye', '{NAMIDA} Good-bye', 'なみだぐっぱい' ],
  yumeno_owari: [ '夢の終わり', '{YUME-NO OWARI}', 'ゆめのおわり' ],
  ringoshuno_rule: [ '林檎酒のルール', '{RINGOSHU-NO rule}', 'りんごしゅのるーる' ],
  otisreddingni_kanpai: [ 'オーティスレディングに乾杯', 'OtisRedding {NI KANPAI}', 'おーてぃすれでぃんぐにかんぱい' ],
  ways: %w( WAYS WAYS うえいず ),
  anohino_photograph: [ 'あの日のフォトグラフ', '{ANOHI-NO} photograph', 'あのひのふぉとぐらふ' ],
  miss_lady: [ 'Miss Lady', 'Miss Lady', 'みすれいでぃ' ],
  weekend_blue: [ 'WEEKEND BLUE', 'WEEKEND BLUE', 'うぃーくえんどぶるー' ],
  overheat_night: [ 'オーバーヒート・ナイト', 'OVERHEAT. NIGHT', 'おーばーひーとないと' ],
  overheat_night_2: [ 'OVERHEAT. NIGHT', 'OVERHEAT. NIGHT', 'おーばーひーとないと' ],
  good_bye_season: [ 'GOOD-BYE SEASON', 'GOOD-BYE SEASON', 'ぐっばいしーずん' ],
  get_smile: [ 'GET SMILE', 'GET SMILE', 'げっとすまいる' ],
  yokohama_one_night: [ 'YOKOHAMA ONE NIGHT', 'YOKOHAMA ONE NIGHT', 'よこはまわんないと' ],
  cant_say_good_bye: [ "CAN'T SAY GOOD-BYE", "CAN'T SAY GOOD-BYE", 'きゃんとせいぐっばい' ],
  pi_a_no: %w( PI-A-NO PI-A-NO ぴあの ),
  forty_seven_har_nights: [ '47 HARD NIGHTS', '47 HARD NIGHTS', 'ふぉーてぃせぶんはーどないつ' ],
  kiss_the_night: [ 'KISS THE NIGHT', 'KISS THE NIGHT', 'きすざないと' ],
  mi_ha_: %w( ミーハー Mi-HA- みーはー ),
  the_mi_ha_: [ 'ザ・ミーハー', 'The Mi-HA-', 'ざ　みーはー' ],
  romantic: %w( ロマンティック Romantic ろまんてぃっく ),
  do_you_know_the_way_to_san_jose: [ 'サンホセへの道 Do you know the way to San Jose', 'Do you know the way to San Jose', 'さんほせへのみち' ],
  kaigan: %w( 海岸 {KAIGAN} かいがん ),
  shizukana_natsu: [ '静かな夏', '{SHIZUKA-NA NATU}', 'しずかななつ' ],
  stress: %w( ストレス Stress すとれす ),
  mite: %w( 見て {MITE} みて ),
  let_me_go: [ 'LET ME GO', 'LET ME GO', 'れっとみーごー' ],
  alone: %w( ALONE ALONE あろーん ),
  chase: %w( CHASE CHASE ちぇいす ),
  omoshiroi: [ 'おもしろい [森高コネクション]', '{OMOSHIROI} [Moritaka Connection]', 'おもしろい' ],
  wakareta_onna: [ '別れた女', '{WAKARETA ONNA}', 'わかれたおんな' ],
  watashiga_hen: [ '私が変?', '{WATASHI-GA HEN?}', 'わたしがへん' ],
  detagari: [ '出たがり', '{DETAGARI}', 'でたがり' ],
  modorenai_natsu: [ '戻れない夏', '{MODORENAI NATSU}', 'もどれないなつ' ],
  the_stress: [ 'ザ・ストレス', 'The Stress', 'ざ　すとれす' ],
  yurusenai: %w( ユルセナイ {YURUSENAI} ゆるせない ),
  seventeen: %w( 17才 JUNANA-SAI じゅうななさい ),
  the_loco_motion: [ 'THE LOCO-MOTION', 'THE LOCO-MOTION', 'ざろこもーしょん' ],
  hatachi: %w( 20才 {HATACHI} はたち ),
  hijitsuryokuha_sengen: [ '非実力派宣言', '{HI-JITSURYOKUHA SENGEN}', 'ひじつりょくはせんげん' ],
  korekkiri_byebye: [ 'これっきりバイバイ', '{KOREKKIRI ByeBye}', 'これっきりばいばい' ],
  daite: [ 'だいて', '{DAITE}', 'だいて' ],
  kondo_watashi_dokoka: [ '今度私どこか連れていって下さいよ', '{KONDO WATASHI DOKOKA TSURETE-ITTE KUDASAI-YO}', 'こんどわたしどこかつれていってくださいよ' ],
  hadakaniha_naranai: [ 'はだかにはならない', '{HADAKA-NIHA NARANAI}', 'はだかにはならない' ],
  watashiha_onchi: [ '私はおんち', '{WATASHI-HA ONCHI}', 'わたしはおんち' ],
  shiritagari: [ 'しりたがり', '{SHIRITAGARI}', 'しりたがり' ],
  wakasugita_koi: [ '若すぎた恋', '{WAKASUGITA-KOI}', 'わかすぎたこい' ],
  akunno_higeki: [ 'A君の悲劇', '{A-KUN NO HIGEKI}', 'えーくんのひげき' ],
  yoruno_entotsu: [ '夜の煙突', '{YORU-NO ENTOTSU}', 'よるのえんとつ' ],
  sonogono_watashi: [ 'その後の私 [森高コネクション]', '{SONOGONO WATASHI} [Moritaka Connection]', 'そのごのわたし' ],
  yumeno_nakano_kiss: [ '夢の中のキス', '{YUME-NO NAKA-NO Kiss}', 'ゆめのなかのきす' ],
  #### album
  #### single
  new_season_overheat_night: [ 'NEW SEASON　OVERHEAT.NIGHT', 'NEW SEASON / OVERHEAT.NIGHT', 'にゅーしーずん　おーばーひーとないと' ],
  the_mi_ha_mi_ha_: [ 'ザ・ミーハー　ミーハー', 'The Mi-HA- / Mi-HA-', 'ざ　みーはー　みーはー' ],
  the_stress_stress_chuukintou_version: [ 'ザ・ストレス ＊ ストレス・中近東ヴァージョン', 'The stress / Stress {CHUKINTO} version', 'ざすとれす　すとれすちゅうきんとうばーじょん' ],
  #### video
  live_get_smile: [ 'LIVE★GET SMILE 日本青年館ライヴ', 'LIVE GET SMILE Nippon SEINENKAN Live', 'らいぶげっとすまいる　にっぽんせいねんかんらいぶ' ],
  the_mi_ha_special_mi_ha_mix: [ 'ザ・ミーハー (スペシャル・ミーハー・ミックス)', 'The Mi-HA- (Special Mi-HA- Mix)', 'ざみーはー　すぺしゃるみーはーみっくす' ],
  mite_special_live: [ '見て 〜スペシャル〜 ライヴ', '{MITE} Special LIVE', 'みて　すぺしゃる　らいぶ' ],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Title] Title FactoryGirl オブジェクト
def title_factory_with_values(j, e, y)
  FG.find_or_create(
    :title,
    japanese: j,
    english: e,
    yomi: y
  ) if j
end

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Title] Title FactoryGirl オブジェクト
def title_factory(key)
  jt, et, yo = TitleFactoryHash[key.to_sym]
  p key unless jt
  title_factory_with_values(jt, et, yo)
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Campus>] Title FactoryGirl オブジェクトの配列
def title_factories(keys)
  keys.map { |k| title_factory(k) }
end
