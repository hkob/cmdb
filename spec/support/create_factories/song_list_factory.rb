# @param [Hash] hash
# @option hash [String] keyword キーワード
# @option hash [String] device デバイス
# @option hash [String] concert コンサート
# @option hash [String, Fixnum] sort_order 並び順
# @return [SongList] SongList FactoryGirl オブジェクト
def song_list_factory(keyword: nil, device: nil, concert: nil, book: nil, sort_order: nil)
  arel = device ? SongList.device_is(device) : concert ? SongList.concert_is(concert) : SongList.book_is(book)
  ans = arel.sort_order_value_is(sort_order.to_i).take
  unless ans
    ans = FG.find_or_create(
      :song_list,
      keyword: keyword,
      device_id: device.try(:id),
      concert_id: concert.try(:id),
      book_id: book.try(:id),
      sort_order: sort_order.to_i
    )
  end
  ans
end

# @return [SongList] SongList FactoryGirl オブジェクト
def song_list_test_factory
  song_list_factory keyword: :common, device: single_test_factory, sort_order: 1
end

# @return [SongList] SongList FactoryGirl オブジェクト
def song_list_test2_factory
  song_list_factory keyword: :A, concert: concert_test_factory, sort_order: 2
end
