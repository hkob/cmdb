# @param [Hash] hash
# @option hash [String] device_type デバイスタイプ
# @option hash [Symbol] title_key タイトルのkey
# @option hash [String] date 発売日
# @option hash [String] minutes 分
# @option Hash [String] seconds 秒
# @option Hash [Fixnum] sort_order 並び順
# @option Hash [String] number 通番
# @option Hash [Singer] singer_key 歌手の key
# @option Hash [Person] singer 歌手
# @option Hash [String] j_comment 日本語コメント
# @option Hash [String] e_comment 英語コメント
# @option Hash [String] link リンク
# @return [Video] Video FactoryGirl オブジェクト
def video_factory(title_key: nil, device_type: nil, date: nil, no_month: false, no_day: false, minutes: nil, seconds: nil, sort_order: nil, number: nil, singer_key: nil, singer: nil, j_comment: nil, e_comment: nil, link: nil)
  dtv = Video::VideoStr2Num[device_type]
  ans = Video.get_object(dtv, sort_order)
  unless ans
    event_date = event_date_factory date: date, no_month: no_month, no_day: no_day
    title = title_factory title_key
    singer ||= singer_key && person_factory(singer_key)
    ans = FG.find_or_create(
      :video,
      title_id: title.id,
      device_type: dtv,
      event_date_id: event_date.id,
      minutes: minutes,
      seconds: seconds,
      sort_order: sort_order.to_i,
      number: number,
      singer_id: singer.try(:id),
      j_comment: j_comment,
      e_comment: e_comment,
      link: link)
  end
  ans
end

# @return [Video] Video FactoryGirl オブジェクト
def video_test_factory
  video_factory title_key: :live_get_smile, device_type: :live_video, date: '1988/3/25', minutes: 48, seconds: 00, sort_order: 1, number: '1st', singer_key: :iChisatoMoritaka
end

