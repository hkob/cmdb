# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [BandMember] BandMember FactoryGirl オブジェクト
def band_member_factory(band_key: nil, band: nil, instrumental_key: nil, instrumental: nil, person_key: nil, person: nil)
  band ||= band_factory(band_key)
  instrumental ||= instrumental_factory(instrumental_key)
  person ||= person_factory(person_key)
  FG.find_or_create(
    :band_member,
    band_id: band.id,
    instrumental_id: instrumental.id,
    person_id: person.id)
end

def band_member_test_factory
  band_member_factory band_key: :ms_band, person_key: :iChisatoMoritaka, instrumental_key: :VOCAL
end

def band_member_test2_factory
  band_member_factory band_key: :ms_band, person_key: :iYukariFujio, instrumental_key: :CHORUS
end
