# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Book] Book FactoryGirl オブジェクト
def book_factory(book_type: nil, title_key: nil, publisher_key: nil, seller_key: nil, author_key: nil, photographer_key: nil, isbn: nil, price: nil, date: nil, no_month: false, no_day: false, sort_order: nil, j_comment: nil, e_comment: nil, link: nil)
  title = title_factory(title_key)
  ans = title.books.book_type_str_is(book_type).sort_order_value_is(sort_order).take
  unless ans
    event_date_obj = date && event_date_factory(date: date, no_month: no_month, no_day: no_day)
    ans = FG.find_or_create(
      :book,
      book_type: Book.book_types[book_type],
      title_id: title.id,
      publisher_id: publisher_key && company_factory(publisher_key).id,
      seller_id: seller_key && company_factory(seller_key).id,
      author_id: author_key && person_factory(author_key).id,
      photographer_id: photographer_key && person_factory(photographer_key).id,
      isbn: isbn,
      price: price,
      event_date_id: event_date_obj.try(:id),
      sort_order: sort_order,
      j_comment: j_comment,
      e_comment: e_comment,
      link: link
    )
  end
  ans
end

def book_test_factory
  book_factory book_type: :poems, title_key: :wakariyasui_koi, publisher_key: :kadokawa_shoten, seller_key: :kadokawa_shoten, author_key: :iNatsuoGiniro, photographer_key: :iNatsuoGiniro, isbn: '4-0416-7302-X', price: '596', date: '1987/12/18', sort_order: 2, link: 'http://www.moritaka-chisato.com/discogprahy/product/4475.html', j_comment: '写真のモデル', e_comment: 'Photographic model'
end

def book_test2_factory
  book_factory(book_type: :picture_book, title_key: :shuka, publisher_key: :kindai_eigasha, seller_key: :kindai_eigasha, photographer_key: :iYutakaNishimura, isbn: '4-7648-1448-X C0076 P1854E', price: '1,854', date: '1987/8/5', sort_order: 1)
end

