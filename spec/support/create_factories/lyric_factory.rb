# @param [Hash] hash
# @option hash [Song] song 曲
# @option hash [Symbol, String] person_key 関係者を一意に決定するキー
# @option hash [Person] person 関係者
# @option hash [Symbol, String, Fixnum] sort_order 並び順
# @return [Lyric] Lyric FactoryGirl オブジェクト
def lyric_factory(song: nil, person_key: nil, person: nil, sort_order: nil)
  person ||= person_factory person_key
  FG.find_or_create(
    :lyric,
    song_id: song.id,
    person_id: person.id,
    sort_order: sort_order.to_i)
end

# @param [Hash] hash
# @option hash [Song] song 曲
# @option hash [Array<Symbol, String>] person_keys 関係者を一意に決定するキーの配列
# @option hash [Array<Person>] people 関係者一覧
# @return [Array<Lyric>] Lyric FactoryGirl オブジェクトの配列
def lyric_factories(song: nil, person_keys: nil, people: nil)
  people ||= person_keys.map { |key| person_factory key }
  people.map.with_index { |p, i| lyric_factory(song: song, person: p, sort_order: i) }
end

