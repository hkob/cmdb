# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [ConcertVideo] ConcertVideo FactoryGirl オブジェクト
def concert_video_factory(csls = nil, vsls = nil, ch = nil)
  ans = ConcertVideo.concert_song_list_song_is(csls).video_song_list_song_is(vsls).concert_hall_is(ch).take
  unless ans
    FG.find_or_create(
      :concert_video,
      concert_song_list_song_id: csls.id,
      video_song_list_song_id: vsls.id,
      concert_hall_id: ch.id)
  end
end

def concert_video_test_factory
  concert_video_factory song_list_song_test_factory, song_list_song_test2_factory, concert_hall_test_factory 
end

