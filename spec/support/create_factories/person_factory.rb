PersonFactoryHash = {
  iChisatoMoritaka: [ '森高千里', 'Chisato Moritaka', 'もりたか　ちさと' ],
  iHideoSaito: [ '斉藤英夫', 'Hideo Saito', 'さいとう　ひでお' ],
  iHiromasaIjichi: [ '伊秩弘将', 'Hiromasa Ijichi', 'いぢち　ひろまさ' ],
  iHiro: [ 'HIRO', 'HIRO', 'ひろ', :iHiromasaIjichi ],
  iShingoKanno: [ '菅野真吾', 'Shingo Kanno', 'かんのしんご' ],
  iTakumiYamamoto: [ '山本拓巳', 'Takumi Yamamoto', 'やまもとたくみ' ],
  iWise: [ 'WISE(山本拓巳・菅野真吾)', 'WISE(Takumi Yamamoto, Singo Kanno)', 'わいずたくみやまもとしんごかんの' ],
  iKazuhikoIwami: [ '岩見和彦', 'Kazuhiko Iwami', 'いわみかずひこ' ],
  iYoichiroKakizaki: [ '柿崎洋一郎', 'Yoichiro Kakizaki', 'かきざきよういちろう' ],
  iHiroshiSawada: [ '沢田浩史', 'Hiroshi Sawada', 'さわだひろし' ],
  iMansakuKimura: [ '木村万作', 'Mansaku Kimura', 'きむらまんさく' ],
  iKazuoKamata: [ '鎌田一夫', 'Kazuo Kamata', 'かまたかずお' ],
  iTheThreePinkies: [ 'The Three Pinkies', 'The Three Pinkies', 'ざすりーぴんきーず' ],
  iTomoakiArima: [ '有馬知章', 'Tomoaki Arima', 'ありまともあき' ],
  iNobitaTsukada: [ '塚田のび太', 'Nobita Tsukada', 'つかだのびた' ],
  iRyoSaito: [ 'さいとうりょう', 'Ryo Saito', 'さいとうりょう' ],
  iKoiTakayanagi: [ '高柳恋', 'Koi Takayanagi', 'たかやなぎこい' ],
  iKenShima: [ '島健', 'Ken Shima', 'しまけん' ],
  iAkiraWada: [ '和田アキラ', 'Akira Wada', 'わだあきら' ],
  iKenjiTakamizu: [ '高水健司', 'Kenji Takamizu', 'たかみずけんじ' ],
  iShuichiPONTAMurakami: [ '村上"PONTA"秀一', 'Shuichi "PONTA" Murakami', 'むらかみぽんたしゅういち' ],
  iShigeoFuchino: [ '渕野繁雄', 'Shigeo Fuchino', 'ふちのしげお' ],
  iMAEDAStrings: [ 'MAEDA Strings', 'MAEDA Strings', 'まえだすとりんぐす' ],
  iTakayoshiUmeno: [ 'ウメノタカヨシ', 'Takayoshi Umeno', 'うめのたかよし' ],
  iHiromichiTsugaki: [ '津垣博通', 'Hiromichi Tsugaki', 'つがきひろみち' ],
  iHarumiMita: [ '三田治美', 'Harumi Mita', 'みたはるみ' ],
  iHiromiFukunaga: [ '福永ひろみ', 'Hiromi Fukunaga', 'ふくながひろみ' ],
  iMakotoMatsushita: [ '松下誠', 'Makoto Matsushita', 'まつしたまこと' ],
  iYasuoTomikura: [ '富倉安生', 'Yasuo Tomikura', 'とみくらやすお' ],
  iEijiShimamura: [ '島村英二', 'Eiji Shimamura', 'しまむらえいじ' ],
  iNobuSaito: [ '斎藤ノヴ', 'Nobu Saito', 'さいとうのぶ' ],
  iTOMODAStrings: [ '友田ストリングス', 'TOMODA Strings', 'ともだすとりんぐす' ],
  iYasuakiMaejima: [ '前嶋康明', 'Yasuaki Maejima', 'まえじまやすあき' ],
  iYasuakiMichaelMaejima: [ "Yasuaki 'Michael' Maejima", "Yasuaki 'Michael' Maejima", "まえじままいけるやすあき", :iYasuakiMaejima ],
  iKoyoMurakami: [ '村上こうよう', 'Koyo Murakami', 'むらかみこうよう' ],
  iYasuhikoFukuda: [ '福田裕彦', 'Yasuhiko Fukuda', 'ふくだやすひこ' ],
  iChiharuMikuzuki: [ '美久月千晴', 'Chiharu Mikuzuki', 'みくずきちはる' ],
  iRyubenTsujino: [ '辻野リューベン', 'Ryuben Tsujino', 'つじのりゅうべん' ],
  iYukariFujio: [ '藤生ゆかり', 'Yukari Fujio', 'ふじおゆかり' ],
  iGinjiOgawa: [ '小川銀士', 'Ginji Ogawa', 'おがわぎんじ' ],
  iHisanoriMizuno: [ 'みずのひさのり', 'Hisanori Mizuno', 'みずのひさのり' ],
  iMasayukiMuraishi: [ '村石雅行', 'Masayuki Muraishi', 'むらいしまさゆき' ],
  iYouichiOkabe: [ '岡部 洋一', 'Youichi Okabe', 'おかべよういち' ],
  iHiroyukiKato: [ '加藤博之', 'Hiroyuki Kato', 'かとうひろゆき' ],
  iKanonKuwa: [ '久和カノン', 'Kanon Kuwa', 'くわかのん' ],
  iYutakaNishimura: [ '西村寛', 'Yutaka Nishimura', 'にしむらゆたか' ],
  iHaruKimura: [ '木村晴', 'Haru Kimura', 'きむらはる' ],
  iMisaNakayama: [ '中山みさ', 'Misa Nakayama', 'なかやまみさ' ],
  iJakeHConception: [ 'ジェイク・コンセプション', 'Jake H. Conception', 'じぇいくえいちこんせぷしょん' ],
  iJunroSato: [ '佐藤純朗', 'Junro Sato', 'さとうじゅんろう' ],
  iNana: [ 'Nana', 'Nana', 'なな' ],
  iTakayukiNegishi: [ '根岸貴幸', 'Takayuki Negishi', 'ねぎしたかゆき' ],
  iHatsuhoFurukawa: [ '古川初穂', 'Hatsuho Furukawa', 'ふるかわはつほ' ],
  iNatsuoGiniro: [ '銀色夏生', 'Natsuo Giniro', 'ぎんいろなつお' ],
  iHDavid: [ 'H. David', 'H. David', 'えいちでびっど' ],
  iBBacharach: [ 'B.Bacharach: ', 'B.Bacharach: ', 'びぃばーくらっち' ],
  iKeiichiIshibashi: [ '石橋敬一', 'Keiichi Ishibashi', 'いしばしけいいち' ],
  iMasahiroMiyazaki: [ '宮崎まさひろ', 'Masahiro Miyazaki', 'みやざきまさひろ' ],
  iKeijiToriyama: [ '鳥山敬治', 'Keiji Toriyama', 'とりやまけいじ' ],
  iYukioSeto: [ '瀬戸由紀男', 'Yukio Seto', 'せとゆきお' ],
  iMakotoOishi: [ '大石誠', 'Makoto Oishi', 'おおいしまこと' ],
  iShinjiYasuda: [ '安田信二', 'Shinji Yasuda', 'やすだしんじ' ],
  iEkisuSuzuki: [ '鈴木エキス', 'Ekisu Suzuki', 'すずきえきす' ],
  iNaokiSuzuki: [ '鈴木直樹', 'Naoki Suzuki', 'すずきなおき', ],
  iMiekoArima: [ '有馬三恵子', 'Mieko Arima', 'ありまみえこ' ],
  iKyoheiTsutsumi: [ '筒美京平', 'Kyohei Tsutsumi', 'つつみきょうへい' ],
  iGerryGoffin: [ 'GERRY-GOFFIN', 'GERRY-GOFFIN', 'げりーごーふぃん' ],
  iCaroleKing: [ 'CAROLE-KING', 'CAROLE-KING', 'きゃろるきんぐ' ],
  iNaokiKimura: [ '木村直樹', 'Naoki Kimura', 'きむらなおき' ],
  iKenichiMitsuda: [ '光田健一', 'Kenichi Mitsuda', 'みつだけんいち' ],
  iYuichiTakahashi: [ '高橋諭一', 'Yuichi Takahashi', 'たかはしゆういち' ],
  iMasataroNaoe: [ '直枝政太郎', 'Masataro Naoe', 'なおえまさたろう'],
  iGiroBando: [ '坂東次郎', 'Giro Bando', 'ばんどうじろう' ],
  iYujiMada: [ '馬田祐次', 'Yuji Mada', 'まだゆうじ' ],
  iYuichiTanaya: [ '棚谷祐一', 'Yuichi Tanaya', 'たなやゆういち' ],
  iHiroshiYabe: [ '矢部浩志', 'Hiroshi Yabe', 'やべひろし' ],
  iKazuyoshiYamauchi: [ '山内和義', 'Kazuyoshi Yamauchi', 'やまうちかずよし' ],
  iCarlosKanno: [ 'カルロス菅野', 'Carlos Kanno', 'かるろすかんの' ],
  iKenjiYoshida: [ '吉田憲司', 'Kenji Yoshida', 'よしだけんじ' ],
  iMasahiroKobayashi: [ '小林正弘', 'Masahiro Kobayashi', 'こばやしまさひろ' ],
  iOsamuMatsuki: [ '松木治', 'Osamu Matsuki', 'まつきおさむ' ],
  iHisashiYoshinaga: [ '吉永寿', 'Hisashi Yoshinaga', 'よしながひさし' ],
  iTakeruMuraoka: [ '村岡建', 'Takeru Muraoka', 'むらおかたける' ],
  iCarnation: [ 'カーネーション', 'Carnation', 'かーねーしょん' ],
  iHitoshiKitamura: [ '北村ひとし', 'Hitoshi Kitamura', 'きたむらひとし' ],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Person] Person FactoryGirl オブジェクト
def person_factory(key)
  jt, et, yo, p = PersonFactoryHash[key.to_sym]
  if jt
    title = title_factory_with_values jt, et, yo
    parent = person_factory(p) if p
    FG.find_or_create(
      :person,
      title_id: title.id,
      parent_id: p && parent.try(:id))
  else
    print "Can't find #{key}. exit\n"
    exit
  end
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Person>] Person FactoryGirl オブジェクトの配列
def person_factories(keys)
  keys.map { |k| person_factory(k) }
end

