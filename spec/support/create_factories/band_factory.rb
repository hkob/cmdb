BandFactoryHash = {
  animals_1: [ 'ANIMALS (1)', 'ANIMALS (1)', 'あにまるず', '-', '-' ],
  animals_2: [ 'ANIMALS (2)', 'ANIMALS (2)', 'あにまるずに', '-', '-' ],
  ms_band: [ "M's BAND", "M's BAND", 'えむずばんど', '(at 日本青年館)', '(at Nihon SEINEN-KAN' ],
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Band] Band FactoryGirl オブジェクト
def band_factory(key)
  jt, et, yo, jc, ec = BandFactoryHash[key.to_sym]
  title = title_factory_with_values jt, et, yo
  FG.find_or_create(
    :band,
    title_id: title.id,
    j_comment: jc == '-' ? nil : jc,
    e_comment: ec == '-' ? nil : ec) if jt
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Band>] Band FactoryGirl オブジェクトの配列
def band_factories(keys)
  keys.map { |k| band_factory(k) }
end

