SongFactoryHash = {
  new_season: %w( new_season - 1987/5/25 ),
  period: %w( period - 1987/5/25 ),
  namida_good_bye: %w( namida_good_bye - 1987/7/25 ),
  yumeno_owari: %w( yumeno_owari - 1987/7/25 ),
  ringoshuno_rule: %w( ringoshuno_rule - 1987/7/25 ),
  otisreddingni_kanpai: %w( otisreddingni_kanpai - 1987/7/25 ),
  ways: %w( ways - 1987/7/25 ),
  anohino_photograph: %w( anohino_photograph - 1987/7/25 ),
  miss_lady: %w( miss_lady - 1987/7/25 ),
  weekend_blue: %w( weekend_blue - 1987/9/7 ),
  overheat_night: %w( overheat_night - 1987/9/7 ),
  overheat_night_2: %w( overheat_night_2 overheat_night 1987/9/7 ),
  good_bye_season: %w( good_bye_season - 1987/12/17 ),
  get_smile: %w( get_smile - 1987/12/17 ),
  yokohama_one_night: %w( yokohama_one_night - 1988/3/25 ),
  cant_say_good_bye: %w( cant_say_good_bye - 1988/3/25 ),
  pi_a_no: %w( pi_a_no - 1988/3/25 ),
  forty_seven_har_nights: %w( forty_seven_har_nights - 1988/3/25 ),
  kiss_the_night: %w( kiss_the_night - 1988/3/25 ),
  mi_ha_: %w( mi_ha_ - 1988/3/25 ),
  the_mi_ha_: %w( the_mi_ha_ mi_ha_ 1988/4/25 ),
  do_you_know_the_way_to_san_jose: %w( do_you_know_the_way_to_san_jose - 1988/7/10 ),
  romantic: %w( romantic - 1988/7/10 ),
  kaigan: %w( kaigan - 1988/7/10 ),
  shizukana_natsu: %w( shizukana_natsu - 1988/7/10 ),
  stress: %w( stress - 1988/11/12 ),
  mite: %w( mite - 1988/11/12 ),
  let_me_go: %w( let_me_go - 1988/11/12 ),
  alone: %w( alone - 1988/10/25 ),
  chase: %w( chase - 1988/10/25 ),
  omoshiroi: %w( omoshiroi - 1988/11/17 ),
  wakareta_onna: %w( wakareta_onna - 1988/11/17 ),
  watashiga_hen: %w( watashiga_hen - 1988/11/17 ),
  detagari: %w( detagari - 1988/11/17 ),
  modorenai_natsu: %w( modorenai_natsu - 1988/11/17 ),
  the_stress: %w( the_stress stress 1988/11/17 ),
  yurusenai: %w( yurusenai - 1989/2/25 ),
  seventeen: %w( seventeen - 1989/4/15 ),
  the_loco_motion: %w( the_loco_motion - 1989/4/15 ),
  hatachi: %w( hatachi - 1989/5/25 ),
  korekkiri_byebye: %w( korekkiri_byebye - 1989/7/25 ),
  daite: %w( daite - 1989/7/25 ),
  hijitsuryokuha_sengen: %w( hijitsuryokuha_sengen - 1989/7/25 ),
  kondo_watashi_dokoka: %w( kondo_watashi_dokoka - 1989/7/25 ),
  hadakaniha_naranai: %w( hadakaniha_naranai - 1989/7/25 ),
  watashiha_onchi: %w( watashiha_onchi - 1989/7/25 ),
  shiritagari: %w( shiritagari - 1989/7/25 ),
  wakasugita_koi: %w( wakasugita_koi - 1989/7/25 ),
  akunno_higeki: %w( akunno_higeki - 1989/7/25 ),
  yoruno_entotsu: %w( yoruno_entotsu - 1989/7/25 ),
  sonogono_watashi: %w( sonogono_watashi - 1989/7/25 ),
  yumeno_nakano_kiss: %w( yumeno_nakano_kiss - 1989/7/25 ),
}

# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [Song] Song FactoryGirl オブジェクト
def song_factory(key)
  t, p, d, nm, nd = SongFactoryHash[key.to_sym]
  p key unless t
  title = title_factory t
  event_date = event_date_factory(date: d, no_month: nm, no_day: nd)
  FG.find_or_create(
    :song,
    title_id: title.id,
    parent_id: p == '-' ? nil : song_factory(p).id,
    event_date_id: event_date.id) if t
end

# @param [Array<Symbol, String>] keys オブジェクトを一意に決定するキーの配列
# @return [Array<Song>] Song FactoryGirl オブジェクトの配列
def song_factories(keys)
  keys.map { |k| song_factory(k) }
end

# @param [Hash] hash
# @option hash [String] song_key 曲を一意に確定する key
# @option hash [Array<Symbol>] lyric_keys 作詞の人を一意に確定する key の配列
# @option hash [Array<Person>] lyrics 作詞の人の配列
# @option hash [Array<Symbol>] music_keys 作曲の人を一意に確定する key の配列
# @option hash [Array<Person>] musics 作曲の人の配列
def song_factory_with_lyrics_musics(song_key: nil, lyric_keys: nil, lyrics: nil, music_keys: nil, musics: nil)
  ans = song_factory song_key
  lyric_factories song: ans, person_keys: lyric_keys, people: lyrics
  music_factories song: ans, person_keys: music_keys, people: musics
  ans
end

