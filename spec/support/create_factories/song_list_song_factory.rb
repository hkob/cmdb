# @param [Hash] hash
# @option hash [SongList] song_list ソングリスト
# @option hash [String] song_key 曲を一意に確定する key
# @option hash [Song] song 曲
# @option hash [String] j_title 曲でないときの日本語名
# @option hash [String] e_title 曲でないときの英語名
# @option hash [String] j_ver 日本語バージョン名
# @option hash [String] e_ver 英語バージョン名
# @option hash [String,Fixnum] sort_order 並び順
# @return [SongListSong] SongListSong FactoryGirl オブジェクト
def song_list_song_factory(song_list: nil, song_key: nil, song: nil, j_title: nil, e_title: nil, j_ver: nil, e_ver: nil, sort_order: nil)
  ans = song_list.song_list_songs.sort_order_value_is(sort_order).take
  unless ans
    song ||= song_key && song_factory(song_key)
    ans = FG.find_or_create(
      :song_list_song,
      song_list_id: song_list.id,
      song_id: song.try(:id),
      j_title: j_title,
      e_title: e_title,
      j_ver: j_ver,
      e_ver: e_ver,
      sort_order: sort_order.to_i
    )
  end
  ans
end

# @return [SongListSong] SongListSong FactoryGirl オブジェクト
def song_list_song_test_factory
  song_list = song_list_test_factory
  song_list_song_factory(song_list: song_list, song_key: :new_season, sort_order: 1)
end

# @return [SongListSong] SongListSong FactoryGirl オブジェクト
def song_list_song_test2_factory
  song_list = song_list_test2_factory
  song_list_song_factory(song_list: song_list, song_key: :yumeno_owari, sort_order: 2)
end
