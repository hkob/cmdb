# @param [Hash] hash
# @option hash [String] concert_key concert を一意に決定するキー
# @option hash [Concert] concert concert
# @option hash [Fixnum] sort_order 並び順
# @option hash [String] hall_key hall を一意に決定するキー
# @option hash [Hall] hall hall
# @option hash [String] date 日付文字列
# @option hash [String] j_comment 日本語コメント
# @option hash [String] e_comment 英語コメント
# @option hash [String] j_product 日本語名産品
# @option hash [String] e_product 英語名産品
# @option hash [SongList] song_list ソングリスト
# @option hash [Device] device デバイス
# @return [ConcertHall] ConcertHall FactoryGirl オブジェクト
def concert_hall_factory(concert: nil, sort_order: nil, hall_key: nil, hall: nil, j_hall_sub: nil, e_hall_sub: nil, date: nil, no_month: false, no_day: false, j_comment: nil, e_comment: nil, j_product: nil, e_product: nil, song_list: nil, device: nil)
  ans = ConcertHall.get_object(concert, sort_order)
  unless ans
    hall ||= hall_factory hall_key
    date_obj = event_date_factory date: date, no_month: no_month, no_day: no_day
    ans = FG.find_or_create(
      :concert_hall,
      concert_id: concert.id,
      sort_order: sort_order,
      hall_id: hall.id,
      j_hall_sub: j_hall_sub,
      e_hall_sub: e_hall_sub,
      event_date_id: date_obj.id,
      j_comment: j_comment,
      e_comment: e_comment,
      j_product: j_product,
      e_product: e_product,
      song_list_id: song_list.try(:id),
      device_id: device.try(:id)
    )
  end
  ans
end

def concert_hall_test_factory
  concert_hall_factory(concert: concert_test_factory, sort_order: 1, hall_key: :shibuya_live_in, date: '1987/9/7', j_comment: 'jc', e_comment: 'ec', j_product: 'jp', e_product: 'ep', song_list: song_list_test_factory, device: album_test_factory)
end

def concert_hall_test2_factory
  concert_hall_factory(concert: concert_test2_factory, sort_order: 2, hall_key: :shibuya_live_in, date: '1987/9/7', j_comment: 'jc', e_comment: 'ec', j_product: 'jp', e_product: 'ep', song_list: song_list_test_factory, device: album_test_factory)
end

