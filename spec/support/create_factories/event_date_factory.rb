# @param [Symbol, String] key オブジェクトを一意に決定するキー
# @return [EventDate] EventDate FactoryGirl オブジェクト
def event_date_factory(date: nil, no_month: false, no_day: false)
  date_obj = Date.parse(date)
  year = year_factory(date_obj.year)
  FG.find_or_create(
    :event_date,
    date: date_obj,
    year_id: year.id,
    month: no_month ? nil : date_obj.month,
    day: no_day ? nil : date_obj.day)
end

# @return [EventDate] EventDate FactoryGirl オブジェクト
def event_date_test_factory
  event_date_factory date:'1987/5/25'
end
