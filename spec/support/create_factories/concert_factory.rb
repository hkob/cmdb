# @param [Hash] hash
# @option hash [String] j_title 日本語タイトル
# @option hash [String] e_title 英語タイトル
# @option hash [String] yomi 読み
# @option hash [String] j_subtitle 日本語サブタイトル
# @option hash [String] e_subtitle 英語サブタイトル
# @option hash [String] j_comment 日本語コメント
# @option hash [String] e_comment 英語コメント
# @option hash [String] concert_type コンサートタイプ
# @option hash [Date] from 公演開始日
# @option hash [Date] to 公演終了日
# @option hash [Boolean] has_song_list song_list があれば true
# @option Hash [Boolean] has_product product があれば true
# @option Hash [Symbol] band_key バンドを一意に決定する key
# @option Hash [Fixnum] num_of_performances 公演数
# @option Hash [Fixnum] num_of_halls 公演ホール数
# @option Hash [Fixnum] sort_order 並び順
# @return [Concert] Concert FactoryGirl オブジェクト
def concert_factory(j_title: nil, e_title: nil, yomi: nil, j_subtitle: nil, e_subtitle: nil, j_comment: nil, e_comment: nil, concert_type: nil, from: nil, from_no_month: false, from_no_day: false, to: nil, to_no_month: false, to_no_day: false, has_song_list: false, has_product: false, band: nil, band_key: nil, num_of_performances: 1, num_of_halls: 1, sort_order: nil)
  ans = Concert.get_object(concert_type, sort_order)
  unless ans
    from_obj = event_date_factory date: from, no_month: from_no_month, no_day: from_no_day
    to_obj = to && event_date_factory(date: to, no_month: to_no_month, no_day: to_no_day)
    title = title_factory_with_values j_title, e_title, yomi
    band ||= band_key && band_factory(band_key)
    ans = FG.find_or_create(
      :concert,
      title_id: title.id,
      j_subtitle: j_subtitle,
      e_subtitle: e_subtitle,
      j_comment: j_comment,
      e_comment: e_comment,
      concert_type: concert_type,
      from_id: from_obj.id,
      to_id: to_obj.try(:id),
      has_song_list: has_song_list,
      has_product: has_product,
      band_id: band.try(:id),
      num_of_performances: num_of_performances,
      num_of_halls: num_of_halls,
      sort_order: sort_order
    )
  end
  ans
end

def concert_test_factory
  concert_factory j_title: '「OVERHEAT. NIGHT」, 「GET SMILE」', e_title: '"OVERHEAT. NIGHT",  "GET SMILE"', yomi: 'おーばーひーとないと　げっとすまいる', concert_type: :live, from: '1987/12/11', to: '1988/3/11', num_of_performances: 3, num_of_halls: 2, has_song_list: true, band_key: :ms_band, sort_order: 2
end

def concert_test2_factory
  concert_factory j_title: '渋谷ライブイン ファーストライブ', e_title: 'First Live at Shibuya Live-in', yomi: 'しぶやらいぶいん　ふぁーすとらいぶ', concert_type: :live, from: '1987/9/7', has_song_list: true, sort_order: 1
end
