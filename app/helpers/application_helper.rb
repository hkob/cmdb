module ApplicationHelper
  # Common hash
  MbStrHash = { true => '○', false => '×' }
  MbOptions = MbStrHash.map { |k, v| [ v, k ] }

  # @params [Array<Array<String, Boolean>>] options 表示する文字列とboolean(省略可)
  # @return [Hash] simple_form 用の hash
  def mb_options_hash(options = MbOptions)
    {
      collection: MbOptions,
      as: :radio_buttons,
      wrapper: :horizontal_radio_and_checkboxes,
      item_wrapper_class: 'radio-inline'
    }
  end

  # @param [String] optional table class
  # @return [Hash] default table class
  def default_table_class
    { class: 'table table-striped table-hover table-bordered table-condensed' }
  end

  # @return [Hash] btn class + remote(if required)
  def btn_class(type: 'info', size: 'xs', remote: true, add_class: nil, method: nil)
    add_class = " #{add_class}" if add_class.present?
    btn_size = size ? " btn-#{size}" : ''
    ans = { class: "btn btn-#{type}#{btn_size}#{add_class}" }
    ans[:remote] = true if remote
    ans[:method] = method if method
    # ans[:data] = { disable_with: '送信中...' }
    ans
  end

  # @return [String] cancel_link
  def modal_cancel_link
    link_to t('common.cancel'), '#', btn_class(type: :warning).merge(data: { dismiss: 'modal' })
  end

  # @return [String] cancel_link
  def modal_close_link
    link_to t('common.close'), '#',
      btn_class(type: 'warning', size: nil).merge(data: { dismiss: 'modal' })
  end

  # @param [Object] model モデルクラス
  # @param [Array<String>] atr 属性名
  def t_ar(model, atr = nil)
    if atr
      return model.human_attribute_name(atr)
    else
      return model.model_name.human
    end
  end

  # @param [Object] model モデルクラス
  # @param [Array<String>] keys 属性名の配列
  # @return [Array<String>] 翻訳後の文字列の配列
  def t_ars(model, keys)
    keys.map { |key| model.human_attribute_name(key) }
  end

  def t_ars_join(model, keys, del = ', ')
    t_ars(model, keys).join(del)
  end

  # @param [Boolean] flag class を設定するかどうか
  # @param [String] class に設定する文字列
  # @return [Hash] flag が true なら { class: str }、false なら {}
  def class_str_or_nil(flag, str = 'info')
    flag ? { class: str } : {}
  end

  # @param [Boolean] flag true or false
  # @return [String] true なら○、false なら×
  def mb(flag)
    flag ? '○' : '×'
  end

  # @param [Boolean] flag true or false
  # @return [String] true なら○、false ならnil
  def mn(flag)
    flag ? '○' : nil
  end

  # @param [Hash] option 変更するオプション
  # @return [String] 再帰パス
  def recursive_path(option = {})
    request.params.merge(option)
  end

  # @param [String] path path名
  # @return [Hash<String>] Routing parameter
  def url_to_params(path, method = :get)
    Rails.application.routes.recognize_path(path, method: method)
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [Array<String>] コントローラ名とアクション名の配列
  def controller_and_action_names_from_path(path, method = :get)
    url_to_params(path, method).values_at(*%i( controller action ))
  end

  # @param [String] cn コントローラ名
  # @param [String] an アクション名
  # @return [String] タイトル
  def title_from_controller_and_action(cn, an)
    t([ cn.gsub('/', '.'), an, 'title' ].join('.'))
  end

  # @param [String] cn コントローラ名
  # @param [String] an アクション名
  # @return [String] リンクタイトル
  def link_title_from_controller_and_action(cn, an)
    t([ cn.gsub('/', '.'), an, 'link_title' ].join('.'))
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [String] タイトル
  def title_from_path(path, method = nil)
    cn, an = controller_and_action_names_from_path(path, method)
    title_from_controller_and_action(cn, an)
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [String] タイトル
  def link_title_from_path(path, method = nil)
    cn, an = controller_and_action_names_from_path(path, method)
    link_title_from_controller_and_action(cn, an)
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [Array<String>] タイトルと path の配列
  def title_with_path(path, method = nil)
    [ title_from_path(path, method), path ]
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [Array<String>] タイトルと path の配列
  def link_title_with_path(path, method = nil)
    [ link_title_from_path(path, method), path ]
  end

  # @param [String] path path 名
  # @param [String,nil] method メソッド名
  # @return [Array<String>] タイトルと path の配列
  def check_title_with_path(path, method = nil)
    [ t('common.check'), path ]
  end

  # @param [Array<String>] array 文字列と URL の配列
  # @note ナビゲーションリンクを表示
  def navi(array)
    render partial: 'shared/navi_link_bar', locals:{ navi: array }
  end

  # @param [Array<String>] メニュータイトルの翻訳キーの配列
  # @return [String] メニューのタイトルコンテンツを得る
  MenuTitles = I18n.t(%w( common.link common.detail common.privileges ))
  def menu_title(*array)
    array = MenuTitles if array.length == 0
    content_tag :tr do
      array.flatten.each do |str|
        concat content_tag(:th, str)
      end
    end
  end

  # @param [Array<String>] メニュータイトルの翻訳キーの配列
  # @return [String] メニューのタイトルコンテンツを得る
  def menu_title_wc(array)
    menu_title t('common.control'), array
  end

  # @param [Boolean] flag リンクを描画するかどうか
  # @param [String] path URL
  # @param [String] add_info 追加情報
  # @return [String] link 文字列
  def link_or_nil(flag, path, add_info = {})
    link_to *link_title_with_path(path), add_info if flag
  end

  # @param [Boolean] flag リンクを描画するかどうか
  # @param [String] path URL
  # @param [String] add_info 追加情報
  # @return [String] link 文字列
  def link_or_nil_with_title(flag, path, title, add_info = {})
    link_to title, path, add_info if flag
  end

  def spacer
    content_tag(:span) { '・' }
  end

  def ajax_normal_link(object: nil, show_path: nil, ajax_title: nil, type: :info, key: nil)
    show_path ||= object.show_path(@is_ja, key)
    ajax_title ||= object && object.name(@is_ja)
    content_tag :div, class: 'btn-group' do
      concat(link_to((ajax_title || link_title_from_path(show_path)), show_path, btn_class(type: type)))
      concat(link_to('↓', show_path, btn_class(type: :default, remote: false)))
    end
  end

  def ajax_normal_link_by_title(object: nil, show_path: nil, ajax_title: nil, type: :info, key: nil)
    show_path ||= object.show_path(@is_ja, key)
    content_tag :div, class: 'btn-group' do
      concat(link_to((ajax_title || title_from_path(show_path)), show_path, btn_class(type: type)))
      concat(link_to('↓', show_path, btn_class(type: :default, remote: false)))
    end
  end

  def year_link(event_date)
    ajax_normal_link show_path: year_path(event_date.year_id, key: event_date.month), ajax_title: event_date.name
  end

  def data_toggle_tab
    { 'data-toggle' => :tab }
  end

  def je_link
    link_to t('common.lang'), recursive_path(locale: @is_ja ? :en : :ja)
  end

  def url_link
    link_to t('common.url'), recursive_path
  end

  def external_or_str(str)
    if /^http/ =~ str
      host = str.split('/')[2]
      key = host == 'www.moritaka-chisato.com' ? :official : :external
      link_to "#{t("common.#{ key }_link")}(#{host})", str, btn_class(type: :warning, remote: false).merge(target: '_blank')
    else
      str
    end
  end
end
