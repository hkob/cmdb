# @!attribute song_list_id
#   @return [Fixnum] ソングリストID
# @!attribute song_id
#   @return [Fixnum] 曲ID
# @!attribute j_title
#   @return [String] 日本語タイトル
# @!attribute e_title
#   @return [String] 英語タイトル
# @!attribute j_ver
#   @return [String] 日本語バージョン名
# @!attribute e_ver
#   @return [String] 英語バージョン名
# @!attribute sort_order
#   @return [Fixnum] 並び順

class SongListSong < ActiveRecord::Base
  validates :song_list_id, :sort_order, presence: true
  validates :sort_order, uniqueness: { scope: %i( song_list_id ) }

  # @return [SongList] 対応するソングリスト
  belongs_to :song_list
  # @return [Song] 対応する曲
  belongs_to :song

#  # @return [<BBB>] 対応するYYY
#  has_one :bbb

  # @return [Array<SongPerson>] 対応する曲関係者一覧
  has_many :song_people, dependent: :destroy
  # @return [Array<ConcertVideo>] 対応するコンサートビデオ(コンサート側)
  has_many :concert_to_concert_videos, class_name: :ConcertVideo, foreign_key: :concert_song_list_song_id, dependent: :destroy
  # @return [Array<ConcertVideo>] 対応するコンサートビデオ(ビデオ側)
  has_many :video_to_concert_videos, class_name: :ConcertVideo, foreign_key: :video_song_list_song_id, dependent: :destroy


  scope :include_concert, -> { includes(:song_list).merge(SongList.include_concert) }
  scope :include_device, -> { includes(:song_list).merge(SongList.include_device) }
  scope :include_song, -> { includes(:song).merge(Song.include_title) }
  scope :include_song_list, -> { includes(:song_list) }
  scope :include_video_to_concert_videos, -> { includes(:video_to_concert_videos) }
  scope :only_album, -> { joins(song_list: :device).merge(Device.only_album) }
  scope :only_single, -> { joins(song_list: :device).merge(Device.only_single) }
  scope :only_video, -> { joins(song_list: :device).merge(Device.only_video) }
  scope :only_concert, -> { joins(:song_list).merge(SongList.only_concert) }
  scope :order_device_year, -> { joins(song_list: :device).merge(Device.order_year) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :songs_are, -> ss { where arel_table[:song_id].in ss.map(&:id) }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }

  # @param [Boolean] is_ja 日本語なら true
  # @return [String] タイトル
  def title(is_ja)
    is_ja ? j_title : e_title
  end

  # @param [Boolean] is_ja 日本語なら true
  # @return [String] タイトル
  def version(is_ja)
    is_ja ? j_ver : e_ver
  end

  def get_arranges
    song_people.include_person.only_arrange.order_person_yomi.map(&:person)
  end

  def arrange_names(is_ja)
    get_arranges.map { |p| p.name(is_ja) }.join(', ')
  end
end
