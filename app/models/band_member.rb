# @!attribute band_id
#   @return [Type] バンドID
# @!attribute person_id
#   @return [Type] 関係者ID
# @!attribute instrumental_id
#   @return [Type] 楽器I
#
class BandMember < ActiveRecord::Base
  validates :band_id, :person_id, :instrumental_id, presence: true
  validates :instrumental_id, uniqueness: { scope: %i( band_id person_id ) }

  # @return [Band] 対応するバンド
  belongs_to :band
  # @return [Person] 対応する関係者
  belongs_to :person
  # @return [Band] 対応する楽器
  belongs_to :instrumental

#  # @return [<BBB>] 対応するYYY
#  has_one :bbb

#  # @return [Array<CCC>] 対応するZZZ一覧
#  has_many :cccs

#  has_many :ddds, class_name: 'CCC', foreign_key: :ddd_id

  scope :include_band, -> { includes :band }
  scope :include_instrumental, -> { includes(:instrumental).merge(Instrumental.include_title) }
  scope :include_person, -> { includes(:person).merge(Person.include_title) }
  scope :order_instrumental_sort_order, -> { joins(:instrumental).merge(Instrumental.order_sort_order) }
  scope :order_band_yomi, -> { joins(:band).merge(Band.order_yomi) }
  scope :order_person_yomi, -> { joins(:person).merge(Person.order_yomi) }
  scope :people_are, -> ps { where arel_table[:person_id].in ps.map(&:id) }
  scope :person_head_value_is, -> v { joins(:person).merge(Person.head_value_is(v)) }
end
