# @!attribute japanese
#   @return [String] 日本語名
# @!attribute english
#   @return [String] 英語名
# @!attribute yomi
#   @return [String] 読み
# @!attribute yomi_suuji
#   @return [String] 読み数字

class Title < ActiveRecord::Base
  include Yomi
  validates :japanese, :english, :yomi, presence: true
  before_save :on_save_validation_method
  # @return [boolean] yomi_suuji が正しく変換できなければ エラーを追加し、false
  def on_save_validation_method
    ans = true
    if yomi
      self.yomi_suuji = make_yomi_suuji(yomi)
      unless yomi_suuji
        errors.add :yomi, 'は，ひらがなと全角スペース以外の文字は使えません'
        ans = false
      end
    end
    ans
  end
  private :on_save_validation_method

  # @return [Band] 対応するバンド
  has_one :band, dependent: :destroy
  # @return [Concert] 対応するコンサート
  has_one :concert, dependent: :destroy
  # @return [Hall] 対応するホール
  has_one :hall, dependent: :destroy
  # @return [Instrumental] 対応する楽器
  has_one :instrumental, dependent: :destroy
  # @return [Person] 対応する関係者
  has_one :person, dependent: :destroy
  # @return [Prefecture] 対応する都道府県
  has_one :prefecture, dependent: :destroy
  # @return [Song] 対応する曲
  has_one :song, dependent: :destroy

  # @return [Array<Activity>] 対応する活動
  has_many :activities, dependent: :destroy
  # @return [Array<Album>] 対応するアルバム
  has_many :albums, dependent: :destroy
  # @return [Array<Book>] 対応する本
  has_many :books, dependent: :destroy
  # @return [Array<Single>] 対応するシングル
  has_many :singles, dependent: :destroy
  # @return [Array<Video>] 対応するビデオ
  has_many :videos, dependent: :destroy

  scope :head_value_is, -> v { where arel_table[:yomi_suuji].matches("#{v}%") }
  scope :order_yomi, -> { order arel_table[:yomi_suuji] }

  def name(is_ja)
    is_ja ? japanese : english
  end

  def head
    yomi_suuji[0,2]
  end

  def show_path(is_ja)
    path_str = "#{self.class.name.underscore}_path"
    Rails.application.routes.url_helpers.send(path_str, self, locale: is_ja ? :ja : :en)
  end

  def preview_show_path(is_ja)
    path_str = "preview_show_#{self.class.name.underscore}_path"
    Rails.application.routes.url_helpers.send(path_str, self, locale: is_ja ? :ja : :en)
  end
end
