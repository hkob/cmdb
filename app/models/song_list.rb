# @!attribute keyword
#   @return [String] Keyword
# @!attribute book_id
#   @return [Fixnum] 書籍ID
# @!attribute device_id
#   @return [Fixnum] デバイスID
# @!attribute concert_id
#   @return [Fixnum] コンサートID
# @!attribute sort_order
#   @return [Fixnum] 並び順

class SongList < ActiveRecord::Base
  validates :keyword, :sort_order, presence: true

  # @return [Book] 対応するシングル
  belongs_to :book
  # @return [Concert] 対応するコンサート
  belongs_to :concert
  # @return [Device] 対応するシングル
  belongs_to :device
  # @return [Album] 対応するアルバム
  belongs_to :album, foreign_key: :device_id
  # @return [Single] 対応するシングル
  belongs_to :single, foreign_key: :device_id
  # @return [Single] 対応するビデオ
  belongs_to :video, foreign_key: :device_id

  # @return [Array<SongListSong>] 対応する曲リスト曲一覧
  has_many :song_list_songs, dependent: :destroy
  # @return [Array<ConcertHall>] 対応するコンサートホール一覧
  has_many :concert_halls, dependent: :destroy

  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }
  scope :book_is, -> b { where arel_table[:book_id].eq b.id }
  scope :concert_is, -> c { where arel_table[:concert_id].eq c.id }
  scope :device_is, -> d { where arel_table[:device_id].eq d.id }
  scope :device_type_value_is, -> t { joins(:device).merge(Device.only_type(t)) }
  scope :include_concert, -> { includes(:concert).merge(Concert.include_title) }
  scope :include_device, -> { includes(:device).merge(Device.include_title) }
  scope :include_album, -> { includes(:album).merge(Album.include_title) }
  scope :include_single, -> { includes(:single).merge(Single.include_title) }
  scope :include_song_list_songs, -> { includes(:song_list_songs) }
  scope :only_concert, -> { where.not(arel_table[:concert_id].eq nil) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }

  def person_instrumental_hash
    ans = Hash.new
    song_list_songs.order_sort_order.each do |sls|
      sps = sls.song_people.include_person.include_instrumental.order_instrumental_sort_order
      sps.each do |sp|
        person = sp.person
        instrumental = sp.instrumental
        ans[person] ||= {}
        ans[person][instrumental] ||= []
        ans[person][instrumental] << sls.sort_order
      end
    end
    ans
  end

  def song_list_songs_from_array(array)
    array.each.with_index(1) do |line, so|
      sk, iss, jv, ev, jt, et = line
      sls = song_list_song_factory song_list: self, sort_order: so, song: (sk && song_factory(sk)), j_ver: jv, e_ver: ev, j_title: jt, e_title: et
      if iss
        iss.each_slice(2) do |i, p|
          song_person_factory(song_list_song: sls, person_key: p, instrumental_key: i)
        end
      end
    end
  end
end
