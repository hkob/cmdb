# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute j_subtitle
#   @return [String] 日本語サブタイトル
# @!attribute e_subtitle
#   @return [String] 英語サブタイトル
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント
# @!attribute conert_type
#   @return [Fixnum] コンサートタイプ
# @!attribute from_id
#   @return [Fixnum] 開始日ID
# @!attribute to_id
#   @return [Fixnum] 終了日ID
# @!attribute has_song_list
#   @return [Boolean] 曲リストがあれば true
# @!attribute has_product
#   @return [Boolean] 名産品があれば true
# @!attribute band_id
#   @return [Fixnum] バンドID
# @!attribute num_of_performances
#   @return [Fixnum] 公演数
# @!attribute num_of_halls
#   @return [Fixnum] 会場数
# @!attribute sort_order
#   @return [Fixnum] 並び順

class Concert < ActiveRecord::Base
  include Name

  validates :title_id, :concert_type, :from_id, :num_of_performances, :num_of_halls, :sort_order, presence: true
  validates :has_song_list, :has_product, inclusion: { in: [ true, false ] }
#  validates :param5, uniqueness: true
  validates :sort_order, uniqueness: { scope: %i( concert_type ) }

  enum concert_type: { tour: 1, live: 2, festival: 3, etc: 4 }

  # @return [Band] 対応するバンド
  belongs_to :band
  # @return [EventDate] 対応する開始日
  belongs_to :from, class_name: :EventDate
  # @return [Title] 対応するタイトル
  belongs_to :title
  # @return [EventDate] 対応する終了日
  belongs_to :to, class_name: :EventDate

  # @return [Array<SongList>] 対応するソングリスト一覧
  has_many :song_lists, dependent: :destroy
  # @return [Array<ConcertHall>] 対応するコンサートホール一覧
  has_many :concert_halls, dependent: :destroy

#  has_many :ddds, class_name: 'CCC', foreign_key: :ddd_id

  scope :concert_type_value_is, -> v { where arel_table[:concert_type].eq v }
  scope :concert_type_str_is, -> s { concert_type_value_is concert_types[s.to_s] }
  scope :include_band, -> { includes(:band).merge(Band.include_title) }
  scope :include_from, -> { includes(:from) }
  scope :include_to, -> { includes(:to) }
  scope :include_title, -> { includes :title }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :order_date, -> { joins(:from).merge(EventDate.order_date) }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }

#  # @param [String] arg1 説明
#  # @return [Concert] XXX な Concert
#  def self.rrr(arg1)
#  end

  # @return [String] 公演数(ホール数)
  # @note ホール数が一緒であれば省略
  def num_of_concerts
    "#{ num_of_performances }(#{ num_of_halls })"
  end

  # @param [String] dt デバイスタイプ
  # @param [Fixnum] so 並び順
  # @return [Device] 対応する Device
  def self.get_object(dt, so)
    self.sort_order_value_is(so).concert_type_str_is(dt).take
  end

  def concert_halls_from_array(array)
    array.each.with_index(1) do |line, so|
      dt, hk, sl, jhs, ehs, jc, ec = line
      concert_hall_factory concert: self, sort_order: so, date: dt, hall_key: hk, j_hall_sub: jhs, e_hall_sub: ehs, j_comment: jc, e_comment: ec, song_list: sl
    end
  end
end
