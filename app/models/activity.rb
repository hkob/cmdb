# @!attribute activity_type
#   @return [Fixnum] アクティビティタイプ
# @!attribute title_id
#   @return [Fixnum] タイトルID
# @!attribute company_id
#   @return [Fixnum] 企業ID (放送局、タイアップ先)
# @!attribute song_id
#   @return [Fixnum] 曲ID
# @!attribute from_id
#   @return [Date] 開始日ID
#   @return [Boolean] 開始日に日がない場合に true
# @!attribute to_id
#   @return [Date] 終了日ID
# @!attribute j_comment
#   @return [String] 日本語コメント
# @!attribute e_comment
#   @return [String] 英語コメント
# @!attribute sort_order
#   @return [Fixnum] 並び順
#
class Activity < ActiveRecord::Base
  include Name

  validates :activity_type, :title_id, :sort_order, presence: true
  validates :sort_order, uniqueness: { scope: %i( activity_type ) }

  enum activity_type: { cm: 1, regular_tv: 2, regular_radio: 3, tv_program: 4, radio_program: 5, magazine: 6 }

  # @return [Company] 対応する企業
  belongs_to :company
  # @return [EventDate] 対応する開始日
  belongs_to :from, class_name: :EventDate, foreign_key: :from_id
  # @return [Song] 対応する曲
  belongs_to :song
  # @return [Title] 対応するタイトル
  belongs_to :title
  # @return [EventDate] 対応する終了日
  belongs_to :to, class_name: :EventDate, foreign_key: :to_id

  scope :activity_type_str_is, -> s { where arel_table[:activity_type].eq activity_types[s] }
  scope :from_year_is, -> y { joins(:from).merge(EventDate.year_is(y)) }
  scope :include_from, -> { includes :from }
  scope :include_title, -> { includes :title }
  scope :include_to, -> { includes :to }
  scope :order_from, -> { joins(:from).merge(EventDate.order_date) }
  scope :order_sort_order, -> { order arel_table[:sort_order] }
  scope :order_to, -> { joins(:to).merge(EventDate.order_date) }
  scope :sort_order_value_is, -> v { where arel_table[:sort_order].eq v }
  scope :to_year_is, -> y { joins(:to).merge(EventDate.year_is(y)) }

  def comment(is_ja)
    is_ja ? j_comment : e_comment
  end

  def from_event_title
    Activity.human_attribute_name "#{ activity_type }_from_event_title"
  end

  def to_event_title
    Activity.human_attribute_name "#{ activity_type }_to_event_title"
  end
end
