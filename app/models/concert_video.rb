# @!attribute concert_song_list_song_id
#   @return [Fixnum] コンサート側のソングリスト曲ID
# @!attribute video_song_list_song_id
#   @return [Fixnum] ビデオ側のソングリスト曲ID

class ConcertVideo < ActiveRecord::Base
  validates :concert_song_list_song_id, :video_song_list_song_id, :concert_hall_id, presence: true

  # @return [SongListSong] 対応するソングリスト曲(コンサート側)
  belongs_to :concert_song_list_song, class_name: :SongListSong, foreign_key: :concert_song_list_song_id
  # @return [SongListSong] 対応するソングリスト曲(ビデオ側)
  belongs_to :video_song_list_song, class_name: :SongListSong, foreign_key: :video_song_list_song_id
  # @return [ConcertHall] 対応するコンサートホール
  belongs_to :concert_hall

  scope :concert_hall_is, -> o { where arel_table[:concert_hall_id].eq o.id }
  scope :concert_song_list_song_is, -> o { where arel_table[:concert_song_list_song_id].eq o.id }
  scope :include_concert_song_list_song, -> { includes(:concert_song_list_song) }
  scope :include_concert_song_list_song_with_song_list, -> { include_concert_song_list_song.merge(SongListSong.include_song_list) }
#  scope :include_video_song_list_song, -> { includes(:video_song_list_song).merge(SongListSong.include_song_list) }
  scope :include_video_song_list_song, -> { includes(:video_song_list_song) }
  scope :video_song_list_song_is, -> o { where arel_table[:video_song_list_song_id].eq o.id }

#  # @return [Array<ConcertVideo>] sort_order 順に並んだ ConcertVideo 一覧を得る
#  scope :order_sort_order, -> { order arel_table[:sort_order] }
#  # @return [Array<ConcertVideo>] qqq が v な ConcertVideo 一覧を得る
#  scope :qqq_value_is, -> v { where arel_table[:qqq].eq v }

#  # @param [String] arg1 説明
#  # @return [ConcertVideo] XXX な ConcertVideo
#  def self.rrr(arg1)
#  end

#  # @param [String] arg1 説明
#  # @return [ConcertVideo] 説明
#  def ttt(arg1)
#  end
end
