# @!attribute date
#   @return [Date] 日付
# @!attribute year_id
#   @return [Fixnum] 年ID
# @!attribute month
#   @return [Fixnum] 月
# @!attribute day
#   @return [Fixnum] 日

class EventDate < ActiveRecord::Base
  validates :date, :year_id, presence: true

  # @return [Year] 対応する年
  belongs_to :year

  # @return [Array<Activity>] 対応する活動(開始日)
  has_many :activity_froms, class_name: :Activity, foreign_key: :from_id, dependent: :destroy
  # @return [Array<Activity>] 対応する活動(終了日)
  has_many :activity_tos, class_name: :Activity, foreign_key: :to_id, dependent: :destroy
  # @return [Array<Album>] 対応するアルバム
  has_many :albums, dependent: :destroy
  # @return [Array<Book>] 対応する書籍
  has_many :books, dependent: :destroy
  # @return [Array<Concert>] 対応するコンサート(開始日)
  has_many :concert_froms, class_name: :Concert, foreign_key: :from_id, dependent: :destroy
  # @return [Array<ConcertHall>] 対応するコンサートホール
  has_many :concert_halls, dependent: :destroy
  # @return [Array<Concert>] 対応するコンサート(終了日)
  has_many :concert_tos, class_name: :Concert, foreign_key: :to_id, dependent: :destroy
  # @return [Array<Song>] 対応する曲
  has_many :songs, dependent: :destroy

  scope :group_month, -> { group(arel_table[:month]) }
  scope :include_year, -> { includes :year }
  scope :no_month, -> { where arel_table[:month].eq nil }
  scope :order_date, -> { order arel_table[:date] }
  scope :order_date_desc, -> { order_date.desc }
  scope :year_is, -> y { where arel_table[:year_id].eq y.id }
  scope :month_is, -> m { where arel_table[:month].eq(m == 13 ? nil : m) }

  def name
    I18n.l date, format: month && day ? :long : month ? :month : :y_only
  end

  def <=>(other)
    date <=> other.date
  end

end
