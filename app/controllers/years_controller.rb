class YearsController < ApplicationController
  def index
    @years = Year.order_year_desc
    @num_of_albums = Album.joins(:event_date).group(:year_id).count
    @num_of_singles = Single.joins(:event_date).group(:year_id).count
    @num_of_concert_halls = ConcertHall.joins(:event_date).group(:year_id).count(:id)
    @num_of_books = Book.joins(:event_date).group(:year_id).count(:id)
    @num_of_from_activities = Activity.group(:from_id).count(:id)
    @num_of_to_activities = Activity.group(:to_id).count(:id)
  end

  def show
    @year = object_from_params_id Year
    @month_hash = @year.event_dates.merge(EventDate.group_month).count
    @month_hash[13] = @month_hash[nil] if @month_hash[nil]
    @month_hash.delete(nil)
    @key = params[:key].try(:to_i) || @month_hash.keys.first
    @key = @month_hash.keys.first unless @month_hash[@key]
    @devices = Device.year_is(@year).include_title.include_event_date.merge(EventDate.month_is(@key)).order_date
    @concert_halls = ConcertHall.year_is(@year).include_hall.include_concert.include_event_date.merge(EventDate.month_is(@key).order_date)
    @books = Book.year_is(@year).include_title.include_event_date.merge(EventDate.month_is(@key)).order_date
    @hash = {}
    dcb = (@devices + @concert_halls + @books).map { |obj| [ obj.event_date, obj ] }
    af = Activity.from_year_is(@year).include_from.merge(EventDate.month_is(@key)).order_from.map { |obj| [ obj.from, obj ] }
    at = Activity.to_year_is(@year).include_to.merge(EventDate.month_is(@key)).order_to.map { |obj| [ obj.to, obj ] }
    (dcb + af + at).each do |(date, obj)|
      @hash[date] ||= []
      @hash[date] << obj
    end
    @years = Year.order_year_desc
  end
end
