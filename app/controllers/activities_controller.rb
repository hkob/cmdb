class ActivitiesController < ApplicationController

  def index
    @activity_type = params[:activity_type] || 'cm'
    @activities = Activity.activity_type_str_is(@activity_type).include_title.order_sort_order
  end

  def show
    @activity = object_from_params_id Activity
    @key = params[:key] || 'cm'
    @activities = Activity.activity_type_str_is(@activity.activity_type).include_title.order_sort_order
  end
end
