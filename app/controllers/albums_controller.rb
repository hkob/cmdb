class AlbumsController < ApplicationController

  def index
    @device_type = params[:device_type].try(:to_i) || 1
    @albums = Album.device_type_value_has(@device_type).include_title.include_singer.include_event_date.order_date
    @asn = Album::AlbumStr2Num
    @ans = Album::AlbumNum2Str
  end

  def show
    @album = object_from_params_id Album
    @media = @album.media.order_sort_order
    @song_lists = @album.song_lists
    @key = params[:key] || 'base_information'
    @device_type = @album.device_types.first
    @albums = Album.device_type_value_has(@device_type).include_title.order_date
    case @key
    when 'base_information'
      @singer = @album.singer
      @whole_time = @album.whole_time(@is_ja)
    end
  end
end
