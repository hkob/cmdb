class SongsController < ApplicationController
  include Head

  def index
    @head, @heads, @lhead_hash, @subtitle, @ids = get_heads(params[:head], @is_ja)
    @songs = Song.head_value_is(@head).include_title.order_yomi
    song_head_arel = Song.head_value_is(@head)
    @lyrics = hash_array_from_array(Lyric.joins(:song).merge(song_head_arel).include_person) { |l| [ l.song_id, l ] }
    @musics = hash_array_from_array(Music.joins(:song).merge(song_head_arel).include_person) { |d| [ d.song_id, d ] }
    song_list_song_arel = SongListSong.joins(:song).merge(song_head_arel).group(:song_id)
    @num_of_albums = song_list_song_arel.only_album.count(:id)
    @num_of_singles = song_list_song_arel.only_single.count(:id)
    @num_of_concerts = song_list_song_arel.only_concert.joins(song_list: :concert_halls).count(:hall_id)
  end

  def list_by_date
    @year = objects_from_params Year
    @year_hash = Song.joins(:event_date).merge(EventDate.group(EventDate.arel_table[:year_id])).count
    @years = Year.ids_are(@year_hash.keys).order_year
    @year ||= @years.first
    @songs = Song.year_is(@year).include_event_date.include_title.order_date.order_yomi
    song_year_arel = Song.year_is(@year)
    @lyrics = hash_array_from_array(Lyric.joins(:song).merge(song_year_arel).include_person) { |l| [ l.song_id, l ] }
    @musics = hash_array_from_array(Music.joins(:song).merge(song_year_arel).include_person) { |d| [ d.song_id, d ] }
    song_list_song_arel = SongListSong.joins(:song).merge(song_year_arel).group(:song_id)
    @num_of_albums = song_list_song_arel.only_album.count(:id)
    @num_of_singles = song_list_song_arel.only_single.count(:id)
    @num_of_concerts = song_list_song_arel.only_concert.joins(song_list: :concert_halls).count(:hall_id)
  end

  def show
    @song = object_from_params_id Song
    @all_songs = @song.all_objects
    @only_one = @all_songs.count == 1
    @devices = {
      'album' => SongListSong.songs_are(@all_songs).include_device.include_song.only_album.order_device_year,
      'single' => SongListSong.songs_are(@all_songs).include_device.include_song.only_single.order_device_year,
      'video' => SongListSong.songs_are(@all_songs).include_device.include_song.only_video.order_device_year,
    }
    @concerts = SongListSong.songs_are(@all_songs).joins(song_list: :concert).merge(Concert.order_date).merge(SongList.order_sort_order)

    @key = params[:key] || 'base_information'
    @key = 'base_information' if @devices[@key] && @devices[@key].count == 0
    if @key == 'base_information'
      @activities = @song.activities.order_sort_order
    end
    @songs = Song.head_value_is(@song.head1).include_title.order_yomi
  end
end
