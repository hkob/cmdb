class BooksController < ApplicationController

  def index
    @book_type = params[:book_type] || 'picture_book'
    @books = Book.book_type_str_is(@book_type).include_title.include_event_date.order_sort_order
  end

  def show
    @book = object_from_params_id Book
    @song_list = @book.song_list
    @books = Book.book_type_str_is(@book.book_type).include_title.order_sort_order
  end
end
