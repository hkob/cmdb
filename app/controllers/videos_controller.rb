class VideosController < ApplicationController

  def index
    @device_type = params[:device_type].try(:to_i) || 1
    @videos = Video.device_type_value_has(@device_type).include_title.include_singer.include_event_date.order_date
    @asn = Video::VideoStr2Num
    @ans = Video::VideoNum2Str
  end

  def show
    @video = object_from_params_id Video
    @media = @video.media.include_company.order_sort_order
    @song_lists = @video.song_lists
    @key = params[:key] || 'base_information'
    @device_type = @video.device_types.first
    @videos = Video.device_type_value_has(@device_type).include_title.order_date
    @recorded_at = {}
    case @key
    when 'base_information'
      @singer = @video.singer
      @whole_time = @video.whole_time(@is_ja)
      @song_lists.each do |sl|
        sl.song_list_songs.include_video_to_concert_videos.each do |sls|
          sls.video_to_concert_videos.each do |vcv|
            @recorded_at[vcv.concert_hall] = true
          end
        end
      end
    end
  end
end
