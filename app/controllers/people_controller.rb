class PeopleController < ApplicationController
  include Head

  def index
    @head, @heads, @lhead_hash, @subtitle, @ids = get_heads(params[:head], @is_ja)
    @people = Person.head_value_is(@head).include_title.order_yomi
    @num_of_albums = Album.singer_head_value_is(@head).group(:singer_id).count(:id)
    @num_of_singles = Single.singer_head_value_is(@head).group(:singer_id).count(:id)
    @num_of_lyrics = Lyric.person_head_value_is(@head).group(:person_id).count(:id)
    @num_of_musics = Music.person_head_value_is(@head).group(:person_id).count(:id)
    @num_of_song_people = SongPerson.person_head_value_is(@head).group(:person_id).count(:id)
    @num_of_bands = BandMember.person_head_value_is(@head).group(:person_id).count(:id)
  end

  def show
    @person = object_from_params_id Person
    @people = @person.all_objects
    @hash = {}
    lyrics = Lyric.people_are(@people).order_song_date
    @hash['lyrics'] = lyrics unless lyrics.empty?
    musics = Music.people_are(@people).order_song_date
    @hash['musics'] = musics unless musics.empty?
    albums = SongPerson.people_are(@people).only_album
    @hash['albums'] = albums unless albums.empty?
    singles = SongPerson.people_are(@people).only_single
    @hash['singles'] = singles unless singles.empty?
    bands = BandMember.people_are(@people).order_band_yomi
    @hash['bands'] = bands unless bands.empty?

    @key = params[:key] || @hash.keys.first
    @key = @hash.keys.first unless @hash[@key]
    @people = Person.head_value_is(@person.head1).include_title.order_yomi
  end
end
