class ConcertsController < ApplicationController
  def index
    @concert_type = params[:concert_type] || 'tour'
    @concert_type_count = Concert.group(:concert_type).count(:id)
    @concerts = Concert.concert_type_value_is(Concert.concert_types[@concert_type]).include_from.include_to.include_title.include_band.order_sort_order
  end

  def show
    @concert = object_from_params_id Concert
    @song_lists = @concert.song_lists
    @key = params[:key] || 'base_information'
    @concert_halls = @concert.concert_halls
    @concerts = Concert.concert_type_value_is(Concert.concert_types[@concert.concert_type]).include_title.order_sort_order
  end
end
