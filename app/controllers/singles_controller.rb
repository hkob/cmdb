class SinglesController < ApplicationController
  def index
    @device_type = params[:device_type].try(:to_i) || 1
    @singles = Single.device_type_value_has(@device_type).include_title.include_singer.include_event_date.order_date
    @ssn = Single::SingleStr2Num
    @sns = Single::SingleNum2Str
  end

  def show
    @single = object_from_params_id Single
    @media = @single.media.order_sort_order
    @song_lists = @single.song_lists
    @key = params[:key] || 'base_information'
    @device_type = @single.device_types.first
    @singles = Single.device_type_value_has(@device_type).include_title.order_date
    case @key
    when 'base_information'
      @singer = @single.singer
      @whole_time = @single.whole_time(@is_ja)
    end
  end
end
