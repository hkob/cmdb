class WelcomeController < ApplicationController
  def index
    @main_models = [ Album, Single, Video, Medium, Concert, ConcertHall, Song, Book, Activity, Person, EventDate ]
    @sub_models = [ Band, Company, Year, Instrumental, Prefecture, Hall, SongList, SongListSong, Title ]
  end
end
