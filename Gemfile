source 'https://rubygems.org'

gem 'rails', '~> 4.2.0'

### OpenShift Online changes:

# Fix the conflict with the system 'rake':
gem 'rake', '~> 0.9.6'

# Support for databases and environment.
# Use 'sqlite3' for testing and development and mysql and postgresql
# for production.
#
# To speed up the 'git push' process you can exclude gems from bundle install:
# For example, if you use rails + mysql, you can:
#
# $ rhc env set BUNDLE_WITHOUT="development test postgresql"
#

# kaminari
gem 'kaminari'

# HTML abstraction markup language
gem 'haml'
gem 'haml-rails'

# twitter bootstrap css & javascript toolkit
gem 'twitter-bootswatch-rails', '~> 3.3.0'

# twitter bootstrap helpers gem, e.g., alerts etc...
gem 'twitter-bootswatch-rails-helpers'

# Embed the V8 JavaScript interpreter into Ruby
gem 'therubyracer', '~> 0.12.1'

# a documentation generation tool for the Ruby programming language
gem 'yard'
gem 'yard-activerecord'

gem 'factory_girl_rails', '~> 4.5.0'

group :development, :test do
  gem 'sqlite3'
  gem 'minitest'
  gem 'thor'
  gem 'rspec-rails', '~> 3.1.0'
  gem 'spring-commands-rspec', '~> 1.0.4'
  gem 'guard-rspec', '~> 4.5.0'
  gem 'quiet_assets', '~> 1.1.0'
# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'bullet'
end

group :test do
  gem 'faker', '~> 1.4.3'
  gem 'capybara', '~> 2.4.4'
  gem 'poltergeist', '~> 1.6.0'
  gem 'launchy', '~> 2.4.3'
  gem 'turnip', '~> 1.2.4'
  gem 'simplecov', '~> 0.9.1'
  gem 'database_rewinder', '~> 0.4.1'
end

group :production, :postgresql do
  gem 'pg'
end

### / OpenShift changes

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

