dt = Single::SingleStr2Num[:ep_single]
if Single.device_type_value_has(dt).sort_order_value_is(3).take
  print "... skip\n"
else
  single = single_factory title_key: :get_smile, device_type: :ep_single, add_device_type: %i( single ), date: '1988/2/25', sort_order: 3, number: '3rd', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4012.html'
  single.media_from_array [
    [ :ep, 'K-1567', '1st', false, @wp, '700' ],
    [ :kt, 'LKC-2056', '1st', false, @wp, '1,000' ],
    [ :cds, '10SL-2', '1st', false, @wp, '937' ],
    [ :cds, '10SL-2', '2nd', false, @wmj, '937' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/get-smile-single/id528975338', '1st', true, @wmj, '500' ]
  ]

  sl = song_list_factory device: single, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :get_smile, %i( VOCAL iChisatoMoritaka ARRANGE iKenShima ) ],
    [ :good_bye_season, %i( VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto ) ]
  ]
  print "\n"
end
