if Book.book_type_str_is(:picture_book).sort_order_value_is(1).take
  print "... skip\n"
else
  book = book_factory book_type: :picture_book, title_key: :shuka, publisher_key: :kindai_eigasha, photographer_key: :iYutakaNishimura, isbn: '4-7648-1448-X C0076 P1854E', price: '1,854', date: '1987/8/5', sort_order: 1, link: 'http://www.moritaka-chisato.com/discogprahy/product/4457.html'

  sl = song_list_factory book: book, keyword: :A, sort_order: 1
  sl.song_list_songs_from_array [
    [ nil, [], nil, nil, '哀楽・都', 'Airaku - Miyako' ],
    [ nil, [], nil, nil, '春華・章', 'Shunka - Sho' ],
    [ nil, [], nil, nil, '夜楽・調', 'Yaraku - Shirabe' ],
    [ nil, [], nil, nil, '眩耀・女', 'Genyo - Onnna' ],
    [ nil, [], nil, nil, '失楽・園', 'Shitsuraku - En' ],
  ]
  print "\n"
end
