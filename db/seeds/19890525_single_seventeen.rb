dt = Single::SingleStr2Num[:single]
if Single.device_type_value_has(dt).sort_order_value_is(7).take
  print "... skip\n"
else
  single = single_factory title_key: :seventeen, device_type: :single, date: '1989/5/25', sort_order: 7, number: '7th', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4167.html'
  single.media_from_array [
    [ :kt, '09L5-4084', '1st', false, @wp, '937' ],
    [ :cds, '09L3-4084', '1st', false, @wp, '937' ],
    [ :cds, '09L3-4084', '2nd', false, @wmj, '937' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/17cai-single/id528969258', '1st', true, @wmj, '500' ]
  ]

  sl = song_list_factory device: single, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :seventeen, @vcah ],
    [ :hatachi, @vcah ]
  ]
  print "\n"
end
