if Concert.concert_type_str_is(:live).sort_order_value_is(1).take
  print "... skip\n"
else
  concert = concert_factory j_title: '渋谷ライブイン ファーストライブ ｢FIRST PARTY｣', e_title: "First Live at Shibuya Live-in `FIRST PARTY'", yomi: 'しぶやらいぶいん　ふぁーすとらいぶ', concert_type: :live, from: '1987/9/7', has_song_list: true, sort_order: 1

  sl = song_list_factory concert: concert, keyword: :A, sort_order: 1
  sl.song_list_songs_from_array %i( otisreddingni_kanpai period miss_lady namida_good_bye anohino_photograph ringoshuno_rule yumeno_owari weekend_blue overheat_night new_season )

  concert_hall_factory(concert: concert, sort_order: 1, hall_key: :shibuya_live_in, date: '1987/9/7', song_list: sl)
  print "\n"
end
