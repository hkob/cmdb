if Concert.concert_type_str_is(:etc).sort_order_value_is(1).take
  print "... skip\n"
else
  concert = concert_factory j_title: '「ザ・ミーハー」, その他', e_title: '"The Mi-HA-", Etc', yomi: 'ざ　みーはー　そのた', concert_type: :etc, from: '1988/5/23', to: '1988/8/12', has_song_list: true, sort_order: 1, num_of_performances: 10, num_of_halls: 10

  sla = song_list_factory concert: concert, keyword: :A, sort_order: 1
  slb = song_list_factory concert: concert, keyword: :B, sort_order: 2

  sla.song_list_songs_from_array(%i( new_season otisreddingni_kanpai namida_good_bye yumeno_owari do_you_know_the_way_to_san_jose anohino_photograph romantic kiss_the_night good_bye_season yokohama_one_night mi_ha_ overheat_night forty_seven_har_nights cant_say_good_bye get_smile ) + @encore + %i( the_mi_ha_ ))
  slb.song_list_songs_from_array %i( new_season yokohama_one_night romantic kiss_the_night mi_ha_ get_smile )

  concert.concert_halls_from_array [
    [ '1988/5/23', :instick_shibaura_factory ],
    [ '1988/6/26', :hibiya_yagai_ongakudo, sla, '「ザ・ミーハー」', '"The Mi-HA-"' ],
    [ '1988/7/7', :osaka_kousei_nenkin_kaikan, sla, '中ホール', 'Medium hall' ],
    [ '1988/7/17', :nagano_live_house, sla ],
    [ '1988/7/22', :niigata_kenmin_kaikan, sla, '大ホール', 'Big hall' ],
    [ '1988/7/24', :enoshima_seaside, ],
    [ '1988/7/26', :mukogaoka_yuen, nil, 'ラジオ日本公録', 'Public recording for Radio-Nippon' ],
    [ '1988/8/4', :ariake_mza_convention, nil, "Bay Side Summer Gal's Festival|過労で途中倒れる", "Bay Side Summer Gal's Festival|The overwork incapacitates her for the concert." ],
    [ '1988/8/11', :omiya_sonic_city, slb ],
    [ '1988/8/12', :shibuya_koukado, nil, 'MUSIC WAVE', 'MUSIC WAVE' ]
  ]
  print "\n"
end
