dt = Album::AlbumStr2Num[:album]
if Album.device_type_value_has(dt).sort_order_value_is(4).take
  print "... skip\n"
else
  album = album_factory title_key: :hijitsuryokuha_sengen, device_type: :album, date: '1989/7/25', minutes: 55, seconds: 32, sort_order: 4, number: '4th', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4038.html'
  album.media_from_array [
    [ :kt, '25L4-85', :first, false, @wp, '2,560' ],
    [ :cd, '29L2-85', :first, false, @wp, '3,008' ],
    [ :cd, 'WPCL-505', :second, true, @wmj, '2,400' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/fei-shi-li-pai-xuan-yan/id528974085', :first, true, @wmj, '2,100' ],
  ]

  sl = song_list_factory device: album, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :seventeen, %i( VOCAL iChisatoMoritaka ARRANGE iMasataroNaoe AGUITAR iMasataroNaoe EGUITAR iGiroBando BASS iYujiMada KEYBOARDS iYuichiTanaya DRUMS iHiroshiYabe), '[カーネーション・ヴァージョン]', '[Carnation version]' ],
    [ :korekkiri_byebye, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito ) ],
    [ :daite, %i( VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi CHORUS iChisatoMoritaka GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi CHORUS iYuichiTakahashi DRUMS iNaokiKimura BASS iKazuyoshiYamauchi TROMBONE iCarlosKanno SYNPR iNaokiSuzuki ) ],
    [ :hijitsuryokuha_sengen, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito CHORUS iChisatoMoritaka CHORUS iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito ) ],
    [ :kondo_watashi_dokoka, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito ) ],
    [ :hadakaniha_naranai, %i( VOCAL iChisatoMoritaka ARRANGE iMasataroNaoe AGUITAR iMasataroNaoe CHORUS iMasataroNaoe EGUITAR iGiroBando BASS iYujiMada KEYBOARDS iYuichiTanaya CHORUS iYuichiTanaya DRUMS iHiroshiYabe ) ],
    [ :watashiha_onchi, %i( VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi CHORUS iYuichiTakahashi ) ],
    [ :shiritagari, %i( VOCAL iChisatoMoritaka ARRANGE iCarlosKanno ARRANGE iYasuakiMaejima SPERCUSSION iChisatoMoritaka PERCUSSION iCarlosKanno PIANO iYasuakiMaejima GUITAR iAkiraWada DRUMS iMansakuKimura BASS iHiroshiSawada SYNTHESIZER iHiromichiTsugaki TRUMPET iKenjiYoshida TRUMPET iMasahiroKobayashi TROMBONE iOsamuMatsuki ALTOSAX iHisashiYoshinaga TENORSAX iTakeruMuraoka ) ],
    [ :wakasugita_koi, %i( VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi GUITAR iYuichiTakahashi KEYBOARDS iYuichiTakahashi CHORUS iYuichiTakahashi DRUMS iNaokiKimura BASS iKazuyoshiYamauchi SYNPR iNaokiSuzuki ) ],
    [ :akunno_higeki, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito ) ],
    [ :yoruno_entotsu, %i( VOCAL iChisatoMoritaka ARRANGE iCarnation AGUITAR iMasataroNaoe EGUITAR iMasataroNaoe CHORUS iMasataroNaoe EGUITAR iGiroBando BASS iYujiMada CHORUS iYujiMada KEYBOARDS iYuichiTanaya CHORUS iYuichiTanaya DRUMS iHiroshiYabe ) ],
    [ :sonogono_watashi, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito CHORUS iHideoSaito ) ],
    [ :yumeno_nakano_kiss, %i( VOCAL iChisatoMoritaka ARRANGE iYuichiTakahashi DRUMS iChisatoMoritaka ORGAN iChisatoMoritaka BASS iYuichiTakahashi CHORUS iYuichiTakahashi TAMBOURINE iYuichiTakahashi GUITAR iYukioSeto ) ],
    [ :seventeen, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito ALLINST iHideoSaito PROGRAMS iHideoSaito ), '[オレンジ・ミックス]', '[Orange Mix]' ]
  ]
  print "\n"
end
