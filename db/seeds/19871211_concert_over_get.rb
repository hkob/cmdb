if Concert.concert_type_str_is(:live).sort_order_value_is(2).take
  print "... skip\n"
else
  band = band_factory(:ms_band)
  band.band_members_from_array %i( VOCAL iChisatoMoritaka SAXOPHONE iGinjiOgawa KEYBOARDS iHisanoriMizuno DRUMS iMasayukiMuraishi PERCUSSION iYouichiOkabe GUITAR iHiroyukiKato CHORUS iYukariFujio )

  concert = concert_factory j_title: '「OVERHEAT. NIGHT」, 「GET SMILE」', e_title: '"OVERHEAT. NIGHT",  "GET SMILE"', yomi: 'おーばーひーとないと　げっとすまいる', concert_type: :live, from: '1987/12/11', to: '1988/3/11', has_song_list: true, sort_order: 2, num_of_performances: 3, num_of_halls: 3, band: band

  sl = song_list_factory concert: concert, keyword: :A, sort_order: 1
  sl.song_list_songs_from_array %i( good_bye_season namida_good_bye anohino_photograph ringoshuno_rule yumeno_owari weekend_blue get_smile overheat_night new_season )

  concert.concert_halls_from_array [
    [ '1987/12/11', :osaka_kousei_nenkin_kaikan, nil, '中ホール', 'Medium hall', '「OVERHEAT. NIGHT」', '"OVERHEAT. NIGHT"' ],
    [ '1987/12/17', :nihon_seinenkan, sl, nil, nil, '「GET SMILE」|<ビデオ収録>', '"GET SMILE"|<Video Recording>' ],
    [ '1988/3/11', :tokushima_vega_hall, nil, nil, nil, '「GET SMILE」', '"GET SMILE"' ]
  ]
  print "\n"
end
