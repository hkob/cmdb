if Activity.activity_type_str_is(:regular_tv).sort_order_value_is(1).take
  print "... skip\n"
else
  activity = activity_factory activity_type: :regular_tv, j_title: '銀河テレビ小説 まんが道・青春編', e_title: "MANGA MICHI -- SEISHUN-HEN", yomi: 'ぎんがてれびしょうせつ　まんがまち　せいしゅんへん', company_key: :nhk, from: '1987/7/27', to: '1987/8/14', sort_order: 1
  print "\n"
end
