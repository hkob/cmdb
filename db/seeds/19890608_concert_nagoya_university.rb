if Concert.concert_type_str_is(:etc).sort_order_value_is(2).take
  print "... skip\n"
else
  concert = concert_factory j_title: '名古屋大学、その他', e_title: 'Nagoya University, Etc', yomi: 'なごやだいがく　そのた', concert_type: :etc, from: '1989/6/8', to: '1989/8/4', has_song_list: false, sort_order: 2, num_of_performances: 4, num_of_halls: 4

  concert.concert_halls_from_array [
    [ '1989/6/8', :nagoya_university ],
    [ '1989/7/30', :kumamoto_techno_research_park, nil, 'TKUサマーブリーズ', 'TKU summer breeze' ],
    [ '1989/8/2', :hiroshima_kamagari, nil, '海と島の博覧会', 'Exhibition of sea and island' ],
    [ '1989/8/4', :miura_kaigan, nil, 'ラジオ日本公録', 'Public recording for Radio-Nippon' ],
  ]
  print "\n"
end
