dt = Video::VideoStr2Num[:cdv]
if Video.device_type_value_has(dt).sort_order_value_is(2).take
  print "... skip\n"
else
  video = video_factory title_key: :overheat_night, device_type: :cdv, date: '1988/11/28', minutes: 23, seconds: 10, sort_order: 2, number: '2nd', singer_key: :iChisatoMoritaka, link: 'http://www.moritaka-chisato.com/discogprahy/product/4313.html'
  video.media_from_array [
    [ :cdv, '24VL-4', :first, false, @wp, '2,400 2,328' ],
    [ :itunes, 'https://itunes.apple.com/jp/music-video/obahito-naito/id534975726', :first, true, @wmj, '400' ],
  ]

  sl = song_list_factory device: video, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :new_season, @vc, '[SLバージョン]', '[SL version]' ],
    [ :yumeno_owari, @vc ],
    [ :period, @vc, '[LPバージョン]', '[LP version]' ],
    [ :weekend_blue, @vc ],
    [ :overheat_night_2, @vc, '(VIDEO Part)', '(VIDEO Part)' ]
  ]
  print "\n"
end
