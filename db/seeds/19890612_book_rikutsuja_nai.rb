if Book.book_type_str_is(:picture_book).sort_order_value_is(2).take
  print "... skip\n"
else
  book = book_factory book_type: :picture_book, title_key: :rikutsuja_nai, publisher_key: :kodansha, photographer_key: :iHaruKimura, isbn: '4-06-103105-8 C0072 P1550E', price: '1,550', date: '1989/6/12', sort_order: 2, link: 'http://www.moritaka-chisato.com/discogprahy/product/4462.html'

  sl = song_list_factory book: book, keyword: :A, sort_order: 1
  sl.song_list_songs_from_array [
    [ nil, [], nil, nil, '新しい季節', '{ATARASHII KISETSU}' ],
    [ nil, [], nil, nil, '素敵だ…', '{SUTEKI-DA...}' ],
    [ nil, [], nil, nil, 'Talk Essay', 'Talk Essay' ],
    [ nil, [], nil, nil, '忘れない', '{WASURENAI}' ],
    [ nil, [], nil, nil, 'なんて不意なサヨナラ', '{NANTE FUI-NA SAYONARA}' ],
    [ nil, [], nil, nil, '夢の終わり', '{YUME-NO OWARI}' ],
    [ nil, [], nil, nil, 'いけない嘘', '{IKENAI USO}' ],
    [ nil, [], nil, nil, '戻れない夏', '{MODORENAI NATSU}' ],
  ]
  print "\n"
end
