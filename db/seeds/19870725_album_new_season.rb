dt = Album::AlbumStr2Num[:album]
if Album.device_type_value_has(dt).sort_order_value_is(1).take
  print "... skip\n"
else
  album = album_factory title_key: :new_season, device_type: :album, date: '1987/7/25', minutes: 42, seconds: 25, sort_order: 1, number: '1st', singer: @cm, j_comment: 'デビューアルバム', e_comment: 'Debut Album', link: 'http://www.moritaka-chisato.com/discogprahy/product/4101.html'
  album.media_from_array [
    [ :lp, 'K-12533', :first, false, @wp, '2,800' ],
    [ :kt, 'LKF-8176', :first, false, @wp, '2,800' ],
    [ :cd, '32XL-216', :first, false, @wp, '3,200 3,008' ],
    [ :cd, 'WPCL-502', :second, true, @wmj, '2,400' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/new-season/id528972314', :first, true, @wmj, '1,900' ],
  ]

  sl = song_list_factory device: album, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :namida_good_bye, %i( VOCAL iChisatoMoritaka ARRANGE iWise GUITAR iKazuhikoIwami KEYBOARDS iYasuakiMaejima KEYBOARDS iYoichiroKakizaki BASS iHiroshiSawada DRUMS iMansakuKimura PERCUSSION iShingoKanno SAXOPHONE iKazuoKamata CHORUS iTheThreePinkies SYNPR iTomoakiArima FRSOLO iChisatoMoritaka) ],
    [ :yumeno_owari, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITAR iHideoSaito KEYBOARDS iNobitaTsukada CYMBALS iRyoSaito TOMS iRyoSaito SIMMONS iRyoSaito CHORUS iHideoSaito) ],
    [ :ringoshuno_rule, %i( VOCAL iChisatoMoritaka ARRANGE iKenShima GUITAR iAkiraWada KEYBOARDS iKenShima BASS iKenjiTakamizu DRUMS iShuichiPONTAMurakami PERCUSSION iShingoKanno SAXOPHONE iShigeoFuchino CHORUS iTheThreePinkies STRINGS iMAEDAStrings SYNPR iTakayoshiUmeno ) ],
    [ :otisreddingni_kanpai, %i( VOCAL iChisatoMoritaka ARRANGE iHiromichiTsugaki KEYBOARDS iHiromichiTsugaki SAXOPHONE iShigeoFuchino TROMBONE iHarumiMita CHORUS iTheThreePinkies SYNPR iTomoakiArima ) ],
    [ :ways, %i( VOCAL iChisatoMoritaka ARRANGE iHiromichiTsugaki KEYBOARDS iHiromichiTsugaki CHORUS iTheThreePinkies SYNPR iTomoakiArima ) ],
    [ :period, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITAR iHideoSaito KEYBOARDS iNobitaTsukada CYMBALS iRyoSaito TOMS iRyoSaito SIMMONS iRyoSaito CHORUS iHideoSaito ), '(アルバム・ヴァージョン)', '(Album version)' ],
    [ :anohino_photograph, %i( VOCAL iChisatoMoritaka ARRANGE iKenShima APIANO iChisatoMoritaka GUITAR iMakotoMatsushita KEYBOARDS iKenShima BASS iYasuoTomikura DRUMS iEijiShimamura PERCUSSION iNobuSaito STRINGS iTOMODAStrings SYNPR iTakayoshiUmeno ) ],
    [ :miss_lady, %i( VOCAL iChisatoMoritaka ARRANGE iWise GUITAR iKazuhikoIwami KEYBOARDS iYasuakiMaejima BASS iHiroshiSawada DRUMS iMansakuKimura PERCUSSION iShingoKanno SAXOPHONE iKazuoKamata TROMBONE iKoyoMurakami BGVOCAL iHiromasaIjichi SYNPR iTomoakiArima ) ],
    [ :new_season, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito SKEYSOLO iChisatoMoritaka GUITAR iHideoSaito KEYBOARDS iNobitaTsukada KEYBOARDS iYasuhikoFukuda BASS iChiharuMikuzuki DRUMS iRyubenTsujino CHORUS iHideoSaito BGVOCAL iYukariFujio ), '(ロング・ヴァージョン)', '(long version)' ]
  ]
  print "\n"
end
