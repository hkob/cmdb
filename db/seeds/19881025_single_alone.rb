dt = Single::SingleStr2Num[:ep_single]
if Single.device_type_value_has(dt).sort_order_value_is(5).take
  print "... skip\n"
else
  single = single_factory title_key: :alone, device_type: :ep_single, add_device_type: %i( single ), date: '1988/10/25', sort_order: 5, number: '5th', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4028.html'
  single.media_from_array [
    [ :ep, '07L7-4018', '1st', false, @wp, '700' ],
    [ :kt, '10L5-4018', '1st', false, @wp, '1,000' ],
    [ :cds, '10L3-4018', '1st', false, @wp, '937' ],
    [ :cds, '10L3-4018', '2nd', false, @wmj, '937' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/alone-single/id528969806', '1st', true, @wmj, '500' ]
  ]

  sl = song_list_factory device: single, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :alone, %i( VOCAL iChisatoMoritaka ARRANGE iShinjiYasuda ) ],
    [ :chase, %i( VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto ) ]
  ]
  print "\n"
end
