if Concert.concert_type_str_is(:live).sort_order_value_is(3).take
  print "... skip\n"
else
  band = band_factory(:animals_2)
  band.band_members_from_array %i( VOCAL iChisatoMoritaka GUITAR iHiroyukiKato SAXOPHONE iGinjiOgawa BASS iKazuyoshiYamauchi DRUMS iNaokiKimura KEYBOARDS iHitoshiKitamura )

  concert = concert_factory j_title: '「非実力派宣言」ツアー', e_title: '"{HI-JITSURYOKUHA SENGEN}" Tour ', yomi: 'ひじつりょくはせんげんつあー', j_subtitle: "森高千里サマーコンサートツアー'89", e_subtitle: "Chisato Moritaka summer concert tour '89", concert_type: :tour, from: '1989/8/11', to: '1989/8/31', has_song_list: true, sort_order: 3, num_of_performances: 5, num_of_halls: 4, band: band

  sl = song_list_factory concert: concert, keyword: :A, sort_order: 1
  sl.song_list_songs_from_array %i( korekkiri_byebye hijitsuryokuha_sengen hatachi new_season overheat_night alone the_stress shiritagari watashiha_onchi kondo_watashi_dokoka sonogono_watashi seventeen hadakaniha_naranai yoruno_entotsu let_me_go get_smile mite ) + @encore + [ [ :daite, [], '[ラスベガス・ヴァージョン]', '[Las Vegas version]' ], [ :seventeen, [], '[オレンジ・ミックス]', '[Orange Mix]' ] ]

  concert.concert_halls_from_array [
    [ '1989/8/11', :niigata_kenmin_kaikan, sl, '大ホール', 'Big hall' ],
    [ '1989/8/21', :nakano_sunplaza, sl, nil, nil, '<ビデオ収録>', '<Video recording>' ],
    [ '1989/8/22', :nakano_sunplaza, sl ],
    [ '1989/8/30', :aichi_kinrou_kaikan, sl ],
    [ '1989/8/31', :osaka_kousei_nenkin_kaikan, sl, '大ホール', 'Big hall' ],
  ]
  print "\n"
end

