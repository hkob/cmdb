dt = Single::SingleStr2Num[:ep_single]
if Single.device_type_value_has(dt).sort_order_value_is(2).take
  print "... skip\n"
else
  single = single_factory title_key: :overheat_night_2, device_type: :ep_single, date: '1987/10/25', sort_order: 2, number: '2nd', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4008.html'
  single.media_from_array [
    [ :ep, 'K-1566', '1st', false, @wp, '700' ],
    [ :kt, 'LKC-2043', '1st', false, @wp, '1,000' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/obahito-naito-single/id528974648', '1st', true, @wmj, '500' ]
  ]

  sl = song_list_factory device: single, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :overheat_night_2, @vcah ],
    [ :weekend_blue, @vc ]
  ]
  print "\n"
end
