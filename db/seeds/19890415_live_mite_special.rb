if Concert.concert_type_str_is(:live).sort_order_value_is(3).take
  print "... skip\n"
else
  band = band_factory(:animals_1)
  band.band_members_from_array %i( VOCAL iChisatoMoritaka SAXOPHONE iGinjiOgawa CHORUS iGinjiOgawa GUITAR iHiroyukiKato DRUMS iNaokiKimura CHORUS iNaokiKimura KEYBOARDS iKenichiMitsuda PERCUSSION iYouichiOkabe )

  concert = concert_factory j_title: '「見てスペシャル」', e_title: '"Mite Special"', yomi: 'みてすぺしゃるらいぶ', concert_type: :live, from: '1989/4/15', has_song_list: true, sort_order: 3, num_of_performances: 1, num_of_halls: 1, band: band

  sl = song_list_factory concert: concert, keyword: :A, sort_order: 1
  sl.song_list_songs_from_array %i( the_stress watashiga_hen alone yurusenai cant_say_good_bye kiss_the_night ringoshuno_rule seventeen the_loco_motion overheat_night let_me_go detagari weekend_blue wakareta_onna get_smile new_season ) + @encore + %i( mi_ha_ mite alone )

  concert.concert_halls_from_array [
    [ '1989/4/15', :shiodome_pit_ii, sl, nil, nil, '<ビデオ収録>|初のチケット即日ソールドアウト', '<Video Recording>|It was the first time that the concert ticket was sold out in a day.' ]
  ]
  print "\n"
end
