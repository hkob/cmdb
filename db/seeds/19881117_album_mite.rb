dt = Album::AlbumStr2Num[:album]
if Album.device_type_value_has(dt).sort_order_value_is(3).take
  print "... skip\n"
else
  album = album_factory title_key: :mite, device_type: :album, date: '1988/11/17', minutes: 42, seconds: 50, sort_order: 3, number: '3rd', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4036.html'
  album.media_from_array [
    [ :lp, '28L1-0027', :first, false, @wp, '2,800' ],
    [ :kt, '28L4-0027', :first, false, @wp, '2,800' ],
    [ :cd, '32L2-0027', :first, false, @wp, '3,200 3,008' ],
    [ :cd, 'WPCL-504', :second, true, @wmj, '2,400' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/jiante/id528970392', :first, true, @wmj, '2,100' ]
  ]

  sl = song_list_factory device: album, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :omoshiroi, %i( VOCAL iChisatoMoritaka GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito ) ],
    [ :wakareta_onna, %i( VOCAL iChisatoMoritaka GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito ) ],
    [ :watashiga_hen, %i( VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto GUITAR iTakumiYamamoto DRUMPR iTakumiYamamoto CHORUS iShinjiYasuda SYNPR iTomoakiArima SYNPR iNaokiSuzuki ) ],
    [ :alone, %i( VOCAL iChisatoMoritaka ARRANGE iShinjiYasuda CHORUS iShinjiYasuda SYNPR iNaokiSuzuki ) ],
    [ :stress, %i( VOCAL iChisatoMoritaka GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito ) ],
    [ :detagari, %i( VOCAL iChisatoMoritaka GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito ) ],
    [ :modorenai_natsu, %i( VOCAL iChisatoMoritaka ARRANGE iKenShima KEYBOARDS iKenShima BASS iKenjiTakamizu PERCUSSION iShingoKanno SYNPR iKeijiToriyama SYNPR iNaokiSuzuki ) ],
    [ :mite, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITAR iHideoSaito SYNPR iHideoSaito DRUMPR iHideoSaito ) ],
    [ :let_me_go, %i( VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto ) ]
  ]
  print "\n"
end
