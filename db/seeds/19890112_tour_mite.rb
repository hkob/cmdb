if Concert.concert_type_str_is(:tour).sort_order_value_is(2).take
  print "... skip\n"
else
  concert = concert_factory j_title: '「見て」ツアー', e_title: '"MITE" Tour', yomi: 'みてつあー', j_subtitle: "森高千里全国コンサートツアー'89", e_subtitle: "Chisato Moritaka a cross country tour '89", concert_type: :tour, from: '1989/1/12', to: '1989/3/27', num_of_performances: 10 , num_of_halls: 10 , has_song_list: true, sort_order: 2

  sl = song_list_factory concert: concert, keyword: :A, sort_order: 1
  sl.song_list_songs_from_array(
    %i( omoshiroi wakareta_onna cant_say_good_bye kiss_the_night yokohama_one_night otisreddingni_kanpai get_smile ) + @medley_in +
    [ [ nil, [], nil, nil, 'ボサノバ・スペシャル (メドレー)', 'Bossa nova special (Medley)' ] ] +
    %i( romantic pi_a_no modorenai_natsu do_you_know_the_way_to_san_jose ) +
    [ [ :anohino_photograph, [], '(ボサノバヴァージョン)', '(Bossa nova version)' ] ] +
    %i( kaigan shizukana_natsu ) + @medley_out +
    %i( the_stress detagari new_season weekend_blue forty_seven_har_nights let_me_go overheat_night ) +
    [ [ :the_mi_ha_, [], "<'89>", "<'89>" ] ] +
    @encore +
    %i( alone mite ))

  concert.concert_halls_from_array [
    [ '1989/1/12', :shibuya_koukado, sl ],
    [ '1989/3/1', :nagano_kenmin_bunka_kaikan, sl, '中ホール', 'Medium hall' ],
    [ '1989/3/3', :nagoya_kinrou_kaikan, sl ],
    [ '1989/3/4', :osaka_kousei_nenkin_kaikan, sl, '中ホール', 'Medium hall' ],
    [ '1989/3/10', :sapporo_doshin_hall, sl ],
    [ '1989/3/12', :sendai_shimin_kaikan, sl, '大ホール', 'Big hall' ],
    [ '1989/3/15', :niigata_kenmin_kaikan, sl, '大ホール', 'Big hall' ],
    [ '1989/3/24', :kumamoto_yubinchokin_hall, sl ],
    [ '1989/3/25', :nagasaki_heiwa_kaikan, sl ],
    [ '1989/3/27', :fukuoka_tsukushi_kaikan, sl ],
  ]
  print "\n"
end
