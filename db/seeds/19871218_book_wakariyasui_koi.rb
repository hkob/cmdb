if Book.book_type_str_is(:poems).sort_order_value_is(1).take
  print "... skip\n"
else
  book = book_factory book_type: :poems, title_key: :wakariyasui_koi, publisher_key: :kadokawa_shoten, author_key: :iNatsuoGiniro, photographer_key: :iNatsuoGiniro, isbn: '4-0416-7302-X', price: '596', date: '1987/12/18', sort_order: 1, link: 'http://www.moritaka-chisato.com/discogprahy/product/4475.html', j_comment: '写真のモデル', e_comment: 'Photographic model'
  print "\n"
end
