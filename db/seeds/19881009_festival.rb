if Concert.concert_type_str_is(:festival).sort_order_value_is(1).take
  print "... skip\n"
else
  concert = concert_factory j_title: '学園祭ツアー(1988年度)，その他', e_title: "University Festival Tour '88, etc.", yomi: 'がくえんさいつあーあ', concert_type: :festival, from: '1988/10/9', to: '1988/12/11', has_song_list: true, sort_order: 1, num_of_performances: 13, num_of_halls: 13

  sl = song_list_factory concert: concert, keyword: :A, sort_order: 1
  sl.song_list_songs_from_array( %i( get_smile overheat_night ) + @mc + %i( cant_say_good_bye kiss_the_night weekend_blue ) + @mc + %i( stress mite ) + @mc + %i( good_bye_season yokohama_one_night ) + [ [ nil, [], nil, nil, '川崎では1コーラス目を"KAWASAKI ONE NIGHT"と唄う', 'She sang "KAWASAKI ONE NIGHT" instead of "YOKOHAMA ONE NIGHT" at the first chorus.' ] ] + %i( forty_seven_har_nights let_me_go mi_ha_ ) + @encore + %i( new_season ) + @mc + %i( alone ))

  concert.concert_halls_from_array [
    [ '1988/10/9', :toyohashi_university_of_technology ],
    [ '1988/10/13', :ariake_mza_convention ],
    [ '1988/10/28', :niigata_university_of_phermacy ],
    [ '1988/10/29', :kanazawa_university ],
    [ '1988/10/30', :takaoka_national_college ],
    [ '1988/10/31', :ehime_bunka_koudou, nil, '(公録)', '(Public recording)' ],
    [ '1988/11/1', :shiga_university ],
    [ '1988/11/4', :StMargaretsSchool ],
    [ '1988/11/5', :shinshu_university ],
    [ '1988/11/6', :niigata_sangyo_university ],
    [ '1988/11/12', :tokyo_metropolitan_institute_of_technology, sl ],
    [ '1988/11/13', :toyo_university ],
    [ '1988/11/15', :club_citta_kawasaki, sl ],
    [ '1988/11/19', :teikyo_technical_college ],
    [ '1988/11/21', :kyushu_institute_technology ],
    [ '1988/12/10', :yatsushiro_kousei_nenkin_kaikan ],
    [ '1988/12/11', :nagasaki_nbc_hall, nil, '(公録)', '(Public recording)' ]
  ]
  print "\n"
end
