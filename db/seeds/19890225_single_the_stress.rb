dt = Single::SingleStr2Num[:ep_single]
if Single.device_type_value_has(dt).sort_order_value_is(6).take
  print "... skip\n"
else
  single = single_factory title_key: :the_stress_stress_chuukintou_version, device_type: :ep_single, add_device_type: %i( single ), date: '1989/2/25', sort_order: 6, number: '6th', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4029.html'
  single.media_from_array [
    [ :ep, '07L7-4037', '1st', false, @wp, '700' ],
    [ :kt, '10L5-4037', '1st', false, @wp, '1,000' ],
    [ :cds, '10L3-4037', '1st', false, @wp, '937' ],
    [ :cds, '10L3-4037', '2nd', false, @wmj, '937' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/za-sutoresu-sutoresu-zhong/id528975527', '1st', true, @wmj, '500' ]
  ]

  sl = song_list_factory device: single, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :the_stress, @vcah, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]' ],
    [ :yurusenai, @vcah ]
  ]
  print "\n"
end
