if Activity.activity_type_str_is(:cm).sort_order_value_is(2).take
  print "... skip\n"
else
  activity = activity_factory activity_type: :cm, j_title: 'パイオニア 「留守番テレフォン TF-A5」', e_title: "PIONEER 'telephone system with answering service TF-A5'", yomi: 'ぱいおにあ　るすばんてれふぉん　てぃーえふえーふぁいぶ', company_key: :pioneer, from: '1988/2/23', to: '1988/11/30', to_no_day: true, song_key: :mi_ha_, sort_order: 2
  print "\n"
end
