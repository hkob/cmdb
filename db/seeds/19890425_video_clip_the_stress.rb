dt = Video::VideoStr2Num[:video_clip]
if Video.device_type_value_has(dt).sort_order_value_is(2).take
  print "... skip\n"
else
  video = video_factory title_key: :the_stress, device_type: :video_clip, date: '1989/4/25', minutes: 10, seconds: 20, sort_order: 2, number: '2nd', singer_key: :iChisatoMoritaka, link: 'http://www.moritaka-chisato.com/discogprahy/product/4317.html'
  video.media_from_array [
    [ :vhs, '15L8-8024', :first, false, @wp, '1,545' ],
    [ :ld, '24L6-8025', :first, false, @wp, '2,472' ],
    [ :vhs, '15L8-8024', :second, false, @wmj, '1,500' ],
    [ :ld, '24L6-8025', :second, false, @wmj, '2,472' ],
    [ :itunes, 'https://itunes.apple.com/jp/music-video/za-sutoresu-sutoresu-zhong/id534978131', :first, true, @wmj, 400 ],
  ]

  slv = song_list_factory device: video, keyword: :vhs, sort_order: 1
  sll = song_list_factory device: video, keyword: :ld, sort_order: 2
  slv.song_list_songs_from_array [
    [ :the_stress, @vc, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]' ],
    [ :modorenai_natsu, @vc, "[メイキング・オブ・「ザ・ストレス」 BGM]", "[Making of `the stress' BGM]" ],
  ]
  sll.song_list_songs_from_array [
    [ :overheat_night, @vc ],
    [ :get_smile, @vc ],
    [ :the_stress, @vc, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]' ],
    [ :modorenai_natsu, @vc, "[メイキング・オブ・「ザ・ストレス」 BGM]", "[Making of `the stress' BGM]" ],
  ]
  print "\n"
end
