dt = Single::SingleStr2Num[:ep_single]
if Single.device_type_value_has(dt).sort_order_value_is(4).take
  print "... skip\n"
else
  single = single_factory title_key: :the_mi_ha_mi_ha_, device_type: :ep_single, add_device_type: %i( single ), date: '1988/4/25', sort_order: 4, number: '4th', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4022.html'
  single.media_from_array [
    [ :ep, 'K-1568', '1st', false, @wp, '700' ],
    [ :kt, 'LKC-2066', '1st', false, @wp, '700' ],
    [ :cds, '10SL-16', '1st', false, @wp, '937' ],
    [ :cds, '10SL-16', '2nd', false, @wmj, '937' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/za-miha-supesharu-miha-mikkusu/id528980756', '1st', true, @wmj, '500' ]
  ]

  sl = song_list_factory device: single, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :the_mi_ha_, %i( VOCAL iChisatoMoritaka REMIX iHideoSaito GUITAR iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito SYNPR iNobitaTsukada KEYBOARDS iNobitaTsukada BGVOCAL iMisaNakayama TIMSOLO iChisatoMoritaka ), '[スペシャル・ミーハー・ミックス]', '[Special {MI-HA-} mix]' ],
    [ :mi_ha_, @vc, '[オリジナル・ヴァージョン]', '[Original version]' ]
  ]
  print "\n"
end
