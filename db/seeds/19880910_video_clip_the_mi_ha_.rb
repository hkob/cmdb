dt = Video::VideoStr2Num[:video_clip]
if Video.device_type_value_has(dt).sort_order_value_is(1).take
  print "... skip\n"
else
  video = video_factory title_key: :the_mi_ha_special_mi_ha_mix, device_type: :video_clip, date: '1988/9/10', minutes: 7, sort_order: 1, number: '1st', singer_key: :iChisatoMoritaka, link: 'http://www.moritaka-chisato.com/discogprahy/product/4311.html'
  video.media_from_array [
    [ :vhs, '12L9-8002', :first, false, @wp, '1,236' ],
    [ :beta, '12L8-8002', :first, false, @wp, '1,236' ],
    [ :vhs, '12L9-8002', :second, false, @wmj, '1,236 1,200' ],
    [ :itunes, 'https://itunes.apple.com/jp/music-video/za-miha-supesharu-miha-mikkusu/id534978390', :first, true, @wmj, 400 ],
  ]

  sl = song_list_factory device: video, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :the_mi_ha_, %i( VOCAL iChisatoMoritaka EDRUMS iChisatoMoritaka ), '(スペシャル・ミーハー・ミックス)', '[Special {MI-HA-} mix]' ]
  ]
  print "\n"
end
