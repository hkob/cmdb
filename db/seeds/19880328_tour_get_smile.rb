if Concert.concert_type_str_is(:tour).sort_order_value_is(1).take
  print "... skip\n"
else
  concert = concert_factory j_title: '「GET SMILE」ツアー', e_title: '"GET SMILE" Tour', yomi: 'げっとすまいるつあー', j_subtitle: "森高千里 LIVE TOUR '88", e_subtitle: "Chisato Moritaka LIVE TOUR '88", concert_type: :tour, from: '1988/3/28', to: '1988/4/14', num_of_performances: 10, num_of_halls: 8, has_song_list: true, sort_order: 1

  sl = song_list_factory concert: concert, keyword: :A, sort_order: 1
  sl.song_list_songs_from_array(
    %i( weekend_blue yokohama_one_night yumeno_owari anohino_photograph pi_a_no otisreddingni_kanpai good_bye_season get_smile mi_ha_ cant_say_good_bye overheat_night forty_seven_har_nights ) + @encore + %i( new_season get_smile ))

  concert.concert_halls_from_array [
    [ '1988/3/28', :shibuya_live_in ],
    [ '1988/3/29', :shibuya_live_in ],
    [ '1988/3/30', :shibuya_live_in, sl ],
    [ '1988/4/5', :sendai_moning_moon, sl ],
    [ '1988/4/6', :niigata_kenmin_kaikan, sl, '小ホール', 'Small hall', 'ライブ後腹痛に見舞われ救急病院へ', 'She was rushed to an emergency hospital because she had a bad stomachache after the live.' ],
    [ '1988/4/8', :osaka_kousei_nenkin_kaikan, sl, '中ホール', 'Medium hall' ],
    [ '1988/4/9', :nagoya_heart_land, sl ],
    [ '1988/4/11', :sapporo_messe_hall, sl ],
    [ '1988/4/13', :kumamoto_yubinchokin_hall, sl ],
    [ '1988/4/14', :fukuoka_vivre, sl ]
  ]
  print "\n"
end
