dt = Single::SingleStr2Num[:ep_single]
if Single.device_type_value_has(dt).sort_order_value_is(1).take
  print "... skip\n"
else
  single = single_factory title_key: :new_season, device_type: :ep_single, date: '1987/5/25', sort_order: 1, number: '1st', singer: @cm, j_comment: 'デビューシングル', e_comment: 'Debut Single', link: 'http://www.moritaka-chisato.com/discogprahy/product/4005.html'

  single.media_from_array [
    [ :ep, 'K-1564', '1st', false, @wp, '700' ],
    [ :kt, 'LKC-2025', '1st', false, @wp, '1,000' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/new-season-single/id528968883', '1st', true, @wmj, '500' ]
  ]

  sl = song_list_factory device: single, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :new_season, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito KEYBOARD_SOLO iChisatoMoritaka ) ],
    [ :period, @vcah ]
  ]
  print "\n"
end
