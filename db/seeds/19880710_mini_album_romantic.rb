dt = Album::AlbumStr2Num[:mini_album]
if Album.device_type_value_has(dt).sort_order_value_is(1).take
  print "... skip\n"
else
  album = album_factory title_key: :romantic, device_type: :mini_album, date: '1988/7/10', minutes: 20, seconds: 31, sort_order: 1, number: '1st', singer: @cm, j_comment: 'サマー特別企画ミニアルバム', e_comment: 'A Mini-Album as a summer special project', link: 'http://www.moritaka-chisato.com/discogprahy/product/4034.html'
  album.media_from_array [
    [ :cds, '18SL-1001', :first, false, @wp, '1,800' ],
    [ :kt, 'LKE-3001', :first, false, @wp, '1,800' ],
    [ :cd, '18SL-1001', :first, false, @wmj, '1,689' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/romantic-romantikku-ep/id528977583', :first, true, @wmj, '1,250' ]
  ]

  sl = song_list_factory device: album, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :do_you_know_the_way_to_san_jose, %i( VOCAL iChisatoMoritaka VOCAL iTakumiYamamoto BASS iKeiichiIshibashi DRUMS iKeiichiIshibashi CHORUS iChisatoMoritaka CHORUS iTakumiYamamoto CHORUS iKenShima PIANO iKenShima ) ],
    [ :romantic, %i( VOCAL iChisatoMoritaka KEYBOARDS iKenShima BASS iKenjiTakamizu DRUMS iMasahiroMiyazaki SYNPR iKeijiToriyama ) ],
    [ :anohino_photograph, %i( VOCAL iChisatoMoritaka VOCAL iKenShima GUITAR iYukioSeto KEYBOARDS iKenShima BASS iKenjiTakamizu DRUMS iMasahiroMiyazaki SYNPR iKeijiToriyama PIANO iKenShima ), '(ボサノバヴァージョン)', '(Bossa Nova version)' ],
    [ :kaigan, %i( VOCAL iChisatoMoritaka SYNPR iTomoakiArima CHORUS iNana ) ],
    [ :shizukana_natsu, %i( VOCAL iChisatoMoritaka SYNPR iTomoakiArima ) ]
  ]
  print "\n"
end
