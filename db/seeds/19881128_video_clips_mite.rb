dt = Video::VideoStr2Num[:video_clips]
if Video.device_type_value_has(dt).sort_order_value_is(1).take
  print "... skip\n"
else
  video = video_factory title_key: :mite, device_type: :video_clips, date: '1988/11/28', minutes: 27, seconds: 15, sort_order: 1, number: '1st', singer_key: :iChisatoMoritaka, link: 'http://www.moritaka-chisato.com/discogprahy/product/4315.html', j_comment: 'VHS発売: 1988年12月10日', e_comment: 'VHS release: Dec. 10, 1988'
  video.media_from_array [
    [ :vhs, '35L8-8020', :first, false, @wp, '3,500' ],
    [ :ld, '25L6-8017', :first, false, @wp, '2,431' ],
    [ :vhs, '35L8-8020', :second, false, @wmj, '3,500' ],
    [ :ld, '25L6-8017', :second, false, @wmj, '2,431' ],
  ]

  slv = song_list_factory device: video, keyword: :vhs, sort_order: 1
  sll = song_list_factory device: video, keyword: :ld, sort_order: 2
  slv.song_list_songs_from_array(%i( new_season overheat_night_2 get_smile the_mi_ha_ alone ).map { |key| [ key, @vc ] })
  sll.song_list_songs_from_array(%i( new_season the_mi_ha_ alone ).map { |key| [ key, @vc ] })
  print "\n"
end
