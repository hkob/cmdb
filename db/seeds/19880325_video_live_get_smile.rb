dt = Video::VideoStr2Num[:live_video]
if Video.device_type_value_has(dt).sort_order_value_is(1).take
  print "... skip\n"
else
  video = video_factory title_key: :live_get_smile, device_type: :live_video, date: '1988/3/25', minutes: 48, sort_order: 1, number: '1st', singer_key: :iChisatoMoritaka, link: 'http://www.moritaka-chisato.com/discogprahy/product/3998.html'
  video.media_from_array [
    [ :vhs, '08PV-79', :first, false, @wp, '7,800' ],
    [ :beta, '08PX-79', :first, false, @wp, '7,800' ],
    [ :ld, '08PL-40', :first, false, @wp, '6,800 6,420' ],
    [ :vhs, 'WPVL-8103', :second, false, @wmj, '4,077' ],
    [ :ld, 'WPLL-8103', :second, false, @wmj, '4,000' ],
  ]

  sl = song_list_factory device: video, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array(%i( good_bye_season namida_good_bye anohino_photograph ringoshuno_rule yumeno_owari weekend_blue get_smile overheat_night_2 new_season ).map { |key| [ key, @vc ] })

  concert = concert_factory concert_type: :live, sort_order: 2
  concert_hall = concert_hall_factory concert: concert, sort_order: 2
  concert_song_list = concert.song_lists.take
  concert_song_list_songs = concert_song_list.song_list_songs.order_sort_order
  sl.song_list_songs.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_song_list_songs[i], vsls, concert_hall
  end
  print "\n"
end
