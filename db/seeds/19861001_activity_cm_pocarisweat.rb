if Activity.activity_type_str_is(:cm).sort_order_value_is(1).take
  print "... skip\n"
else
  activity = activity_factory activity_type: :cm, j_title: '大塚製薬 「ポカリスエット」', e_title: "Otsuka Pharmaceutical Company `Pocarisweat'", yomi: 'おおつかせいやく　ぽかりすえっと', company_key: :otsuka_seiyaku, from: '1986/10/1', to: '1988/9/30', j_comment: '和服編|寝室編|電車編|自動販売機編|骨董店編|レストラン編|温泉編', e_comment: 'Japanese custume version|Bed room version|Train version|{JIDOHANBAIKI} version|{KOTTOHIN-TEN} version|Restaurant version|{ONSEN} version', sort_order: 1
  print "\n"
end
