dt = Album::AlbumStr2Num[:album]
if Album.device_type_value_has(dt).sort_order_value_is(2).take
  print "... skip\n"
else
  album = album_factory title_key: :mi_ha_, device_type: :album, date: '1988/3/25', minutes: 48, seconds: 32, sort_order: 2, number: '2nd', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4032.html'
  album.media_from_array [
    [ :lp, 'K-12540', :first, false, @wp, '2,800' ],
    [ :kt, 'LKF-8801', :first, false, @wp, '2,800' ],
    [ :cd, '32XL-271', :first, false, @wp, '3,200 3,008' ],
    [ :cd, 'WPCL-503', :second, true, @wmj, '2,400' ],
    [ :itunes, 'https://itunes.apple.com/jp/album/miha/id528967994', :first, true, @wmj, '2,400' ]
  ]

  sl = song_list_factory device: album, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :overheat_night_2, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada HIHAT iRyubenTsujino CRASH iRyubenTsujino CONGAS iShingoKanno CHORUS iHideoSaito BGVOCAL iMisaNakayama ) ],
    [ :yokohama_one_night, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada GUIRO iShingoKanno TENORSAX iJakeHConception BGVOCAL iMisaNakayama ) ],
    [ :good_bye_season, %i( VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto GUITARS iJunroSato CHORUS iNana BGVOCAL iYukariFujio TENORSAX iJakeHConception SYNPR iTomoakiArima ) ],
    [ :cant_say_good_bye, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada ) ],
    [ :pi_a_no, %i( VOCAL iChisatoMoritaka VOCAL iKenShima ARRANGE iKenShima FRSOLO iChisatoMoritaka APIANO iKenShima OTHERKB iKenShima SYNPR iTakayukiNegishi ) ],
    [ :forty_seven_har_nights, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada ) ],
    [ :weekend_blue, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada CRASH iRyubenTsujino TROMBONE iShingoKanno ) ],
    [ :kiss_the_night, %i( VOCAL iChisatoMoritaka ARRANGE iTakumiYamamoto GUITARS iJunroSato CHORUS iNana CHORUS iTakumiYamamoto BGVOCAL iYukariFujio BASS iChiharuMikuzuki EPIANO iHatsuhoFurukawa SYNPR iTomoakiArima ) ],
    [ :mi_ha_, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito TIMSOLO iChisatoMoritaka GUITARS iHideoSaito CHORUS iHideoSaito DRUMPR iHideoSaito SYNPR iHideoSaito KEYBOARDS iNobitaTsukada SYNPR iNobitaTsukada BGVOCAL iMisaNakayama ) ],
    [ :get_smile, %i( VOCAL iChisatoMoritaka ARRANGE iKenShima CARRANGE iHideoSaito GARRANGE iHideoSaito KEYBOARDS iKenShima GUITAR iHideoSaito CHORUS iHideoSaito BASS iChiharuMikuzuki TENORSAX iJakeHConception BGVOCAL iYukariFujio SYNPR iTakayukiNegishi ) ]
  ]
  print "\n"
end
