songs = [
  %i( new_season iHiro iHideoSaito ),
  %i( period iHiro iHideoSaito ),
  %i( namida_good_bye iShingoKanno iTakumiYamamoto ),
  %i( yumeno_owari iShingoKanno iHideoSaito ),
  %i( ringoshuno_rule iKoiTakayanagi iKenShima ),
  %i( otisreddingni_kanpai iKoiTakayanagi iHiromichiTsugaki ),
  %i( ways iHiromiFukunaga iHiromichiTsugaki ),
  %i( anohino_photograph iHiromasaIjichi iKenShima ),
  %i( miss_lady iHiromasaIjichi iHiromasaIjichi ),
  %i( weekend_blue iHiromasaIjichi iHideoSaito ),
  %i( overheat_night iHiromasaIjichi iHideoSaito ),
  %i( overheat_night_2 iHiromasaIjichi iHideoSaito ),
  %i( good_bye_season iKanonKuwa iTakumiYamamoto ),
  %i( get_smile iHiromasaIjichi iKenShima ),
  %i( yokohama_one_night iHiromasaIjichi iHideoSaito ),
  %i( cant_say_good_bye iHiromasaIjichi iHideoSaito ),
  %i( pi_a_no iKanonKuwa iKenShima ),
  %i( forty_seven_har_nights iHiromasaIjichi iHideoSaito ),
  %i( kiss_the_night iHiromasaIjichi iTakumiYamamoto ),
  %i( mi_ha_ iChisatoMoritaka iHideoSaito ),
  %i( the_mi_ha_ iChisatoMoritaka iHideoSaito ),
  [ :do_you_know_the_way_to_san_jose, %i( iHDavid iBBacharach ), %i( iHDavid iBBacharach ) ],
  %i( romantic iChisatoMoritaka iKenShima ),
  %i( kaigan iMakotoOishi iTakumiYamamoto ),
  %i( shizukana_natsu iKanonKuwa iTakumiYamamoto ),
  %i( stress iChisatoMoritaka iHideoSaito ),
  %i( mite iChisatoMoritaka iHideoSaito ),
  %i( let_me_go iChisatoMoritaka iShinjiYasuda ),
  %i( alone iChisatoMoritaka iShinjiYasuda ),
  %i( chase iKanonKuwa iTakumiYamamoto ),
  %i( omoshiroi iChisatoMoritaka iHideoSaito ),
  %i( wakareta_onna iChisatoMoritaka iHideoSaito ),
  %i( watashiga_hen iEkisuSuzuki iTakumiYamamoto ),
  %i( detagari iChisatoMoritaka iHideoSaito ),
  %i( modorenai_natsu iKanonKuwa iKenShima ),
  %i( the_stress iChisatoMoritaka iHideoSaito ),
  %i( yurusenai iChisatoMoritaka iHideoSaito ),
  %i( seventeen iMiekoArima iKyoheiTsutsumi ),
  %i( the_loco_motion iGerryGoffin iCaroleKing ),
  %i( hatachi iChisatoMoritaka iHideoSaito ),
  %i( korekkiri_byebye iChisatoMoritaka iHideoSaito ),
  %i( daite iChisatoMoritaka iYuichiTakahashi ),
  %i( hijitsuryokuha_sengen iChisatoMoritaka iHideoSaito ),
  %i( kondo_watashi_dokoka iChisatoMoritaka iHideoSaito ),
  %i( hadakaniha_naranai iChisatoMoritaka iMasataroNaoe ),
  %i( watashiha_onchi iChisatoMoritaka iYuichiTakahashi ),
  %i( shiritagari iChisatoMoritaka iChisatoMoritaka ),
  %i( wakasugita_koi iChisatoMoritaka iYuichiTakahashi ),
  %i( akunno_higeki iChisatoMoritaka iHideoSaito ),
  %i( yoruno_entotsu iMasataroNaoe iMasataroNaoe ),
  %i( sonogono_watashi iChisatoMoritaka iHideoSaito ),
  %i( yumeno_nakano_kiss iChisatoMoritaka iYuichiTakahashi ),
]
if Song.count == songs.count
  print "... skip\n"
else
  songs.each do |line|
    sk, lk, mk = line
    lk = [ lk ] if lk.instance_of? Symbol
    mk = [ mk ] if mk.instance_of? Symbol
    song_factory_with_lyrics_musics song_key: sk, lyric_keys: lk, music_keys: mk
  end
  print "\n"
end
