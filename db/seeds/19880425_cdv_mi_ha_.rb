dt = Video::VideoStr2Num[:cdv]
if Video.device_type_value_has(dt).sort_order_value_is(1).take
  print "... skip\n"
else
  video = video_factory title_key: :mi_ha_, device_type: :cdv, date: '1988/4/25', minutes: 23, seconds: 59, sort_order: 1, number: '1st', singer_key: :iChisatoMoritaka, link: 'http://www.moritaka-chisato.com/discogprahy/product/4048.html'
  video.media_from_array [
    [ :cdv, '24VL-13', :first, false, @wp, '2,400 2,328' ],
    [ :itunes, 'https://itunes.apple.com/jp/music-video/get-smile/id534975913', :first, true, @wmj, '400' ],
  ]

  sl = song_list_factory device: video, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :yokohama_one_night, @vc ],
    [ :good_bye_season, @vc ],
    [ :forty_seven_har_nights, @vc ],
    [ :mi_ha_, @vc ],
    [ :get_smile, @vc, '(VIDEO Part)', '(VIDEO Part)' ]
  ]
  print "\n"
end
