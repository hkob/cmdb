dt = Video::VideoStr2Num[:live_video]
if Video.device_type_value_has(dt).sort_order_value_is(2).take
  print "... skip\n"
else
  video = video_factory title_key: :mite_special_live, device_type: :live_video, date: '1989/6/25', minutes: 55, sort_order: 2, number: '2nd', singer_key: :iChisatoMoritaka, link: 'http://www.moritaka-chisato.com/discogprahy/product/4321.html'
  video.media_from_array [
    [ :vhs, '55L8-8029', :first, false, @wp, '5,665' ],
    [ :ld, '55L6-8029', :first, false, @wp, '5,665' ],
    [ :vhs, 'WPVL-8150', :second, false, @wmj, '4,000' ],
    [ :ld, 'WPLL-8150', :second, false, @wmj, '4,000' ],
  ]


  sl = song_list_factory device: video, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array([
    [ :the_stress, @vc, '[ストレス中近東ヴァージョン]', '[Stress {CHUUKINTOU} version]' ],
    [ :watashiga_hen, @vc ],
    [ :yurusenai, @vc ],
    [ :seventeen, @vc ],
    [ :the_loco_motion, @vc ],
    [ :overheat_night_2, @vc ],
    [ :let_me_go, @vc ],
    [ :wakareta_onna, @vc ],
    [ :new_season, @vc ],
    [ :mi_ha_, @vc ],
    [ :mite, @vc ],
    [ :alone, @vc ],
  ])

  concert = concert_factory concert_type: :live, sort_order: 3
  concert_hall = concert_hall_factory concert: concert, sort_order: 1
  concert_song_list = concert.song_lists.take
  concert_song_list_songs = concert_song_list.song_list_songs.order_sort_order
  csos = { 0 => 0, 1 => 1, 2 => 3, 3 => 7, 4 => 8, 5 => 9, 6 => 10, 7 => 13, 8 => 15, 9 => 17, 10 => 18, 11 => 19 }

  sl.song_list_songs.order_sort_order.each_with_index do |vsls, i|
    concert_video_factory concert_song_list_songs[csos[i]], vsls, concert_hall
  end
  print "\n"
end
