dt = Single::SingleStr2Num[:single]
if Single.device_type_value_has(dt).sort_order_value_is(2).take
  print "... skip\n"
else
  single = single_factory title_key: :new_season_overheat_night, device_type: :single, date: '1988/3/25', sort_order: 2, number: 'Re-released', singer: @cm, link: 'http://www.moritaka-chisato.com/discogprahy/product/4020.html'
  single.media_from_array [
    [ :cds, '10SL-110', '1st', false, @wp, '937' ],
    [ :cds, '10SL-110', '2nd', false, @wmj, '937' ],
  ]

  sl = song_list_factory device: single, keyword: :common, sort_order: 1
  sl.song_list_songs_from_array [
    [ :new_season, %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito KEYBOARD_SOLO iChisatoMoritaka ) ],
    [ :overheat_night_2, @vcah ]
  ]
  print "\n"
end
