# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

base_path = File.join(Rails.root, 'db', 'seeds', 'locales')
dst_path = File.join(Rails.root, 'config', 'locales')
Dir.glob(File.join(base_path, '**', '*.yml')) do |file|
  dir = File.dirname(file.gsub(base_path, ''))
  ja_path = File.join(dst_path, dir, 'ja.yml')
  en_path = File.join(dst_path, dir, 'en.yml')
  mt = File.mtime(file)
  if mt > File.mtime(ja_path) || mt > File.mtime(en_path)
    File.open(file) do |f|
      File.open(ja_path, 'w') do |fja|
        File.open(en_path, 'w') do |fen|
          puts file
          fja.puts 'ja:'
          fen.puts 'en:'
          while line = f.gets
            if /(.*):(.*)/ =~ line
              key = $1
              body = $2
              ja, en = body.split('|')
              if ja.present?
                fja.puts "#{ key }:#{ ja }"
                fen.puts "#{ key }: #{ en }"
              else
                fja.puts "#{ key }:"
                fen.puts "#{ key }:"
              end
            end
          end
        end
      end
    end
  end
end

require 'factory_girl'
Dir[Rails.root.join('spec/support/factory_girl.rb')].each { |f| require f }
Dir[Rails.root.join('spec/support/create_factories/*.rb')].each {|f| require f }

@vocal = instrumental_factory :VOCAL
@arrange = instrumental_factory :ARRANGE
@cm = person_factory :iChisatoMoritaka
@hs = person_factory :iHideoSaito
@wp = company_factory :warner_pioneer
@wmj = company_factory :warner_music_japan
@vc = %i( VOCAL iChisatoMoritaka )
@vcah = %i( VOCAL iChisatoMoritaka ARRANGE iHideoSaito )
@mc = [ [ nil, [], nil, nil, 'MC', 'MC' ] ]
@encore = [ [ nil, [], nil, nil, 'アンコール', 'ENCORE' ] ]
@double_encore = [ [ nil, [], nil, nil, 'ダブルアンコール', 'DOUBLE ENCORE' ] ]
@medley_in = [ [ nil, [], nil, nil, 'MEDLEY_IN', 'MEDLEY_IN' ] ]
@medley_out = [ [ nil, [], nil, nil, 'MEDLEY_OUT', 'MEDLEY_OUT' ] ]
Dir.glob(File.join(Rails.root, 'db', 'seeds', '*.rb')) do |file|
  print File.basename(file)
  load(file)
end
