# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151203141905) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.integer  "activity_type", null: false
    t.integer  "title_id",      null: false
    t.integer  "company_id"
    t.integer  "song_id"
    t.string   "j_comment"
    t.string   "e_comment"
    t.integer  "sort_order",    null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "from_id"
    t.integer  "to_id"
  end

  add_index "activities", ["activity_type", "sort_order"], name: "index_activities_on_activity_type_and_sort_order", unique: true, using: :btree
  add_index "activities", ["from_id"], name: "index_activities_on_from_id", using: :btree

  create_table "band_members", force: :cascade do |t|
    t.integer  "band_id",         null: false
    t.integer  "person_id",       null: false
    t.integer  "instrumental_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "band_members", ["band_id", "instrumental_id"], name: "band_members_bi", using: :btree
  add_index "band_members", ["band_id", "person_id", "instrumental_id"], name: "band_members_bpi", unique: true, using: :btree
  add_index "band_members", ["instrumental_id"], name: "band_members_i", using: :btree
  add_index "band_members", ["person_id", "instrumental_id"], name: "band_members_pi", using: :btree

  create_table "bands", force: :cascade do |t|
    t.integer  "title_id",   null: false
    t.string   "j_comment"
    t.string   "e_comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bands", ["title_id"], name: "index_bands_on_title_id", unique: true, using: :btree

  create_table "books", force: :cascade do |t|
    t.integer  "book_type",       null: false
    t.integer  "title_id",        null: false
    t.integer  "publisher_id"
    t.integer  "seller_id"
    t.integer  "photographer_id"
    t.string   "isbn"
    t.string   "price"
    t.integer  "sort_order",      null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "event_date_id"
    t.integer  "author_id"
    t.string   "j_comment"
    t.string   "e_comment"
    t.string   "link"
  end

  add_index "books", ["author_id"], name: "index_books_on_author_id", using: :btree
  add_index "books", ["book_type", "sort_order"], name: "index_books_on_book_type_and_sort_order", unique: true, using: :btree
  add_index "books", ["event_date_id"], name: "index_books_on_event_date_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.integer  "title_id",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "companies", ["title_id"], name: "index_companies_on_title_id", unique: true, using: :btree

  create_table "concert_halls", force: :cascade do |t|
    t.integer  "concert_id",    null: false
    t.integer  "sort_order",    null: false
    t.integer  "hall_id",       null: false
    t.string   "j_hall_sub"
    t.string   "e_hall_sub"
    t.string   "j_comment"
    t.string   "e_comment"
    t.string   "j_product"
    t.string   "e_product"
    t.integer  "song_list_id"
    t.integer  "device_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "event_date_id"
  end

  add_index "concert_halls", ["concert_id", "sort_order", "hall_id"], name: "concert_halls_csh", unique: true, using: :btree
  add_index "concert_halls", ["event_date_id"], name: "index_concert_halls_on_event_date_id", using: :btree
  add_index "concert_halls", ["song_list_id"], name: "index_concert_halls_on_song_list_id", using: :btree

  create_table "concert_videos", force: :cascade do |t|
    t.integer  "concert_song_list_song_id", null: false
    t.integer  "video_song_list_song_id",   null: false
    t.integer  "concert_hall_id",           null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "concert_videos", ["concert_hall_id"], name: "index_concert_videos_on_concert_hall_id", using: :btree
  add_index "concert_videos", ["concert_song_list_song_id"], name: "index_concert_videos_on_concert_song_list_song_id", using: :btree
  add_index "concert_videos", ["video_song_list_song_id"], name: "index_concert_videos_on_video_song_list_song_id", using: :btree

  create_table "concerts", force: :cascade do |t|
    t.integer  "title_id",            null: false
    t.string   "j_subtitle"
    t.string   "e_subtitle"
    t.string   "j_comment"
    t.string   "e_comment"
    t.integer  "concert_type",        null: false
    t.boolean  "has_song_list",       null: false
    t.boolean  "has_product",         null: false
    t.integer  "band_id"
    t.integer  "num_of_performances", null: false
    t.integer  "num_of_halls",        null: false
    t.integer  "sort_order",          null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "from_id"
    t.integer  "to_id"
  end

  add_index "concerts", ["concert_type", "sort_order"], name: "index_concerts_on_concert_type_and_sort_order", unique: true, using: :btree
  add_index "concerts", ["from_id"], name: "index_concerts_on_from_id", using: :btree
  add_index "concerts", ["title_id"], name: "index_concerts_on_title_id", unique: true, using: :btree

  create_table "devices", force: :cascade do |t|
    t.string   "type",          null: false
    t.integer  "device_type",   null: false
    t.integer  "title_id",      null: false
    t.integer  "minutes"
    t.integer  "seconds"
    t.integer  "sort_order",    null: false
    t.string   "number"
    t.integer  "singer_id"
    t.string   "j_comment"
    t.string   "e_comment"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "event_date_id"
    t.string   "link"
  end

  add_index "devices", ["event_date_id"], name: "index_devices_on_event_date_id", using: :btree
  add_index "devices", ["singer_id"], name: "index_devices_on_singer_id", using: :btree
  add_index "devices", ["title_id"], name: "index_devices_on_title_id", using: :btree
  add_index "devices", ["type", "device_type", "event_date_id"], name: "index_devices_on_type_and_device_type_and_event_date_id", using: :btree

  create_table "event_dates", force: :cascade do |t|
    t.date     "date",       null: false
    t.integer  "year_id",    null: false
    t.integer  "month"
    t.integer  "day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "event_dates", ["date"], name: "index_event_dates_on_date", using: :btree
  add_index "event_dates", ["year_id", "date", "month", "day"], name: "index_event_dates_on_year_id_and_date_and_month_and_day", using: :btree

  create_table "halls", force: :cascade do |t|
    t.integer  "title_id",      null: false
    t.integer  "sort_order",    null: false
    t.integer  "prefecture_id", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "halls", ["prefecture_id", "sort_order"], name: "index_halls_on_prefecture_id_and_sort_order", unique: true, using: :btree
  add_index "halls", ["title_id"], name: "index_halls_on_title_id", unique: true, using: :btree

  create_table "instrumentals", force: :cascade do |t|
    t.integer  "sort_order", null: false
    t.integer  "title_id",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "instrumentals", ["sort_order"], name: "index_instrumentals_on_sort_order", unique: true, using: :btree
  add_index "instrumentals", ["title_id"], name: "index_instrumentals_on_title_id", unique: true, using: :btree

  create_table "lyrics", force: :cascade do |t|
    t.integer  "song_id",    null: false
    t.integer  "person_id",  null: false
    t.integer  "sort_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "lyrics", ["person_id"], name: "index_lyrics_on_person_id", using: :btree
  add_index "lyrics", ["song_id", "person_id", "sort_order"], name: "index_lyrics_on_song_id_and_person_id_and_sort_order", using: :btree
  add_index "lyrics", ["song_id", "sort_order"], name: "index_lyrics_on_song_id_and_sort_order", using: :btree

  create_table "media", force: :cascade do |t|
    t.string   "medium_device", null: false
    t.string   "code"
    t.string   "release"
    t.boolean  "now_sale"
    t.integer  "company_id"
    t.string   "price"
    t.integer  "sort_order",    null: false
    t.integer  "device_id",     null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "media", ["company_id", "code"], name: "index_media_on_company_id_and_code", using: :btree
  add_index "media", ["device_id", "sort_order"], name: "index_media_on_device_id_and_sort_order", using: :btree

  create_table "musics", force: :cascade do |t|
    t.integer  "song_id",    null: false
    t.integer  "person_id",  null: false
    t.integer  "sort_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "musics", ["person_id"], name: "index_musics_on_person_id", using: :btree
  add_index "musics", ["song_id", "person_id", "sort_order"], name: "index_musics_on_song_id_and_person_id_and_sort_order", using: :btree
  add_index "musics", ["song_id", "sort_order"], name: "index_musics_on_song_id_and_sort_order", using: :btree

  create_table "people", force: :cascade do |t|
    t.integer  "title_id",   null: false
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "people", ["parent_id"], name: "index_people_on_parent_id", using: :btree
  add_index "people", ["title_id"], name: "index_people_on_title_id", unique: true, using: :btree

  create_table "prefectures", force: :cascade do |t|
    t.integer  "title_id",   null: false
    t.integer  "region",     null: false
    t.string   "j_capital",  null: false
    t.string   "e_capital",  null: false
    t.integer  "sort_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "prefectures", ["region", "sort_order"], name: "index_prefectures_on_region_and_sort_order", using: :btree
  add_index "prefectures", ["sort_order"], name: "index_prefectures_on_sort_order", unique: true, using: :btree
  add_index "prefectures", ["title_id"], name: "index_prefectures_on_title_id", unique: true, using: :btree

  create_table "song_list_songs", force: :cascade do |t|
    t.integer  "song_list_id", null: false
    t.integer  "song_id"
    t.string   "j_title"
    t.string   "e_title"
    t.string   "j_ver"
    t.string   "e_ver"
    t.integer  "sort_order",   null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "song_list_songs", ["song_id"], name: "index_song_list_songs_on_song_id", using: :btree
  add_index "song_list_songs", ["song_list_id", "sort_order"], name: "index_song_list_songs_on_song_list_id_and_sort_order", unique: true, using: :btree

  create_table "song_lists", force: :cascade do |t|
    t.string   "keyword",    null: false
    t.integer  "device_id"
    t.integer  "concert_id"
    t.integer  "sort_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "book_id"
  end

  add_index "song_lists", ["book_id"], name: "index_song_lists_on_book_id", using: :btree
  add_index "song_lists", ["concert_id", "sort_order"], name: "index_song_lists_on_concert_id_and_sort_order", using: :btree
  add_index "song_lists", ["device_id", "sort_order"], name: "index_song_lists_on_device_id_and_sort_order", using: :btree

  create_table "song_people", force: :cascade do |t|
    t.integer  "song_list_song_id", null: false
    t.integer  "person_id",         null: false
    t.integer  "instrumental_id",   null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "song_people", ["person_id", "instrumental_id"], name: "song_people_i", using: :btree
  add_index "song_people", ["song_list_song_id", "instrumental_id"], name: "song_people_si", using: :btree
  add_index "song_people", ["song_list_song_id", "person_id", "instrumental_id"], name: "song_people_spi", using: :btree

  create_table "songs", force: :cascade do |t|
    t.integer  "title_id",      null: false
    t.integer  "parent_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "event_date_id"
  end

  add_index "songs", ["event_date_id"], name: "index_songs_on_event_date_id", using: :btree
  add_index "songs", ["parent_id"], name: "index_songs_on_parent_id", using: :btree
  add_index "songs", ["title_id"], name: "index_songs_on_title_id", unique: true, using: :btree

  create_table "titles", force: :cascade do |t|
    t.string   "japanese"
    t.string   "english"
    t.string   "yomi"
    t.string   "yomi_suuji"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "titles", ["yomi_suuji"], name: "index_titles_on_yomi_suuji", using: :btree

  create_table "years", force: :cascade do |t|
    t.string   "year",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "years", ["year"], name: "index_years_on_year", unique: true, using: :btree

end
