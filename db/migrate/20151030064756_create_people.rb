class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.integer :title_id, null: false
      t.integer :parent_id
      t.timestamps null: false
    end
    add_index :people, %i( title_id ), unique: true
    add_index :people, %i( parent_id )
  end
end
