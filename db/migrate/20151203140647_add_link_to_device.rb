class AddLinkToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :link, :string
  end
end
