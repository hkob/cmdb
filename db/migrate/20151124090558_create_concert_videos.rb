class CreateConcertVideos < ActiveRecord::Migration
  def change
    create_table :concert_videos do |t|
      t.integer :concert_song_list_song_id, null: false
      t.integer :video_song_list_song_id, null: false
      t.integer :concert_hall_id, null: false
      t.timestamps null: false
    end
    add_index :concert_videos, %i( concert_song_list_song_id )
    add_index :concert_videos, %i( video_song_list_song_id )
    add_index :concert_videos, %i( concert_hall_id )
  end
end
