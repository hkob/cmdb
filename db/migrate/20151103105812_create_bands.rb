class CreateBands < ActiveRecord::Migration
  def change
    create_table :bands do |t|
      t.integer :title_id, null: false
      t.string :j_comment
      t.string :e_comment
      t.timestamps null: false
    end
    add_index :bands, %i( title_id ), unique: true
  end
end
