class ChangeEventDateToConcert < ActiveRecord::Migration
  def change
    remove_index :concerts, column: %i( from )
    remove_column :concerts, :from, :date
    remove_column :concerts, :to, :date
    add_column :concerts, :from_id, :integer
    add_column :concerts, :to_id, :integer
    add_index :concerts, %i( from_id )
  end
end
