class CreateInstrumentals < ActiveRecord::Migration
  def change
    create_table :instrumentals do |t|
      t.integer :sort_order, null: false
      t.integer :title_id, null: false
      t.timestamps null: false
    end
    add_index :instrumentals, %i( sort_order ), unique: true
    add_index :instrumentals, %i( title_id ), unique: true
  end
end
