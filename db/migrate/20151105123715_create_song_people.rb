class CreateSongPeople < ActiveRecord::Migration
  def change
    create_table :song_people do |t|
      t.integer :song_list_song_id, null: false
      t.integer :person_id, null: false
      t.integer :instrumental_id, null: false
      t.timestamps null: false
    end
    add_index :song_people, %i( song_list_song_id person_id instrumental_id ), name: :song_people_spi
    add_index :song_people, %i( song_list_song_id instrumental_id ), name: :song_people_si
    add_index :song_people, %i( person_id instrumental_id ), name: :song_people_i
  end
end
