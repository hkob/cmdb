class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.integer :book_type, null: false
      t.integer :title_id, null: false
      t.integer :publisher_id
      t.integer :seller_id
      t.integer :photographer_id
      t.string :isbn
      t.string :price
      t.date :date
      t.integer :year_id
      t.integer :sort_order, null: false
      t.timestamps null: false
    end
    add_index :books, %i( book_type sort_order ), unique: true
    add_index :books, %i( year_id date )
  end
end
