class ChangeEventDateToActivity < ActiveRecord::Migration
  def change
    remove_index :activities, column: %i( year_id from )
    remove_column :activities, :from, :date
    remove_column :activities, :from_noday, :boolean
    remove_column :activities, :to, :date
    remove_column :activities, :to_noday, :boolean
    remove_column :activities, :year_id, :integer
    add_column :activities, :from_id, :integer
    add_column :activities, :to_id, :integer
    add_index :activities, %i( from_id )
  end
end
