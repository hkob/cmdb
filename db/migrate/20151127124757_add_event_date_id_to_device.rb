class AddEventDateIdToDevice < ActiveRecord::Migration
  def change
    remove_index :devices, column: %i( type device_type date )
    remove_index :devices, column: %i( year_id date )
    remove_column :devices, :date, :date
    remove_column :devices, :year_id, :integer
    add_column :devices, :event_date_id, :integer
    add_index :devices, %i( type device_type event_date_id )
    add_index :devices, %i( event_date_id )
  end
end
