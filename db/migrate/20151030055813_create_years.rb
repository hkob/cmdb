class CreateYears < ActiveRecord::Migration
  def change
    create_table :years do |t|
      t.string :year, null: false
      t.timestamps null: false
    end
    add_index :years, %i( year ), unique: true
  end
end
