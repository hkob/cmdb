class AddBookIdToSongList < ActiveRecord::Migration
  def change
    add_column :song_lists, :book_id, :integer
    add_index :song_lists, %i( book_id )
  end
end
