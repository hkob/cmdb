class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.integer :title_id, null: false
      t.timestamps null: false
    end
    add_index :companies, %i( title_id ), unique: true

  end
end
