class CreateLyrics < ActiveRecord::Migration
  def change
    create_table :lyrics do |t|
      t.integer :song_id, null: false
      t.integer :person_id, null: false
      t.integer :sort_order, null: false
      t.timestamps null: false
    end
    add_index :lyrics, %i( song_id person_id sort_order )
    add_index :lyrics, %i( song_id sort_order )
    add_index :lyrics, %i( person_id )
  end
end
