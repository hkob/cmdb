class AddEventDateIdToBook < ActiveRecord::Migration
  def change
    remove_index :books, column: %i( year_id date )
    remove_column :books, :year_id, :integer
    remove_column :books, :date, :date
    add_column :books, :event_date_id, :integer
    add_index :books, %i( event_date_id )
  end
end
