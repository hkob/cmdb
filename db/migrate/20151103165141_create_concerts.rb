class CreateConcerts < ActiveRecord::Migration
  def change
    create_table :concerts do |t|
      t.integer :title_id, null: false
      t.string :j_subtitle
      t.string :e_subtitle
      t.string :j_comment
      t.string :e_comment
      t.integer :concert_type, null: false
      t.date :from, null: false
      t.date :to
      t.boolean :has_song_list, null: false
      t.boolean :has_product, null: false
      t.integer :band_id
      t.integer :num_of_performances, null: false
      t.integer :num_of_halls, null: false
      t.integer :sort_order, null: false

      t.timestamps null: false
    end
    add_index :concerts, %i( title_id ), unique: true
    add_index :concerts, %i( concert_type sort_order ), unique: true
    add_index :concerts, %i( from )
  end
end
