class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :type, null: false
      t.integer :device_type, null: false
      t.integer :title_id, null: false
      t.date :date
      t.integer :minutes
      t.integer :seconds
      t.integer :sort_order, null: false
      t.string :number
      t.integer :year_id, null: false
      t.integer :singer_id
      t.string :j_comment
      t.string :e_comment

      t.timestamps null: false
    end
    add_index :devices, %i( title_id )
    add_index :devices, %i( type device_type date )
    add_index :devices, %i( year_id date )
    add_index :devices, %i( singer_id )
  end
end
