class CreateConcertHalls < ActiveRecord::Migration
  def change
    create_table :concert_halls do |t|
      t.integer :concert_id, null: false
      t.integer :sort_order, null: false
      t.integer :hall_id, null: false
      t.date :date, null: false
      t.integer :year_id, null: false
      t.string :j_hall_sub
      t.string :e_hall_sub
      t.string :j_comment
      t.string :e_comment
      t.string :j_product
      t.string :e_product
      t.integer :song_list_id
      t.integer :device_id
      t.timestamps null: false
    end
    add_index :concert_halls, %i( concert_id sort_order hall_id ), unique: true, name: :concert_halls_csh
    add_index :concert_halls, %i( year_id date )
    add_index :concert_halls, %i( song_list_id )
  end
end
