class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
      t.string :medium_device, null: false
      t.string :code
      t.string :release
      t.boolean :now_sale
      t.integer :company_id
      t.string :price
      t.integer :sort_order, null: false
      t.integer :device_id, null: false
      t.timestamps null: false
    end
    add_index :media, %i( device_id sort_order )
    add_index :media, %i( company_id code )
  end
end
