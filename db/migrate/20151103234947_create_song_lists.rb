class CreateSongLists < ActiveRecord::Migration
  def change
    create_table :song_lists do |t|
      t.string :keyword, null: false
      t.integer :device_id
      t.integer :concert_id
      t.integer :sort_order, null: false
      t.timestamps null: false
    end
    add_index :song_lists, %i( device_id sort_order )
    add_index :song_lists, %i( concert_id sort_order )
  end
end
