class AddLinkToBook < ActiveRecord::Migration
  def change
    add_column :books, :author_id, :integer
    add_column :books, :j_comment, :string
    add_column :books, :e_comment, :string
    add_column :books, :link, :string
    add_index :books, %i( author_id )
  end
end
