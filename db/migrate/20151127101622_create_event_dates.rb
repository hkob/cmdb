class CreateEventDates < ActiveRecord::Migration
  def change
    create_table :event_dates do |t|
      t.date :date, null: false
      t.integer :year_id, null: false
      t.integer :month
      t.integer :day
      t.timestamps null: false
    end
    add_index :event_dates, %i( year_id date month day )
    add_index :event_dates, %i( date )
  end
end
