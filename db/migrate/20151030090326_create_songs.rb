class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.integer :title_id, null: false
      t.integer :parent_id
      t.integer :year_id, null: false
      t.date :date
      t.timestamps null: false
    end
    add_index :songs, %i( year_id date )
    add_index :songs, %i( title_id ), unique: true
    add_index :songs, %i( parent_id )
  end
end
