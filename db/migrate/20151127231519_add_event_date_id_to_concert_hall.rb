class AddEventDateIdToConcertHall < ActiveRecord::Migration
  def change
    remove_index :concert_halls, column: %i( year_id date )
    remove_column :concert_halls, :date, :date
    remove_column :concert_halls, :year_id, :integer
    add_column :concert_halls, :event_date_id, :integer
    add_index :concert_halls, %i( event_date_id )
  end
end
