class CreateSongListSongs < ActiveRecord::Migration
  def change
    create_table :song_list_songs do |t|
      t.integer :song_list_id, null: false
      t.integer :song_id
      t.string :j_title
      t.string :e_title
      t.string :j_ver
      t.string :e_ver
      t.integer :sort_order, null: false
      t.timestamps null: false
    end
    add_index :song_list_songs, %i( song_list_id sort_order ), unique: true
    add_index :song_list_songs, %i( song_id )
  end
end
