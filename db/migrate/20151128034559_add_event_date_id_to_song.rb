class AddEventDateIdToSong < ActiveRecord::Migration
  def change
    remove_index :songs, column: %i( year_id date )
    remove_column :songs, :date, :date
    remove_column :songs, :year_id, :integer
    add_column :songs, :event_date_id, :integer
    add_index :songs, %i( event_date_id )
  end
end
