class CreateTitles < ActiveRecord::Migration
  def change
    create_table :titles do |t|
      t.string :japanese
      t.string :english
      t.string :yomi
      t.string :yomi_suuji
      t.timestamps null: false
    end
    add_index :titles, %i( yomi_suuji )
  end
end
