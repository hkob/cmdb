class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :activity_type, null: false
      t.integer :title_id, null: false
      t.integer :company_id
      t.integer :song_id
      t.date :from
      t.boolean :from_noday, null: false
      t.integer :year_id
      t.date :to
      t.boolean :to_noday, null: false
      t.string :j_comment
      t.string :e_comment
      t.integer :sort_order, null: false
      t.timestamps null: false
    end
    add_index :activities, %i( activity_type sort_order ), unique: true
    add_index :activities, %i( year_id from )
  end
end
